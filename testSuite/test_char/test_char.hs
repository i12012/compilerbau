import AbsSyn
import TypeCheck
test = ClassDecl("Test", [], [MethodDecl ("char", "getChar",[],
            Block ([
                    LocalVarDecl ("char", "i"),
                    StmtExprStmt (Assign ("i", Char('c'))),
                    LocalVarDecl ("char", "j"),
                    StmtExprStmt (Assign ("j", Char('e'))),
                    If (
                            Binary (   
                                "!=", 
                                LocalOrFieldVar ("i"),
                                LocalOrFieldVar ("j")),    
                            StmtExprStmt (Assign ("i", LocalOrFieldVar ("j"))),
                            Nothing
                            ),
                    Return (Just (LocalOrFieldVar("i")))
                ]))]) 