import AbsSyn

test = ClassDecl ("Test",[],[MethodDecl ("char","getChar",[],
        TypedStmt (Block [
                TypedStmt (LocalVarDecl ("char","i"),"char"),
                TypedStmt (StmtExprStmt (TypedStmtExpr (Assign ("i",TypedExpr (Char 'c',"char")),"char")),"char"),
                TypedStmt (LocalVarDecl ("char","j"),"char"),
                TypedStmt (StmtExprStmt (TypedStmtExpr (Assign ("j",TypedExpr (Char 'e',"char")),"char")),"char"),
                TypedStmt (If (
                    TypedExpr (Binary ("!=",TypedExpr (LocalOrFieldVar "i","char"),TypedExpr (LocalOrFieldVar "j","char")),"bool"),
                    TypedStmt (StmtExprStmt (TypedStmtExpr (Assign ("i",TypedExpr (LocalOrFieldVar "j","char")),"char")),"char"),
                    Nothing),"char"),
                TypedStmt (Return (Just (TypedExpr (LocalOrFieldVar "i","char"))),"char")
                ],"char")
              )])