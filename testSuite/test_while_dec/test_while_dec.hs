import AbsSyn
import TypeCheck

test = ClassDecl("Dec", [], [MethodDecl ("int", "m",[],
	Block([
			LocalVarDecl ("int", "i"),
			StmtExprStmt( Assign ("i",Integer(1))),
			While ( Binary ("!=",
                        LocalOrFieldVar("i"),
                        Integer(12)),
                    Block([
                        StmtExprStmt(Assign("i",Unary("--", LocalOrFieldVar("i"))))
                    ])
			 ),
			Return ( Just (LocalOrFieldVar("i")))
		]))
	])
