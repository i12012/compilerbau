import AbsSyn
import TypeCheck

test = ClassDecl("Test", [], [
    MethodDecl ("void", "method", [], Block ([
        If (Binary ("==", Integer (42), Integer (42)), Block([
            Return (Nothing)
            ]), Nothing)
        ]))
    ])
