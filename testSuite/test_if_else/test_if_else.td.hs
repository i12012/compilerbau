import AbsSyn
    
test = ClassDecl ("Test",[],[
    MethodDecl ("void","method",[],TypedStmt (Block [
        TypedStmt (If (
            TypedExpr (Binary ("==",TypedExpr (Integer 42,"int"),TypedExpr (Integer 42,"int")),"bool"),
            TypedStmt (Block [TypedStmt (Return Nothing,"void")],"void"),
            Just (TypedStmt (Block [TypedStmt (Return Nothing,"void")],"void")))            
         ,"void")     
     ],"void"))])
