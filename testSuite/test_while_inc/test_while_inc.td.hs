import AbsSyn

test = ClassDecl("Dec", [], [MethodDecl ("int", "m",[],
	Block([
			TypedStmt (LocalVarDecl ("int", "i"), "int"),
			TypedStmt (StmtExprStmt( Assign ("i",Integer(1))), "int"),
			TypedStmt (While ( Binary ("!=",
                        TypedExpr (LocalOrFieldVar("i"), "int"),
                        TypedExpr (Integer(12),"int")),
                    TypedStmt (Block([
                        TypedStmt (StmtExprStmt(Assign("i",TypedStmt (Unary("++", TypedStmt (LocalOrFieldVar("i"), "int")), "int"))), "int")
                    ]), "int")
			 ),
			TypedStmt (Return ( Just (TypedExpr(LocalOrFieldVar("i"),"int"))), "int")
		]))
	])
