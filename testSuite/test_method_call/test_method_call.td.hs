import AbsSyn

test = ClassDecl ("Test",[],[MethodDecl ("void","methode",[],
        TypedStmt (Block [
                LocalVarDecl ("Test", "a"),
                TypedStmt (StmtExprStmt (TypedStmtExpr (Assign ("a", TypedExpr (StmtExprExpr (TypedStmtExpr (New ("Test", []), "Test")), "Test")), "Test")), "Test"),
                TypedStmt (StmtExprStmt (TypedStmtExpr (MethodCall (TypedExpr(This, "Test"), "methode", []), "void")), "Test")
            ],"<none>")
        )])