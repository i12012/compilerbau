import AbsSyn
import TypeCheck

test = ClassDecl("Test", [],
	[MethodDecl("int", "methode",	[],
		Block([
			Return(Just(Integer(47)))
			])
	)])