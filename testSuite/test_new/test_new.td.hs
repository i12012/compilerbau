import AbsSyn

test = ClassDecl ("Test",[],[MethodDecl ("void","method",[],
        TypedStmt (Block [
                LocalVarDecl ("Test", "a"),
                TypedStmt (StmtExprStmt (TypedStmtExpr (Assign ("a", TypedExpr (StmtExprExpr (TypedStmtExpr (New ("Test", []), "Test")), "Test")), "Test")), "Test")
            ],"<none>")
        )])