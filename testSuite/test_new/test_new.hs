import AbsSyn
import TypeCheck

test = ClassDecl("Test",[],
	[MethodDecl("void", "methode",	[],
        Block([
            LocalVarDecl ("Test", "a"),
			StmtExprStmt (Assign ("a", StmtExprExpr (New ("Test", []))))
            ])
        )])
