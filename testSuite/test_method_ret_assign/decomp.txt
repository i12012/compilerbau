magic = 0x CAFEBABE
minor_version = 0
major_version = 52
constant_pool_count = 16
constant_pool =
{
1| tag = CONSTANT_Methodref, class_index = 4, name_and_type_index = 12
2| tag = CONSTANT_Fieldref, class_index = 3, name_and_type_index = 13
3| tag = CONSTANT_Class, name_index = 14
4| tag = CONSTANT_Class, name_index = 15
5| tag = CONSTANT_Utf8, length = 1, bytes = v
6| tag = CONSTANT_Utf8, length = 1, bytes = I
7| tag = CONSTANT_Utf8, length = 6, bytes = <init>
8| tag = CONSTANT_Utf8, length = 3, bytes = ()V
9| tag = CONSTANT_Utf8, length = 4, bytes = Code
10| tag = CONSTANT_Utf8, length = 6, bytes = method
11| tag = CONSTANT_Utf8, length = 11, bytes = (LTest;II)I
12| tag = CONSTANT_NameAndType, name_index = 7, descriptor_index = 8
13| tag = CONSTANT_NameAndType, name_index = 5, descriptor_index = 6
14| tag = CONSTANT_Utf8, length = 4, bytes = Test
15| tag = CONSTANT_Utf8, length = 16, bytes = java/lang/Object
}
access_flags = 32  // ACC_SUPER
this_class = #3  // Test
super_class = #4  // java/lang/Object
interfaces_count = 0
interfaces = {}
fields_count = 1
fields [0] = 
{
access_flags = 0
name_index = #5  // v
descriptor_index = #6  // I
attributes_count = 0
attributes = {}
}
methods_count = 2
methods [0] = 
{
access_flags = 0
name_index = #7  // <init>
descriptor_index = #8  // ()V
attributes_count = 1
attributes [0] = 
{
attribute_name_index = #9  // Code
attribute_length = 17
max_stack = 1, max_locals = 1
code_length = 5
code =
{
    0  aload_0
    1  invokespecial #1  // java/lang/Object.<init> ()V
    4  return
}
exception_table_length = 0
exception_table = {}
attributes_count = 0
attributes = {}
}
}
methods [1] = 
{
access_flags = 0
name_index = #10  // method
descriptor_index = #11  // (LTest;II)I
attributes_count = 1
attributes [0] = 
{
attribute_name_index = #9  // Code
attribute_length = 21
max_stack = 1, max_locals = 5
code_length = 9
code =
{
    0  aload_0
    1  getfield #2  // Test.v I
    4  istore 4
    6  iload 4
    8  ireturn
}
exception_table_length = 0
exception_table = {}
attributes_count = 0
attributes = {}
}
}
attributes_count = 0
attributes = {}
