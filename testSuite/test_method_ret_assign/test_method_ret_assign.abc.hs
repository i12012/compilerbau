import ByteCodeGen
import ClassFormat

test = ClassFile { magic = Magic   -- Magic
                           , minver = MinorVersion {
                                            numMinVer = 0
                                        }           -- MinorVersion
                           , maxver = MajorVersion {
                                                numMaxVer = 52
                                            }           -- MajorVersion
                           , count_cp = 16         -- ConstantPool_Count
                           , array_cp = [ 
                                    MethodRef_Info
                                        { tag_cp = TagMethodRef
                                        , index_name_cp = 4         -- Index_Constant_Pool
                                        , index_nameandtype_cp = 12  -- Index_Constant_Pool
                                        , desc = ""                 -- String
                                        },
                                        
                                    FieldRef_Info
                                        { tag_cp = TagFieldRef               -- Tag
                                        , index_name_cp = 3       -- Index_Constant_Pool
                                        , index_nameandtype_cp = 13  -- Index_Constant_Pool
                                        , desc = ""                  -- String
                                        },
                                    
                                    Class_Info
                                        { tag_cp =  TagClass                -- Tag
                                        , index_cp = 14              -- Index_Constant_Pool
                                        , desc = ""                  -- String
                                        },
                                    
                                    Class_Info
                                        { tag_cp = TagClass                -- Tag
                                        , index_cp = 15              -- Index_Constant_Pool
                                        , desc = ""                  -- String
                                        },
                                        
                                    Utf8_Info
                                        { tag_cp = TagUtf8                -- Tag
                                        , tam_cp = 1                -- Int
                                        , cad_cp = "v"                -- String
                                        , desc = ""                  -- String
                                        },
                                        
                                    Utf8_Info
                                        { tag_cp = TagUtf8                -- Tag
                                        , tam_cp = 1                -- Int
                                        , cad_cp = "I"                -- String
                                        , desc = ""                  -- String
                                        },
                                        
                                    Utf8_Info
                                        { tag_cp = TagUtf8                -- Tag
                                        , tam_cp = 6                -- Int
                                        , cad_cp = "<init>"                -- String
                                        , desc = ""                  -- String
                                        },
                                        
                                    Utf8_Info
                                        { tag_cp = TagUtf8                -- Tag
                                        , tam_cp = 3                -- Int
                                        , cad_cp = "()V"                -- String
                                        , desc = ""                  -- String
                                        },
                                        
                                    Utf8_Info
                                        { tag_cp = TagUtf8                -- Tag
                                        , tam_cp = 4                -- Int
                                        , cad_cp = "Code"                -- String
                                        , desc = ""                  -- String
                                        },
                                        
                                    Utf8_Info
                                        { tag_cp = TagUtf8                -- Tag
                                        , tam_cp = 6                -- Int
                                        , cad_cp = "method"                -- String
                                        , desc = ""                  -- String
                                        },
                                        
                                    Utf8_Info
                                        { tag_cp = TagUtf8                -- Tag
                                        , tam_cp = 11                -- Int
                                        , cad_cp = "(LTest;II)I"                -- String
                                        , desc = ""                  -- String
                                        },
                                        
                                    NameAndType_Info
                                        { tag_cp = TagNameAndType                -- Tag
                                        , index_name_cp = 7         -- Index_Constant_Pool
                                        , index_descr_cp = 8        -- Index_Constant_Pool
                                        , desc = ""                  -- String
                                        },
                                        
                                    NameAndType_Info
                                        { tag_cp = TagNameAndType                -- Tag
                                        , index_name_cp = 5         -- Index_Constant_Pool
                                        , index_descr_cp = 6        -- Index_Constant_Pool
                                        , desc = ""                  -- String
                                        },
                                        
                                    Utf8_Info
                                        { tag_cp = TagUtf8                -- Tag
                                        , tam_cp = 4                -- Int
                                        , cad_cp = "Test"                -- String
                                        , desc = ""                  -- String
                                        },
                                        
                                    Utf8_Info
                                        { tag_cp = TagUtf8                -- Tag
                                        , tam_cp = 16                -- Int
                                        , cad_cp = "java/lang/Object"                -- String
                                        , desc = ""                  -- String
                                        }
                                ]         -- CP_Infos
                           , acfg = AccessFlags [32]             -- AccessFlags
                           , this = ThisClass {
                                    index_th = 3
                                 }            -- ThisClass
                           , super = SuperClass {
                                    index_sp = 4
                                  }            -- SuperClass
                           , count_interfaces = 0              -- Interfaces_Count
                           , array_interfaces = []              -- Interfaces
                           , count_fields = 1              -- Fields_Count
                           , array_fields = [
                                Field_Info
                                    { af_fi = AccessFlags [0]          -- AccessFlags
                                    , index_name_fi = 5  -- Index_Constant_Pool     -- name_index
                                    , index_descr_fi = 6 -- Index_Constant_Pool     -- descriptor_index
                                    , tam_fi = 0          -- Int                     -- count_attributte
                                    , array_attr_fi = []  -- Attribute_Infos
                                    }
                                ]              -- Field_Infos
                           , count_methods = 2                  -- Methods_Count
                           , array_methods = [
                                Method_Info{ 
                                    af_mi = AccessFlags [0]          -- AccessFlags
                                    , index_name_mi = 7  -- Index_Constant_Pool       -- name_index
                                    , index_descr_mi = 8 -- Index_Constant_Pool       -- descriptor_index
                                    , tam_mi = 1         -- Int                       -- attributes_count
                                    , array_attr_mi = [
                                    
                                         AttributeCode
                                            {index_name_attr = 9           -- Index_Constant_Pool              -- attribute_name_index
                                            , tam_len_attr = 17              -- Int                              -- attribute_length
                                            , len_stack_attr = 1            -- Int                              -- max_stack
                                            , len_local_attr = 1            -- Int                              -- max_local
                                            , tam_code_attr = 5             -- Int                              -- code_length
                                            , array_code_attr = [Aload0,Invokespecial 1,CodeReturn]           -- ListaInt                         -- code como array de bytes
                                --, array_code_attr                     -- [Code]                           -- code array (altern.)
                                            , tam_ex_attr = 0               -- Int                              -- exceptions_length
                                            , array_ex_attr = []             -- Tupla4Int                        -- no usamos
                                            , tam_atrr_attr = 0             -- Int                              -- attributes_count
                                            , array_attr_attr = []           -- Attribute_Infos
                                            }
                                        ]  -- Attribute_Infos
                                },
                                Method_Info{ 
                                    af_mi = AccessFlags [0]          -- AccessFlags
                                    , index_name_mi = 10  -- Index_Constant_Pool       -- name_index
                                    , index_descr_mi = 11 -- Index_Constant_Pool       -- descriptor_index
                                    , tam_mi = 1         -- Int                       -- attributes_count
                                    , array_attr_mi = [
                                    
                                         AttributeCode
                                            {index_name_attr =  7           -- Index_Constant_Pool              -- attribute_name_index
                                            , tam_len_attr = 21              -- Int                              -- attribute_length
                                            , len_stack_attr = 1            -- Int                              -- max_stack
                                            , len_local_attr = 1            -- Int                              -- max_local
                                            , tam_code_attr = 9             -- Int                              -- code_length
                                            , array_code_attr = [Aload0,Getfield 2,Istore 4, Iload 4, Ireturn]           -- ListaInt                         -- code como array de bytes
                                --, array_code_attr                     -- [Code]                           -- code array (altern.)
                                            , tam_ex_attr = 0               -- Int                              -- exceptions_length
                                            , array_ex_attr = []             -- Tupla4Int                        -- no usamos
                                            , tam_atrr_attr = 0             -- Int                              -- attributes_count
                                            , array_attr_attr = []           -- Attribute_Infos
                                            }
                                        ]  -- Attribute_Infos
                                }]    -- Method_Infos
                           , count_attributes = 0 -- Attributes_Count
                           , array_attributes = [] -- Attribute_Infos
                           }