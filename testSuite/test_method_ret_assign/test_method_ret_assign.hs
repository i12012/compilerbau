import AbsSyn
import TypeCheck

test = ClassDecl("Test",[FieldDecl("int", "v")],
	[MethodDecl("int", "methode",	[("Test", "x"), ("int", "y"), ("int", "z")],
		Block([
			LocalVarDecl("int", "i"),
			StmtExprStmt(Assign("i", LocalOrFieldVar("v"))),
			Return(Just (LocalOrFieldVar("i")))
			])
	)])
