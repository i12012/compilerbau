import AbsSyn

test = ClassDecl ("Test",[FieldDecl ("int","v")],
	[MethodDecl ("int","methode",[("Test","x"),("int","y"),("int","z")],
		TypedStmt (Block [
			TypedStmt (LocalVarDecl ("int","i"),"int"),
			TypedStmt (StmtExprStmt (TypedStmtExpr (Assign ("i",TypedExpr (LocalOrFieldVar "v","int")),"int")),"int"),
			TypedStmt (Return (Just (TypedExpr (LocalOrFieldVar "i","int"))),"int")
		],"int")
	)])