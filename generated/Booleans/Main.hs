import AbsSyn
import CodeGen.ClassFileGen
import CodeGen.ClassFileIO
import CodeGen.ByteCodeGen

test = ClassDecl("Test", [FieldDecl("boolean", "i"),FieldDecl("boolean", "j")], [])

main = writeByteCode (generateClassFile test) ("Test.class")