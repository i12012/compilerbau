import AbsSyn
import CodeGen.ClassFileGen
import CodeGen.ClassFileIO
import CodeGen.ByteCodeGen

test = ClassDecl("Dec", [], [MethodDecl ("int", "m",[],
	Block([
			TypedStmt (LocalVarDecl ("int", "i"), "int"),
			TypedStmt (StmtExprStmt( Assign ("i",Integer(1))), "int"),
			TypedStmt (While ( Binary ("!=",
                        TypedExpr (LocalOrFieldVar("i"), "int"),
                        TypedExpr (Integer(12),"int")),
                    TypedStmt (Block([
                        TypedStmt (StmtExprStmt(Assign("i", TypedExpr (Unary("--", TypedExpr (LocalOrFieldVar("i"), "int")), "int"))), "int")
                    ]), "int")
			 ), "<none>"),
			TypedStmt (Return ( Just (TypedExpr(LocalOrFieldVar("i"),"int"))), "int")
		]))
	])

main = writeByteCode (generateClassFile test) ("Test.class")