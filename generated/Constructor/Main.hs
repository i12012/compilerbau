import AbsSyn
import CodeGen.ClassFileGen
import CodeGen.ClassFileIO
import CodeGen.ByteCodeGen

test = ClassDecl ("Test",[],[MethodDecl ("void","<init>",[],TypedStmt (Block [],"<none>"))])

main = writeByteCode (generateClassFile test) ("Test.class")