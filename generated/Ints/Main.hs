import AbsSyn
import CodeGen.ClassFileGen
import CodeGen.ClassFileIO
import CodeGen.ByteCodeGen

test = ClassDecl("Test", [FieldDecl("int", "i"),FieldDecl("int", "j")], [])

main = writeByteCode (generateClassFile test) ("Test.class")