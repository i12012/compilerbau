
import JavaParser
import TypeCheck
import CodeGen.ByteCodeGen
import CodeGen.ClassFileGen
import CodeGen.ClassFileIO
import AbsSyn

main2 = do
    inputText <- getContents
    let classDecl@(ClassDecl (name, _, _)) = (parse inputText)!!0
        byteCode = writeByteCode (generateClassFile (typecheckClassDecl classDecl)) (name ++ ".class")
    byteCode

test = ClassDecl ("Test",[FieldDecl ("int","v")],
	[MethodDecl ("int","methode",[("Test","x"),("int","y"),("int","z")],
		TypedStmt (Block [
			TypedStmt (LocalVarDecl ("int","i"),"int"),
			TypedStmt (StmtExprStmt (TypedStmtExpr (Assign ("i",TypedExpr (LocalOrFieldVar "v","int")),"int")),"int"),
			TypedStmt (Return (Just (TypedExpr (LocalOrFieldVar "i","int"))),"int")
		],"int")
	)])

main = writeByteCode (generateClassFile test) ("Test.class")
