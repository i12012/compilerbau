import AbsSyn
import CodeGen.ClassFileGen
import CodeGen.ClassFileIO
import CodeGen.ByteCodeGen

test = ClassDecl("Test", [FieldDecl("double", "i"),FieldDecl("double", "j")], [])

main = writeByteCode (generateClassFile test) ("Test.class")