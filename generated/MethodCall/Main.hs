import AbsSyn
import CodeGen.ClassFileGen
import CodeGen.ClassFileIO
import CodeGen.ByteCodeGen

test = ClassDecl ("Test",[],[MethodDecl ("void","methode",[],
        TypedStmt (Block [
                LocalVarDecl ("Test", "a"),
                TypedStmt (StmtExprStmt (TypedStmtExpr (MethodCall (TypedExpr(This, "Test"), "blubb", []), "void")), "subtest")
            ],"<none>")
        ),
		MethodDecl ("void", "blubb", [], Block [])])

main = writeByteCode (generateClassFile test) ("Test.class")