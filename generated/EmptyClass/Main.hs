import AbsSyn
import CodeGen.ClassFileGen
import CodeGen.ClassFileIO
import CodeGen.ByteCodeGen

test = ClassDecl ("Test", [], [])


main = writeByteCode (generateClassFile test) ("Test.class")