import AbsSyn
import CodeGen.ClassFileGen
import CodeGen.ClassFileIO
import CodeGen.ByteCodeGen

test = ClassDecl("Test",[],
	[MethodDecl("void", "methode",	[],
		Block([])
	)])

main = writeByteCode (generateClassFile test) ("Test.class")