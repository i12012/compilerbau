
import AbsSyn
import CodeGen.ClassFileGen
import CodeGen.ClassFileIO
import CodeGen.ByteCodeGen

test = ClassDecl ("Test",[],[MethodDecl ("void","methode",[],
        TypedStmt (Block [
                TypedStmt (StmtExprStmt (TypedStmtExpr (MethodCall (TypedExpr(This, "int"), "<init>", []), "void")), "subtest")
            ],"<none>")
        )])


main = writeByteCode (generateClassFile test) ("Test.class")