import AbsSyn
import CodeGen.ClassFileGen
import CodeGen.ClassFileIO
import CodeGen.ByteCodeGen

test = ClassDecl ("Test",[],
	[MethodDecl ("int","methode",[],
		TypedStmt (Block [
			TypedStmt (Return (Just (TypedExpr (Integer 47,"int"))),"int")
		],"int")
	)])

main = writeByteCode (generateClassFile test) ("Test.class")