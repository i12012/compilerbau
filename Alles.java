
class Alles {
    int doIf() {
        int a = 42;
        int b = a + 5;
        if (a != b) {
            return 2;
        } else {
            return 5;
        }
    }

    void doWhile() {
        int i = 5;
        while (i != 0) {
            i = i - 1;
        }
    }

    char getChar() {
        char a = '5';
        if (this.calcSomething() > 10) {
            a = '6';
        }
        return a;
    }

    int calcSomething() {
        int x = 5;
        return x*42;
    }

    Alles newAlles() {
        return new Alles();
    }
}