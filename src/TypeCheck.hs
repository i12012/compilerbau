module TypeCheck where

import AbsSyn
import Data.List

-- ClassDecl==========================================================================================================
typecheckClassDecl :: ClassDecl -> ClassDecl
typecheckClassDecl (ClassDecl (className, [], [])) = ClassDecl (className, [], [])
typecheckClassDecl (ClassDecl (className, fields, methods)) = 
		let vars = readFields fields
		in 	let classes = [ClassDecl ("this", fields, methods)]	-- TODO: get list of all known classes if necessary
			in	ClassDecl (className, 
				(map (\x -> typecheckFieldDecl x) fields), 
				(map (\x -> (typecheckMethodDecl x vars classes)) methods))

-- ===================================================================================================================

-- MethodDecl=========================================================================================================
typecheckMethodDecl :: MethodDecl -> [VarDecl] -> [ClassDecl] -> MethodDecl
typecheckMethodDecl (MethodDecl (typ, name, params, stmt)) vars classes = 
	let newvars = vars ++ params
	in 	let typedStmt = head(typecheckStmt [stmt] newvars classes)
		in 	if typ == (getTypeFromStmt typedStmt) || (typ == "void" && (getTypeFromStmt typedStmt) == "<none>")
			then (MethodDecl (typ, name, params, typedStmt))
			else error("Method type doesnt match return type. Types are: (" ++ typ ++ ", " ++ (getTypeFromStmt typedStmt) ++ ")")

-- ===================================================================================================================

-- FieldDecl==========================================================================================================
typecheckFieldDecl :: FieldDecl -> FieldDecl
typecheckFieldDecl fd = fd -- here isnt anyting to be done
-- ===================================================================================================================

-- Stmt===============================================================================================================
--               stmt,   lokale Variablen,   klassen,       typisiertes stmt
typecheckStmt :: [Stmt] -> [VarDecl] -> [ClassDecl] -> [Stmt]
typecheckStmt [] vars classes = []

-- Block
typecheckStmt (Block ([]) : nstatements) vars classes = 
	(TypedStmt (Block ([]), "<none>") : (typecheckStmt nstatements vars classes)) -- empty block has none type
typecheckStmt (Block (statements) : nstatements) vars classes = 
	let typedStatements = typecheckStmt statements vars classes
	in 	let blocktypes = getTypeFromBlockStatementList typedStatements []
		in 	if (length blocktypes) == 1
			then (TypedStmt (Block (typedStatements), (head blocktypes)) : (typecheckStmt nstatements vars classes))
		 	else 	if (allTheSame (filter (/= "<none>") blocktypes))
		 			then (TypedStmt (Block (typedStatements), head(blocktypes)) : (typecheckStmt nstatements vars classes))
		 			else error("Unequal return types in block : " ++ (intercalate "; " blocktypes))

--Return
typecheckStmt (Return Nothing : statements) vars classes = 
	(TypedStmt ((Return Nothing), "void") : (typecheckStmt statements vars classes))
typecheckStmt (Return (Just expr) : statements) vars classes = 
	let texpr = (typecheckExpr (expr) vars classes)
	in	(TypedStmt (Return (Just (texpr)), (getTypeFromExpr texpr)) : (typecheckStmt statements vars classes))

-- LocalVarDecl
typecheckStmt (LocalVarDecl (varType, varName) : statements) vars classes = 
	((TypedStmt (LocalVarDecl (varType, varName), varType)) : (typecheckStmt statements ((varType, varName) : vars) classes))

-- While
typecheckStmt (While (expr, stmt) : statements) vars classes =
	let typedExpr = typecheckExpr expr vars classes
	in 	let typedStmt = head(typecheckStmt [stmt] vars classes)
		in 	if (getTypeFromExpr typedExpr) == "bool"
			then (TypedStmt (While (typedExpr, typedStmt), (getTypeFromStmt typedStmt)) : (typecheckStmt statements vars classes))
			else error("Expression in While-Clause is not boolean type")

-- If
typecheckStmt (If (expr, ifStmt, Just elStmt) : statements) vars classes = 
	let typedExpr = typecheckExpr expr vars classes
	in 	let typedIfStmt = head(typecheckStmt [ifStmt] vars classes)
		in 	let typedElStmt = head(typecheckStmt [elStmt] vars classes)
			in 	if (getTypeFromExpr typedExpr) == "bool"
				then	let typ = (checkIfElseTypes (getTypeFromStmt typedIfStmt) (getTypeFromStmt typedElStmt))
						in (TypedStmt (If (typedExpr, typedIfStmt, Just typedElStmt), typ) : (typecheckStmt statements vars classes))
				else error("Expression in If-Statement is not boolean type")
typecheckStmt (If (expr, ifStmt, Nothing) : statements) vars classes = 
	let typedExpr = typecheckExpr expr vars classes
	in 	let typedIfStmt = head(typecheckStmt [ifStmt] vars classes)
		in 	if (getTypeFromExpr typedExpr) == "bool"
			then (TypedStmt (If (typedExpr, typedIfStmt, Nothing), (getTypeFromStmt typedIfStmt)): (typecheckStmt statements vars classes))
			else error("Expression in If-Statement is not boolean type")

-- StmtExprStmt
typecheckStmt (StmtExprStmt (stmtExpr) : statements) vars classes = 
	let typedStmtExpr = typecheckStmtExpr stmtExpr vars classes
	in (TypedStmt (StmtExprStmt (typedStmtExpr), (getTypeFromStmtExpr typedStmtExpr)) : (typecheckStmt statements vars classes))

-- ===================================================================================================================

-- Expr===============================================================================================================
--               expr,   lokale Variablen,   klassen,       typisierte expr
typecheckExpr :: Expr -> [VarDecl] -> [ClassDecl] -> Expr
-- Constant Types
typecheckExpr (Integer value) vars classes = TypedExpr(Integer (value), "int")
typecheckExpr (Bool value) vars classes = TypedExpr(Bool (value), "bool")
typecheckExpr (Char value) vars classes = TypedExpr(Char (value), "char")
typecheckExpr (String value) vars classes = TypedExpr(String (value), "string")

-- Local or Field Variables
typecheckExpr (LocalOrFieldVar fieldName) ((ftype, fname) : vars) classes =
	if fname == fieldName
		then TypedExpr (LocalOrFieldVar (fieldName), ftype)
		else typecheckExpr (LocalOrFieldVar (fieldName)) vars classes
typecheckExpr (LocalOrFieldVar fieldName) [] classes = error("LocalOrFieldVar '" ++ fieldName ++ "' not found")

-- Instance Variable
typecheckExpr (InstVar (expr, name)) vars classes = 
	let (TypedExpr(texpr, exprType)) = typecheckExpr expr vars classes
    in 	let (ClassDecl (cname, cfields, cmethods)) = findClass exprType classes
    	in let ftype = findField name cfields
  	    in TypedExpr(InstVar(TypedExpr(texpr, exprType), name), ftype)

-- Statement Expression Expression
typecheckExpr (StmtExprExpr (stmtExpr)) vars classes =
	let typedStmtExpr = typecheckStmtExpr stmtExpr vars classes
	in (TypedExpr (StmtExprExpr (typedStmtExpr), getTypeFromStmtExpr typedStmtExpr))

-- Null
typecheckExpr (Jnull) vars classes = 
	(TypedExpr (Jnull, "null"))

-- Unary
typecheckExpr (Unary ("-", expr)) vars classes = 
	let (TypedExpr (texpr1, type1)) = typecheckExpr expr vars classes
	in 	if type1 == "int"
		then (TypedExpr (Unary ("-", texpr1), type1))
		else error("Unary expression of '-' must be integer type")

typecheckExpr (Unary ("+", expr)) vars classes = 
	let (TypedExpr (texpr1, type1)) = typecheckExpr expr vars classes
	in 	if type1 == "int"
		then (TypedExpr (Unary ("+", texpr1), type1))
		else error("Unary expression of '+' must be integer type")

typecheckExpr (Unary ("--", expr)) vars classes = 
	let (TypedExpr (texpr1, type1)) = typecheckExpr expr vars classes
	in 	if type1 == "int"
		then (TypedExpr (Unary ("--", texpr1), type1))
		else error("Unary expression of '--' must be integer type")

typecheckExpr (Unary ("++", expr)) vars classes = 
	let (TypedExpr (texpr1, type1)) = typecheckExpr expr vars classes
	in 	if type1 == "int"
		then (TypedExpr (Unary ("++", texpr1), type1))
		else error("Unary expression of '++' must be integer type")

typecheckExpr (Unary ("!", expr)) vars classes = 
	let (TypedExpr (texpr1, type1)) = typecheckExpr expr vars classes
	in 	if type1 == "bool"
		then (TypedExpr (Unary ("!", texpr1), type1))
		else error("Unary expression of '!' must be boolean type")		

-- Binary
typecheckExpr (Binary ("&&", Bool (True), Bool (True))) vars classes = 
	(TypedExpr (Bool (True), "bool"))
typecheckExpr (Binary ("&&", expr1, expr2)) vars classes = 
	let (TypedExpr (texpr1, type1)) = typecheckExpr expr1 vars classes
	in 	let (TypedExpr (texpr2, type2)) = typecheckExpr expr2 vars classes
		in 	if type1 == "bool" && type2 == "bool"
			then (TypedExpr (Binary ("&&", TypedExpr (texpr1, type1), TypedExpr (texpr2, type2)), "bool"))
			else error("Binary expressions of '&&' must be boolean type, not (" ++ type1 ++ ", " ++ type2 ++ ")")

typecheckExpr (Binary ("||", Bool (False), Bool (False))) vars classes = 
	(TypedExpr (Bool (False), "bool"))
typecheckExpr (Binary ("||", expr1, expr2)) vars classes =
	let (TypedExpr (texpr1, type1)) = typecheckExpr expr1 vars classes
	in 	let (TypedExpr (texpr2, type2)) = typecheckExpr expr2 vars classes
		in 	if type1 == "bool" && type2 == "bool"
			then (TypedExpr (Binary ("||", TypedExpr (texpr1, type1), TypedExpr (texpr2, type2)), "bool"))
			else error("Binary expressions of '||' must be boolean type, not (" ++ type1 ++ ", " ++ type2 ++ ")")

typecheckExpr (Binary ("==", expr1, expr2)) vars classes = 
	let (TypedExpr (texpr1, type1)) = typecheckExpr expr1 vars classes
	in 	let (TypedExpr (texpr2, type2)) = typecheckExpr expr2 vars classes
		in 	if type1 == type2
			then (TypedExpr (Binary ("==", TypedExpr (texpr1, type1), TypedExpr (texpr2, type2)), "bool"))
			else error("Binary expressions of '==' must be same type, not (" ++ type1 ++ ", " ++ type2 ++ ")")

typecheckExpr (Binary ("!=", expr1, expr2)) vars classes = 
	let (TypedExpr (texpr1, type1)) = typecheckExpr expr1 vars classes
	in 	let (TypedExpr (texpr2, type2)) = typecheckExpr expr2 vars classes
		in 	if type1 == type2
			then (TypedExpr (Binary ("!=", TypedExpr (texpr1, type1), TypedExpr (texpr2, type2)), "bool"))
			else error("Binary expressions of '!=' must be same type, not (" ++ type1 ++ ", " ++ type2 ++ ")")

typecheckExpr (Binary ("<", expr1, expr2)) vars classes = 
	let (TypedExpr (texpr1, type1)) = typecheckExpr expr1 vars classes
	in 	let (TypedExpr (texpr2, type2)) = typecheckExpr expr2 vars classes
		in 	if type1 == "int" && type2 == "int"
			then (TypedExpr (Binary ("<", TypedExpr (texpr1, type1), TypedExpr (texpr2, type2)), "bool"))
			else error("Binary expressions of '<' must be integer type, not (" ++ type1 ++ ", " ++ type2 ++ ")")

typecheckExpr (Binary (">", expr1, expr2)) vars classes = 
	let (TypedExpr (texpr1, type1)) = typecheckExpr expr1 vars classes
	in 	let (TypedExpr (texpr2, type2)) = typecheckExpr expr2 vars classes
		in 	if type1 == "int" && type2 == "int"
			then (TypedExpr (Binary (">", TypedExpr (texpr1, type1), TypedExpr (texpr2, type2)), "bool"))
			else error("Binary expressions of '>' must be integer type, not (" ++ type1 ++ ", " ++ type2 ++ ")")			

typecheckExpr (Binary ("<=", expr1, expr2)) vars classes = 
	let (TypedExpr (texpr1, type1)) = typecheckExpr expr1 vars classes
	in 	let (TypedExpr (texpr2, type2)) = typecheckExpr expr2 vars classes
		in 	if type1 == "int" && type2 == "int"
			then (TypedExpr (Binary ("<=", TypedExpr (texpr1, type1), TypedExpr (texpr2, type2)), "bool"))
			else error("Binary expressions of '<=' must be integer type, not (" ++ type1 ++ ", " ++ type2 ++ ")")

typecheckExpr (Binary (">=", expr1, expr2)) vars classes = 
	let (TypedExpr (texpr1, type1)) = typecheckExpr expr1 vars classes
	in 	let (TypedExpr (texpr2, type2)) = typecheckExpr expr2 vars classes
		in 	if type1 == "int" && type2 == "int"
			then (TypedExpr (Binary (">=", TypedExpr (texpr1, type1), TypedExpr (texpr2, type2)), "bool"))
			else error("Binary expressions of '>=' must be integer type, not (" ++ type1 ++ ", " ++ type2 ++ ")")

-- ===================================================================================================================

-- StmtExpr===============================================================================================================
--               expr,   lokale Variablen,   klassen,       typisierte expr
typecheckStmtExpr :: StmtExpr -> [VarDecl] -> [ClassDecl] -> StmtExpr

-- Assign (ist das so richtig???)
typecheckStmtExpr (Assign (name, expr)) vars classes =
	let varType = findVariable name vars
	in 	let typedExpr = typecheckExpr expr vars classes
		in 	if varType == (getTypeFromExpr typedExpr) || (getTypeFromExpr typedExpr) == "null"
			then (TypedStmtExpr (Assign (name, typedExpr), varType))
			else error("Cannot assign from type '" ++ (getTypeFromExpr typedExpr) ++ "' to type '" ++ varType ++ "'")

-- New
typecheckStmtExpr (New (typ, exprs)) vars classes = 
	let typClass = findClass typ classes
	in 	let typedExprs = (map (\x -> typecheckExpr x vars classes) exprs)
		in 	(TypedStmtExpr (New (typ, typedExprs), typ))

-- Method call
typecheckStmtExpr (MethodCall (This, name, params)) vars classes =
	let (ClassDecl (cname, cfields, cmethods)) = findClass "this" classes
	in 	let methodType = findMethod name cmethods
		in 	let typedParams = (map (\x -> typecheckExpr x vars classes) params)
			in 	(TypedStmtExpr (MethodCall (This, name, typedParams), methodType))
typecheckStmtExpr (MethodCall (callBase, name, params)) vars classes =
	let typedCallBase = typecheckExpr callBase vars classes
	in 	let (ClassDecl (cname, cfields, cmethods)) = findClass (getTypeFromExpr typedCallBase)  classes
		in 	let methodType = findMethod name cmethods
			in 	let typedParams = (map (\x -> typecheckExpr x vars classes) params)
				in 	(TypedStmtExpr (MethodCall (typedCallBase, name, typedParams), methodType))

-- ===================================================================================================================

-- Helper Functions===================================================================================================

-- Find class by name from list (return class declaration)
findClass :: String -> [ClassDecl] -> ClassDecl
findClass className [] = error("Class '" ++ className ++ "' not existing")
findClass className ((ClassDecl (cname, fields, methods)): classes) =
	if cname == className 
		then ClassDecl (cname, fields, methods) 
		else findClass className classes

-- Find field by name from list (return fields type)
findField :: String -> [FieldDecl] -> Type
findField fieldName [] = error("Field '" ++ fieldName ++ "' not existing")
findField fieldName ((FieldDecl (ftype , fname)) : fields) = 
	if fname == fieldName 
		then ftype 
		else findField fieldName fields

-- Covnert field declarations to variable declarations
readFields :: [FieldDecl] -> [VarDecl]
readFields [] = []
readFields ((FieldDecl (ftype, fname)) : fields) = ((ftype, fname) : (readFields fields))

-- Find variable by name (return variable type)
findVariable :: String -> [VarDecl] -> Type
findVariable name [] = error("Variable '" ++ name ++ "' not existing")
findVariable name ((vtype, vname) : variables) = 
	if vname == name
		then vtype
		else findVariable name variables

-- Find method by name (return methods type)
findMethod :: String -> [MethodDecl] -> Type
findMethod methodName [] = error("Method '" ++ methodName ++ "' not existing")
findMethod methodName ((MethodDecl (mtype, mName, mParams, mbody) : methods)) = 
	if mName == methodName
		then mtype
		else findMethod methodName methods

getTypeFromBlockStatementList :: [Stmt] -> [Type] -> [Type]
getTypeFromBlockStatementList [] [] = ["<none>"]	-- Leerer Block hat Typ "none"
getTypeFromBlockStatementList [] types = types 	-- wenn schon typ v.h. dann diese
getTypeFromBlockStatementList (TypedStmt (Return (Nothing), typ) : statements) typen = getTypeFromBlockStatementList statements ("void" : typen)
getTypeFromBlockStatementList (TypedStmt (Return (Just expr), typ) : statements) typen = getTypeFromBlockStatementList statements (typ : typen)
getTypeFromBlockStatementList (TypedStmt (Block (blockstatements), typ) : statements) typen = getTypeFromBlockStatementList statements (typ : typen)
getTypeFromBlockStatementList (TypedStmt (If (expr, stmt, Nothing), typ) : statements) typen = getTypeFromBlockStatementList statements (typ : typen)
getTypeFromBlockStatementList (TypedStmt (If (expr, stmt, Just (elstmt)), typ) : statements) typen = getTypeFromBlockStatementList statements (typ : typen)
getTypeFromBlockStatementList (TypedStmt (While (expr, stmt), typ) : statements) typen = getTypeFromBlockStatementList statements (typ : typen)
getTypeFromBlockStatementList (x : xs) types = getTypeFromBlockStatementList xs types

-- Compare all elements in list if they are equal
allTheSame :: (Eq a) => [a] -> Bool
allTheSame [] = True
allTheSame (x : xs)= and $ map (== x) (xs)

-- determine if types from if and else statements are compatible
checkIfElseTypes :: Type -> Type -> Type
checkIfElseTypes "<none>" typ = typ
checkIfElseTypes typ "<none>" = typ
checkIfElseTypes typ1 typ2 = 
	if typ1 == typ2
		then typ1
		else error("Die Typen '" ++ typ1 ++ "' und '" ++ typ2 ++ "' können nicht gleichzeitig Teil eines If-Statement sein.")