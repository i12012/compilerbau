module CodeGen.ByteCodeGen where

import CodeGen.Data
import CodeGen.CpLookup
import ClassFormat
import AbsSyn
import Mangle

import Data.Char (ord);

instance CodeGenerator Stmt where
    generateCode cs (Block stmts) = foldl (++) [] (map (generateCode cs) stmts)
    generateCode cs (Return (Nothing)) = [CodeReturn]
    generateCode cs (Return (Just expr)) =
        let exprCode = generateCode cs expr
            typ = getTypeFromExpr expr
            retValIdx = (length (currentVars cs)) + 1 -- Index of return variable
            in case typ of
                "boolean" -> exprCode ++ [Ireturn]
                "byte" -> exprCode ++ [Ireturn]
                "short" -> exprCode ++ [Ireturn]
                "int" -> exprCode ++ [Ireturn]
                "char" -> exprCode ++ [Ireturn]
                "double" -> exprCode ++ [Dreturn]
                "long" -> exprCode ++ [Lreturn]
                "float" -> exprCode ++ [Freturn]
                _ -> exprCode ++ [storeInstr typ retValIdx] ++ [Ret retValIdx]
    generateCode cs (LocalVarDecl _) = [] -- Variable declaration do not generate any code
    generateCode cs (While (expr, thenStmt)) =
        let exprCode = generateIfExprCode cs expr
            loopCode = generateCode cs thenStmt
            jumpTrueLoc = (calcCodeSize loopCode)+(getCodeSize (Ifeq 0))+(getCodeSize (Goto 0))
            whileExprCode = (exprCode jumpTrueLoc)
            jumpBackLoc = -(calcCodeSize loopCode)-(calcCodeSize whileExprCode)
            in whileExprCode ++ loopCode ++ [Goto jumpBackLoc]
    generateCode cs (If (expr, thenStmt, Nothing)) =
        let ifExprCode = generateIfExprCode cs expr
            thenCode = generateCode cs thenStmt
            jumpLoc = ((getCodeSize (Ifeq 0)) + (calcCodeSize (thenCode)))
            in (ifExprCode jumpLoc) ++ thenCode
    generateCode cs (If (expr, thenStmt, Just elseStmt)) =
        let ifExprCode = generateIfExprCode cs expr
            elseCode = generateCode cs elseStmt
            thenCode = (generateCode cs thenStmt) ++ [Goto ((calcCodeSize elseCode) + getCodeSize (Goto 0))]
            jumpLoc = ((getCodeSize (Ifeq 0)) + (calcCodeSize (thenCode)))
            in (ifExprCode jumpLoc) ++ thenCode ++ elseCode
    generateCode cs (StmtExprStmt (stmtExpr)) = generateCode cs stmtExpr
    generateCode cs (TypedStmt (stmt,_)) = generateCode cs stmt

instance CodeGenerator StmtExpr where
    generateCode cs (TypedStmtExpr (MethodCall (expr, name, args), typ)) =
        let exprcode = generateCode cs expr
            argscode = generateAllCodes cs args
            in exprcode ++ argscode ++ [Invokevirtual (indexOfMethod (constantPool cs) (getTypeFromExpr expr) (name) (mangleMethodCall args typ) )]
    generateCode cs (TypedStmtExpr (stmtExpr, typ)) = generateCode cs stmtExpr
    generateCode cs (Assign (name, expr)) =
        let (typ, idx) = findVar name (currentVars cs)
            exprcode = generateCode cs expr
            in exprcode ++ [storeInstr typ idx]
    generateCode cs (New (typ, expr)) =
        let exprcode = generateAllCodes cs expr
            in exprcode ++ [CodeNew (indexOfClass (constantPool cs) typ), Dup, Invokespecial (indexOfMethod (constantPool cs) (typ) ("<init>") (mangleMethodCall expr "void") )]

instance CodeGenerator Expr where
    -- | Generates the code that calculates the expression and leaves the result on the stack
    generateCode cs (TypedExpr (expr, _)) = generateCode cs expr
    generateCode cs (Integer i)
        | i == -1 = [Iconst_m1]
        | i == 0 = [Iconst_0]
        | i == 1 = [Iconst_1]
        | i == 2 = [Iconst_2]
        | i == 3 = [Iconst_3]
        | i == 4 = [Iconst_4]
        | i == 5 = [Iconst_5]
        | (i >= -128) && (i <= 127) = [Bipush (fromInteger i)]
        | (i >= -32768) && (i <= 32767) = [Sipush (fromInteger i)]
        | otherwise =
            let idx = indexOfInt (constantPool cs) (fromInteger i)
                in if idx < 256 then [Ldc idx]
                                else [Ldc_w idx]
    generateCode cs (Char c) = generateCode cs (Integer $ toInteger (ord c))
    generateCode cs (LocalOrFieldVar name) =
        let varlookup = lookup name (currentVars cs)
            in case varlookup of
                Nothing -> [Getfield (indexOfThisField (constantPool cs) name)]
                Just (typ, idx) -> [loadInstr typ idx]
    generateCode cs (This) = [Aload_0]
    generateCode cs (Jnull) = [Aconst_null]
    generateCode cs (String s) = [Ldc (indexOfString (constantPool cs) s)]
    generateCode cs (Binary (op, left, right)) =
        let leftCode = generateCode cs left
            rightCode = generateCode cs right
            leftType = getTypeFromExpr left
            rightType = getTypeFromExpr right
            (wtyp, code) = unifyTypes leftCode rightCode leftType rightType
            in case (op, wtyp) of
                ("+", "int") -> code ++ [Iadd]
                ("-", "int") -> code ++ [Isub]
                ("*", "int") -> code ++ [Imul]
                ("/", "int") -> code ++ [Idiv]
                ("&", "int") -> code ++ [Iand]
                ("|", "int") -> code ++ [Ior]
                ("^", "int") -> code ++ [Ixor]
                ("%", "int") -> code ++ [Irem]
                ("+", "long") -> code ++ [Ladd]
                ("-", "long") -> code ++ [Lsub]
                ("*", "long") -> code ++ [Lmul]
                ("/", "long") -> code ++ [Ldiv]
                ("&", "long") -> code ++ [Land]
                ("^", "long") -> code ++ [Lxor]
                ("|", "long") -> code ++ [Lor]
                ("%", "long") -> code ++ [Lrem]
                ("+", "float") -> code ++ [Fadd]
                ("-", "float") -> code ++ [Fsub]
                ("*", "float") -> code ++ [Fmul]
                ("/", "float") -> code ++ [Fdiv]
                ("%", "float") -> code ++ [Frem]
                ("+", "double") -> code ++ [Dadd]
                ("-", "double") -> code ++ [Dsub]
                ("*", "double") -> code ++ [Dmul]
                ("/", "double") -> code ++ [Ddiv]
                ("%", "double") -> code ++ [Drem]
                _ -> case (op, leftType, rightType) of
                    ("<<", "int", t) -> leftCode ++ (castTo rightCode t "int") ++ [Ishl]
                    ("<<", "long", t) -> leftCode ++ (castTo rightCode t "int") ++ [Lshl]
                    (">>", "int", t) -> leftCode ++ (castTo rightCode t "int") ++ [Ishr]
                    (">>", "long", t) -> leftCode ++ (castTo rightCode t "int") ++ [Lshr]
                    (">>>", "int", t) -> leftCode ++ (castTo rightCode t "int") ++ [Iushr]
                    (">>>", "long", t) -> leftCode ++ (castTo rightCode t "int") ++ [Lushr]
    generateCode cs (Unary (op, e)) =
        let code = generateCode cs e
            typ = getTypeFromExpr e
            varIdx = getVarIndexFromExpr cs e
            in case (op, typ) of
                ("-", "int") -> code ++ [Ineg]
                ("-", "long") -> code ++ [Lneg]
                ("-", "float") -> code ++ [Fneg]
                ("-", "double") -> code ++ [Dneg]
                ("~", "int") -> code ++ [Iconst_m1, Ixor]
                ("~", "long") -> code ++ [Iconst_m1, I2l, Lxor]
                ("!", "boolean") -> code ++ [Ifne 7, Iconst_1, Goto 4, Iconst_0]
                ("++", "int") -> [Iinc varIdx 1, loadInstr typ varIdx]
                ("--", "int") -> [Iinc varIdx (-1), loadInstr typ varIdx]
                (_, _) -> error ("Error: The operator " ++ op ++ " isn't supported on expressions of type " ++ typ)

    generateCode cs (StmtExprExpr (stmtExpr)) = generateCode cs stmtExpr

    generateCode _ x = error ("Code generation for " ++ (show x) ++ " missing") :: [Code]

generateIfExprCode :: CompilerState -> Expr -> Int -> [Code]
--generateIfExprCode a b c d | trace ("ifexpr: " ++ show c) False = undefined
generateIfExprCode cs (TypedExpr (e, _)) jumploc = generateIfExprCode cs e jumploc
generateIfExprCode cs (Binary (op, l, r)) jumploc =
    let leftCode = generateCode cs l
        rightCode = generateCode cs r
        ltyp = getTypeFromExpr l
        rtyp = getTypeFromExpr r
        (wtyp, code) = unifyTypes leftCode rightCode ltyp rtyp
        in case (op, wtyp) of
            ("!=", "int") -> code ++ [If_icmpeq jumploc]
            ("==", "int") -> code ++ [If_icmpne jumploc]
            ("<", "int") -> code ++ [If_icmpge jumploc]
            (">", "int") -> code ++ [If_icmple jumploc]
            ("<=", "int") -> code ++ [If_icmpgt jumploc]
            (">=", "int") -> code ++ [If_icmplt jumploc]
            ("!=", "long") -> code ++ [Lcmp, Ifeq jumploc]
            ("==", "long") -> code ++ [Lcmp, Ifne jumploc]
            ("<", "long") -> code ++ [Lcmp, Ifge jumploc]
            (">", "long") -> code ++ [Lcmp, Ifle jumploc]
            ("<=", "long") -> code ++ [Lcmp, Ifgt jumploc]
            (">=", "long") -> code ++ [Lcmp, Iflt jumploc]
            ("!=", "float") -> code ++ [Fcmpg, Ifeq jumploc]
            ("==", "float") -> code ++ [Fcmpg, Ifne jumploc]
            ("<", "float") -> code ++ [Fcmpg, Ifge jumploc]
            (">", "float") -> code ++ [Fcmpl, Ifle jumploc]
            ("<=", "float") -> code ++ [Fcmpg, Ifgt jumploc]
            (">=", "float") -> code ++ [Fcmpl, Iflt jumploc]
            ("!=", "double") -> code ++ [Dcmpg, Ifeq jumploc]
            ("==", "double") -> code ++ [Dcmpg, Ifne jumploc]
            ("<", "double") -> code ++ [Dcmpg, Ifge jumploc]
            (">", "double") -> code ++ [Dcmpl, Ifle jumploc]
            ("<=", "double") -> code ++ [Dcmpg, Ifgt jumploc]
            (">=", "double") -> code ++ [Dcmpl, Iflt jumploc]
generateIfExprCode cs (Unary (op, expr)) jumploc =
    let exprCode = generateCode cs expr
        in case op of
            "!" -> exprCode ++ [Ifne jumploc]
generateIfExprCode cs expr jumploc = (generateCode cs expr) ++ [Ifeq jumploc]

getVarIndexFromExpr :: CompilerState -> Expr -> Int
getVarIndexFromExpr cs (TypedExpr (e, _)) = getVarIndexFromExpr cs e
getVarIndexFromExpr cs (LocalOrFieldVar (name)) =
    let (typ, idx) = findVar name (currentVars cs)
        in idx
getVarIndexFromExpr _ e = error ("ICE: Is not a variable: " ++ show e)

-- | Generates code that widens the given expression codes into other code
--   When the returned code has been executed the stack contains two values that are of the widened type
unifyTypes :: [Code] -- ^ Code of the left expression
    -> [Code] -- ^ Code of the right expression
    -> Type -- ^ Type of the left expression
    -> Type -- ^ Type of the right expression
    -> (Type, [Code]) -- ^ Widened type and the code
unifyTypes lcode rcode "boolean" rtyp = unifyTypes lcode rcode "int" rtyp
unifyTypes lcode rcode ltyp "boolean" = unifyTypes lcode rcode ltyp "int"
unifyTypes lcode rcode "char" rtyp = unifyTypes lcode rcode "int" rtyp
unifyTypes lcode rcode ltyp "char" = unifyTypes lcode rcode ltyp "int"
unifyTypes lcode rcode "byte" rtyp = unifyTypes lcode rcode "int" rtyp
unifyTypes lcode rcode ltyp "byte" = unifyTypes lcode rcode ltyp "int"
unifyTypes lcode rcode "short" rtyp = unifyTypes lcode rcode "int" rtyp
unifyTypes lcode rcode ltyp "short" = unifyTypes lcode rcode ltyp "int"
unifyTypes lcode rcode "int" "int" = ("int", lcode ++ rcode)
unifyTypes lcode rcode "int" "long" = ("long", lcode ++ [I2l] ++ rcode)
unifyTypes lcode rcode "int" "float" = ("float", lcode ++ [I2f] ++ rcode)
unifyTypes lcode rcode "int" "double" = ("double", lcode ++ [I2d] ++ rcode)
unifyTypes lcode rcode "long" "int" = ("long", lcode ++ rcode ++ [I2l])
unifyTypes lcode rcode "long" "long" = ("long", lcode ++ [I2l] ++ rcode)
unifyTypes lcode rcode "long" "float" = ("double", lcode ++ [L2d] ++ rcode ++ [F2d])
unifyTypes lcode rcode "long" "double" = ("double", lcode ++ [L2d] ++ rcode)
unifyTypes lcode rcode "float" "int" = ("float", lcode ++ rcode ++ [I2f])
unifyTypes lcode rcode "float" "long" = ("double", lcode ++ [F2d] ++ rcode ++ [L2d])
unifyTypes lcode rcode "float" "float" = ("float", lcode ++ rcode)
unifyTypes lcode rcode "float" "double" = ("double", lcode ++ [F2d] ++ rcode)
unifyTypes lcode rcode "double" "int" = ("double", lcode ++ rcode ++ [I2d])
unifyTypes lcode rcode "double" "long" = ("double", lcode ++ rcode ++ [L2d])
unifyTypes lcode rcode "double" "float" = ("double", lcode ++ rcode ++ [F2d])
unifyTypes lcode rcode "double" "double" = ("double", lcode ++ rcode)

-- | Generates code that casts an expression from one type to another
castTo :: [Code] -> Type -> Type -> [Code]
castTo code otype ntype
    | otype == ntype = code
castTo code "boolean" x = castTo code "byte" x
castTo code "byte" "short" = code
castTo code "byte" "int" = code
castTo code "byte" "long" = code ++ [I2l]
castTo code "byte" "float" = code ++ [I2f]
castTo code "byte" "double" = code ++ [I2d]
castTo code "short" "byte" = code ++ [I2b]
castTo code "short" "int" = code
castTo code "short" "long" = code ++ [I2l]
castTo code "short" "float" = code ++ [I2f]
castTo code "short" "double" = code ++ [I2d]
castTo code "int" "byte" = code ++ [I2b]
castTo code "int" "short" = code ++ [I2s]
castTo code "int" "long" = code ++ [I2l]
castTo code "int" "float" = code ++ [I2f]
castTo code "int" "double" = code ++ [I2d]
castTo code "long" "byte" = code ++ [L2i, I2b]
castTo code "long" "short" = code ++ [L2i, I2s]
castTo code "long" "int" = code ++ [L2i]
castTo code "long" "float" = code ++ [L2f]
castTo code "long" "double" = code ++ [L2d]
castTo code "float" "byte" = code ++ [F2i, I2b]
castTo code "float" "short" = code ++ [F2i, I2s]
castTo code "float" "int" = code ++ [F2i]
castTo code "float" "long" = code ++ [F2l]
castTo code "float" "double" = code ++ [F2d]
castTo code "double" "byte" = code ++ [D2i, I2b]
castTo code "double" "short" = code ++ [D2i, I2s]
castTo code "double" "int" = code ++ [D2i]
castTo code "double" "long" = code ++ [D2l]
castTo code "double" "float" = code ++ [D2f]
castTo code _ _ = code ++ [I2l]

storeInstr :: Type -> Int -> Code
storeInstr "int" 0 = Istore_0
storeInstr "int" 1 = Istore_1
storeInstr "int" 2 = Istore_2
storeInstr "int" 3 = Istore_3
storeInstr "int" x = Istore x
storeInstr "float" 0 = Fstore_0
storeInstr "float" 1 = Fstore_1
storeInstr "float" 2 = Fstore_2
storeInstr "float" 3 = Fstore_3
storeInstr "float" x = Fstore x
storeInstr "double" 0 = Dstore_0
storeInstr "double" 1 = Dstore_1
storeInstr "double" 2 = Dstore_2
storeInstr "double" 3 = Dstore_3
storeInstr "double" x = Dstore x
storeInstr "long" 0 = Lstore_0
storeInstr "long" 1 = Lstore_1
storeInstr "long" 2 = Lstore_2
storeInstr "long" 3 = Lstore_3
storeInstr "long" x = Lstore x
-- byte, short, char is the same as int on the jvm
storeInstr "short" x = storeInstr "int" x
storeInstr "char" x = storeInstr "int" x
storeInstr "byte" x = storeInstr "int" x
storeInstr "boolean" x = storeInstr "int" x
-- classes
storeInstr _ 0 = Astore_0
storeInstr _ 1 = Astore_1
storeInstr _ 2 = Astore_2
storeInstr _ 3 = Astore_3
storeInstr _ x = Astore x

loadInstr :: Type -> Int -> Code
loadInstr "int" 0 = Iload_0
loadInstr "int" 1 = Iload_1
loadInstr "int" 2 = Iload_2
loadInstr "int" 3 = Iload_3
loadInstr "int" x = Iload x
loadInstr "float" 0 = Fload_0
loadInstr "float" 1 = Fload_1
loadInstr "float" 2 = Fload_2
loadInstr "float" 3 = Fload_3
loadInstr "float" x = Fload x
loadInstr "double" 0 = Dload_0
loadInstr "double" 1 = Dload_1
loadInstr "double" 2 = Dload_2
loadInstr "double" 3 = Dload_3
loadInstr "double" x = Dload x
loadInstr "long" 0 = Lload_0
loadInstr "long" 1 = Lload_1
loadInstr "long" 2 = Lload_2
loadInstr "long" 3 = Lload_3
loadInstr "long" x = Lload x
-- byte, short, char is the same as int on the jvm
loadInstr "short" x = loadInstr "int" x
loadInstr "char" x = loadInstr "int" x
loadInstr "byte" x = loadInstr "int" x
loadInstr "boolean" x = loadInstr "int" x
-- classes
loadInstr _ 0 = Aload_0
loadInstr _ 1 = Aload_1
loadInstr _ 2 = Aload_2
loadInstr _ 3 = Aload_3
loadInstr _ x = Aload x
