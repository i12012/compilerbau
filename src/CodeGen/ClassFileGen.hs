module CodeGen.ClassFileGen where

import AbsSyn
import ClassFormat
import CodeGen.Data
import CodeGen.CpLookup
import CodeGen.ByteCodeGen
import CodeGen.Preparation
import Mangle

import Debug.Trace

generateClassFile :: ClassDecl -> ClassFile
generateClassFile cls@(ClassDecl (name, fields, methods)) =
    let cs = prepareClassDecl cls
        cp = constantPool cs
        field_infos = map (generateFieldInfo cs) fields
        cons = case filter isConstructor methods of
            [] -> [defaultConstructor]
            x -> x
        meths = filter (\x -> not$isConstructor x) methods
        method_infos = map (generateMethodInfo cs) methods
        cons_infos = map (generateConsInfo cs) cons
        in ClassFile {
               magic = Magic,
               minver = MinorVersion 0,
               maxver = MajorVersion 48,
               count_cp = (length cp)+1,
               array_cp = trace (show cp) cp,
               acfg = AccessFlags [acc_Super_Synchronized],
               this = ThisClass (indexOfClass cp name),
               super = SuperClass (indexOfClass cp "java/lang/Object"),
               count_interfaces = 0,
               array_interfaces = [],
               count_fields = length field_infos,
               array_fields = field_infos,
               count_methods = (length method_infos)+(length cons_infos),
               array_methods = cons_infos ++ method_infos,
               count_attributes = 0,
               array_attributes = []
           }


generateFieldInfo :: CompilerState -> FieldDecl -> Field_Info
generateFieldInfo cs f@(FieldDecl (typ, name)) =
    let cp = constantPool cs
        in Field_Info {
    af_fi = AccessFlags [acc_Public],
    index_name_fi = indexOfText cp name,
    index_descr_fi = indexOfText cp (mangleType typ),
    tam_fi = 0,
    array_attr_fi = []}

generateConsInfo :: CompilerState -> MethodDecl -> Method_Info
generateConsInfo cs m@(MethodDecl (rettype, name, params, stmt)) =
    let csUpdated = cs { currentVars = makeVars m }
        cp = constantPool cs
        attrs = [codeAttributeCons csUpdated m]
        in Method_Info {
               af_mi = AccessFlags [acc_Public],
               index_name_mi = indexOfText cp name,
               index_descr_mi = indexOfText cp (mangleMethodDesc (rettype:(map fst params))),
               tam_mi = (length attrs),
               array_attr_mi = attrs
           }

generateMethodInfo :: CompilerState -> MethodDecl -> Method_Info
generateMethodInfo cs m@(MethodDecl (rettype, name, params, stmt)) =
    let csUpdated = cs { currentVars = makeVars m }
        cp = constantPool cs
        attrs = [codeAttribute csUpdated m]
        in Method_Info {
               af_mi = AccessFlags [acc_Public],
               index_name_mi = indexOfText cp name,
               index_descr_mi = indexOfText cp (mangleMethodDesc (rettype:(map fst params))),
               tam_mi = (length attrs),
               array_attr_mi = attrs
           }

codeAttribute :: CompilerState -> MethodDecl -> Attribute_Info
codeAttribute cs m@(MethodDecl (rettype, name, params, stmt)) =
    let cp = constantPool cs
        vars = currentVars cs
        varCount = if rettype == "void" then (length vars)+1 -- Number of variables + this
                                        else (length vars)+2 -- Number of variables + this + return value
        gendcode = generateCode cs stmt
        code = case (rettype, firstBlockEndsWithReturn stmt) of
            ("void", False) -> gendcode ++ [CodeReturn]
            _ -> gendcode
        in AttributeCode {
    index_name_attr = indexOfText cp "Code",
    tam_len_attr = -1, -- Will be calculated later by ByteCodeGen module
    len_stack_attr = stackSize stmt, -- Number of stack variables
    len_local_attr = varCount,
    tam_code_attr = -1, -- Will be calculated later by ByteCodeGen module
    array_code_attr = trace ("Code for " ++ name ++ ":" ++ show code) code,
    -- array_code_attr = code,
    tam_ex_attr = 0,
    array_ex_attr = [],
    tam_atrr_attr = 0,
    array_attr_attr = []
}

codeAttributeCons cs m@(MethodDecl (rettype, name, params, stmt)) =
    let cp = constantPool cs
        vars = currentVars cs
        consCode = defaultConstructorCode
        varCount = if rettype == "void" then (length vars)+1 -- Number of variables + this
                                        else (length vars)+2 -- Number of variables + this + return value
        gendcode = consCode ++ (generateCode cs stmt)
        code = case (rettype, firstBlockEndsWithReturn stmt) of
            ("void", False) -> gendcode ++ [CodeReturn]
            _ -> gendcode
        in AttributeCode {
    index_name_attr = indexOfText cp "Code",
    tam_len_attr = -1, -- Will be calculated later by ByteCodeGen module
    len_stack_attr = max 2 (stackSize stmt), -- Number of stack variables
    len_local_attr = varCount,
    tam_code_attr = -1, -- Will be calculated later by ByteCodeGen module
    array_code_attr = trace ("Code for " ++ name ++ ":" ++ show code) code,
    -- array_code_attr = code,
    tam_ex_attr = 0,
    array_ex_attr = [],
    tam_atrr_attr = 0,
    array_attr_attr = []
}

instance StackSize Stmt where
    stackSize (TypedStmt (s, _)) = stackSize s
    stackSize (LocalVarDecl _) = 1
    stackSize (Block stmts) = maxStackSize stmts
    stackSize (Return Nothing) = 0
    stackSize (Return (Just e)) = stackSize e
    stackSize (While (e, stmt)) = max (1 + stackSize e) (stackSize stmt)
    stackSize (If (e, stmt, Nothing)) = max (1 + stackSize e) (stackSize stmt)
    stackSize (If (e, stmt, Just elseStmt))
        = maximum [1 + stackSize e, stackSize stmt, stackSize elseStmt]
    stackSize (StmtExprStmt s) = stackSize s

instance StackSize StmtExpr where
    stackSize (Assign (_, e)) = stackSize e
    stackSize (New (_, exprs)) = maximum [2, 1+length exprs, 1+maxStackSize exprs]
    stackSize (MethodCall (e, _, exprs)) = (stackSize e) + (max (length exprs) (maxStackSize exprs))
    stackSize (TypedStmtExpr (s, _)) = stackSize s

instance StackSize Expr where
    stackSize (This) = 1
    stackSize (Super) = 1
    stackSize (LocalOrFieldVar _) = 1
    stackSize (InstVar (e, _)) = 1 + stackSize e
    stackSize (Unary (_, e)) = stackSize e
    stackSize (Binary (_, e1, e2)) = 1 + (max (stackSize e1) (stackSize e2))
    stackSize (Integer _) = 1
    stackSize (Bool _) = 1
    stackSize (Char _) = 1
    stackSize (String _) = 1
    stackSize (Jnull) = 1
    stackSize (StmtExprExpr e) = stackSize e
    stackSize (TypedExpr (e, _)) = stackSize e

firstBlockEndsWithReturn :: Stmt -> Bool
firstBlockEndsWithReturn (TypedStmt (s, _)) = firstBlockEndsWithReturn s
firstBlockEndsWithReturn (Block []) = False
firstBlockEndsWithReturn (Block stmts) =
    case stmts!!((length stmts)-1) of
        (Return _) -> True
        _ -> False

isConstructor :: MethodDecl -> Bool
isConstructor (MethodDecl (_, "<init>", _, _)) = True
isConstructor _ = False