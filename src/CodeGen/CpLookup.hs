module CodeGen.CpLookup where

import ClassFormat
import Mangle

import Data.List

indexOfInfo :: CP_Infos -> CP_Info -> String -> Int
indexOfInfo cp info errorText =
    case elemIndex info cp of
        Nothing -> error (errorText) :: Int
        Just i -> i+1

indexOfText cp text = indexOfInfo cp (utf8Info text) ("ICE: Utf8_Info " ++ text ++ " not found")

indexOfString cp text =
    let idx = indexOfText cp text
        in indexOfInfo cp (stringInfo idx) ("ICE: String_Info for " ++ text ++ " not found")

indexOfClass cp name =
    let idx = indexOfText cp name
        in indexOfInfo cp (classRefInfo idx) ("ICE: ClassRef_Info for " ++ name ++ " not found")

indexOfInt cp int = indexOfInfo cp (integerInfo int) ("ICE: Integer_Info for " ++ (show int) ++ " not found")

indexOfNameAndType cp name typ =
    let nameIdx = indexOfText cp name
        typIdx = indexOfText cp (typ)
        in indexOfInfo cp (nameAndTypeInfo nameIdx typIdx)
            ("ICE: NameAndType_Info for " ++ name ++ "," ++ typ ++ " not found")

indexOfField cp cls name typ =
    let clsIdx = indexOfClass cp cls
        natIdx = indexOfNameAndType cp name (mangleType typ)
        in indexOfInfo cp (fieldRefInfo clsIdx natIdx)
            ("ICE: FieldRef_Info for " ++ name ++ " on " ++ cls ++ " missing")

indexOfMethod cp cls name signature = 
    let typIdx = indexOfClass cp cls
        nameandtypeIdx = indexOfNameAndType cp name signature
        in indexOfInfo cp (methodRefInfo typIdx nameandtypeIdx)
            ("ICE: MethofRef_Info for " ++ name ++ " in class " ++ cls ++ " not found")

indexOfThisField :: CP_Infos -> String -> Int
indexOfThisField cp name =
    let nameIdx = (indexOfText cp name)
        classIdx = thisClassIdx -- Current class is at index 1 in the constant pool
        in case findIndex (matchField cp classIdx nameIdx) cp of
            Nothing -> error ("ICE: FieldRef_Info for " ++ name ++ " on this class not found") :: Int
            Just i -> i

matchField :: CP_Infos -> Int -> Int -> CP_Info -> Bool
matchField cp classIdx nameIdx f@(FieldRef_Info {}) =
    (index_name_cp f) == (classIdx) && ((nameIdx) == index_name_cp (cp!!((index_nameandtype_cp f)-1)))
matchField _ _ _ _ = False