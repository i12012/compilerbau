{-# LANGUAGE NamedFieldPuns #-}
module CodeGen.ClassFileIO where

import ClassFormat;

import Data.ByteString (unpack);
import Data.Binary;
import Data.Bits;
import Data.Int;
import qualified Data.ByteString.UTF8 as UTF8;

writeByteCode :: ClassFile -> FilePath -> IO ()
writeByteCode cls path = encodeFile path cls

instance Binary ClassFile where
    put cf = do
        put $ magic cf
        put $ minver cf
        put $ maxver cf
        putInt16 $ count_cp cf
        mapM_ put (array_cp cf)
        put $ acfg cf
        put $ this cf
        put $ super cf
        putInt16 $ count_interfaces cf
        mapM_ put (array_interfaces cf)
        putInt16 $ count_fields cf
        mapM_ put (array_fields cf)
        putInt16 $ count_methods cf
        mapM_ put (array_methods cf)
        putInt16 $ count_attributes cf
        mapM_ put (array_attributes cf)
    get = undefined

instance Binary Magic where
    put _ = do
        put (3405691582 :: Word32) -- 0xCAFEBABE
    get = undefined

instance Binary MinorVersion where
    put (MinorVersion v) = putWord16 v
    get = undefined

instance Binary MajorVersion where
    put (MajorVersion v) = putWord16 v
    get = undefined

instance Binary AccessFlags where
    put (AccessFlags f) = putWord16 (foldl (.|.) 0 f)
    get = undefined

instance Binary ThisClass where
    put (ThisClass idx) = putWord16 idx
    get = undefined

instance Binary SuperClass where
    put (SuperClass idx) = putWord16 idx
    get = undefined

instance Binary CP_Info where
    put (info)
        | tag_cp info == TagClass = do
            put $ tag_cp info
            putInt16 (index_cp info)
        | tag_cp info == TagFieldRef = do
            put $ tag_cp info
            putInt32 (index_name_cp info)
            putInt32 (index_nameandtype_cp info)
        | tag_cp info == TagMethodRef = do
            put $ tag_cp info
            putInt16 (index_name_cp info)
            putInt16 (index_nameandtype_cp info)
        | tag_cp info == TagInterfaceMethodRef = undefined
        | tag_cp info == TagString = do
            put $ tag_cp info
            putInt32 (numi_cp info)
        | tag_cp info == TagInteger = do
            put $ tag_cp info
            put $ numi_cp info
        | tag_cp info == TagFloat = do
            put $ tag_cp info
            put $ numf_cp info
        | tag_cp info == TagDouble = do
            put $ tag_cp info
            putInt32 (numi_d1_cp info)
            putInt32 (numi_d2_cp info)
        | tag_cp info == TagLong = do
            put $ tag_cp info
            putInt32 (numi_l1_cp info)
            putInt32 (numi_l2_cp info)
        | tag_cp info == TagNameAndType = do
            put $ tag_cp info
            putInt16 (index_name_cp info)
            putInt16 (index_descr_cp info)
        | tag_cp info == TagUtf8 = do
            put $ tag_cp info
            putInt16 (tam_cp info)
            putUtf8 (cad_cp info)
    get = undefined

instance Binary Tag where
    put TagClass = putWord8 7
    put TagFieldRef = putWord8 9
    put TagMethodRef = putWord8 10
    put TagInterfaceMethodRef = putWord8 11
    put TagString = putWord8 8
    put TagInteger = putWord8 3
    put TagFloat = putWord8 4
    put TagLong = putWord8 5
    put TagDouble = putWord8 6
    put TagNameAndType = putWord8 12
    put TagUtf8 = putWord8 1
    get = undefined

instance Binary Interface where
    put = undefined
    get = undefined

instance Binary Field_Info where
    put field = do
        put $ af_fi field
        putInt16 $ index_name_fi field
        putInt16 $ index_descr_fi field
        putInt16 $ tam_fi field
        mapM_ put (array_attr_fi field)
    get = undefined

instance Binary Method_Info where
    put met = do
        put $ af_mi met
        putInt16 $ index_name_mi met
        putInt16 $ index_descr_mi met
        putInt16 $ tam_mi met
        mapM_ put (array_attr_mi met)
    get = undefined

instance Binary Attribute_Info where
    put ai@(AttributeCode {}) = do
        putInt16 $ index_name_attr ai
        putInt32 $ calcAttrSize ai
        putInt16 $ len_stack_attr ai
        putInt16 $ len_local_attr ai
        putInt32 $ calcCodeSize (array_code_attr ai)
        mapM_ put (array_code_attr ai)
        putInt16 $ tam_ex_attr ai
        mapM_ (\(a,b,c,d) -> do
            putInt16 a
            putInt16 b
            putInt16 c
            putInt16 d) (array_ex_attr ai)
        putInt16 $ tam_atrr_attr ai
        mapM_ put (array_attr_attr ai)

    get = undefined

instance Binary Code where
    put (Aload i) = do
        putWord8 25
        putWord8 (toWord8 i)
    put Aload_0 = putWord8 42
    put Aload_1 = putWord8 43
    put Aload_2 = putWord8 44
    put Aload_3 = putWord8 45
    put (Invokespecial i) = do
        putWord8 183
        putInt16 i
    put CodeReturn = putWord8 177
    put (Ret i) = do
        putWord8 169
        putWord8 $ toWord8 i
    put (Istore i) = do
        putWord8 54
        putWord8 $ toWord8 i
    put (Istore_0) = putWord8 59
    put (Istore_1) = putWord8 60
    put (Istore_2) = putWord8 61
    put (Istore_3) = putWord8 62
    put (Iconst_m1) = putWord8 2
    put (Iconst_0) = putWord8 3
    put (Iconst_1) = putWord8 4
    put (Iconst_2) = putWord8 5
    put (Iconst_3) = putWord8 6
    put (Iconst_4) = putWord8 7
    put (Iconst_5) = putWord8 8
    put (Ldc i) = do
        putWord8 18
        putWord8$toWord8 i
    put (Ldc_w i) = do
        putWord8 19
        putWord16 i
    put (Sipush i) = do
        putWord8 17
        putInt16 i
    put (Ireturn) = putWord8 172
    put (Dreturn) = putWord8 175
    put (Freturn) = putWord8 174
    put (Areturn) = putWord8 176
    put (Lreturn) = putWord8 173
    put (Ifeq i) = do
        putWord8 153
        putWord16 i
    put (Ifne i) = do
        putWord8 154
        putWord16 i
    put (Iflt i) = do
        putWord8 155
        putWord16 i
    put (Ifge i) = do
        putWord8 156
        putWord16 i
    put (Ifgt i) = do
        putWord8 157
        putWord16 i
    put (Ifle i) = do
        putWord8 158
        putWord16 i
    put (If_icmpeq i) = do
        putWord8 159
        putWord16 i
    put (If_icmpne i) = do
        putWord8 160
        putWord16 i
    put (If_icmplt i) = do
        putWord8 161
        putWord16 i
    put (If_icmpge i) = do
        putWord8 162
        putWord16 i
    put (If_icmpgt i) = do
        putWord8 163
        putWord16 i
    put (If_icmple i) = do
        putWord8 164
        putWord16 i
    put (Goto x) = do
        putWord8 167
        putInt16 x
    put (Iload_0) = putWord8 26
    put (Iload_1) = putWord8 27
    put (Iload_2) = putWord8 28
    put (Iload_3) = putWord8 29
    put (Iload i) = do
        putWord8 21
        putWord8$toWord8 i
    put (Bipush i) = do
        putWord8 16
        putInt8 i
    put (Aconst_null) = putWord8 1
    put (Getfield x) = do
        putWord8 180
        putWord16 x
    put (Putfield x) = do
        putWord8 181
        putWord16 x
    put (Iadd) = putWord8 96
    put (Isub) = putWord8 100
    put (Imul) = putWord8 104
    put (Idiv) = putWord8 108
    put (Ineg) = putWord8 116
    put (Iand) = putWord8 126
    put (Ior) = putWord8 128
    put (Ixor) = putWord8 130
    put (Irem) = putWord8 112
    put (Ladd) = putWord8 97
    put (Lsub) = putWord8 101
    put (Lmul) = putWord8 105
    put (Ldiv) = putWord8 109
    put (Lneg) = putWord8 117
    put (Land) = putWord8 127
    put (Lor) = putWord8 129
    put (Lxor) = putWord8 131
    put (Lrem) = putWord8 112
    put (Fadd) = putWord8 98
    put (Fsub) = putWord8 102
    put (Fmul) = putWord8 106
    put (Fdiv) = putWord8 110
    put (Fneg) = putWord8 118
    put (Frem) = putWord8 114
    put (Dadd) = putWord8 99
    put (Dsub) = putWord8 103
    put (Dmul) = putWord8 107
    put (Ddiv) = putWord8 111
    put (Dneg) = putWord8 119
    put (Drem) = putWord8 115
    put (Ishl) = putWord8 120
    put (Lshl) = putWord8 121
    put (Ishr) = putWord8 122
    put (Lshr) = putWord8 123
    put (Iushr) = putWord8 124
    put (Lushr) = putWord8 125
    put (Lcmp) = putWord8 148
    put (Fcmpl) = putWord8 149
    put (Fcmpg) = putWord8 150
    put (Dcmpl) = putWord8 151
    put (Dcmpg) = putWord8 152
    put (I2b) = putWord8 145
    put (I2c) = putWord8 146
    put (I2s) = putWord8 147
    put (I2d) = putWord8 135
    put (I2f) = putWord8 134
    put (I2l) = putWord8 133
    put (L2i) = putWord8 136
    put (L2f) = putWord8 137
    put (L2d) = putWord8 138
    put (F2d) = putWord8 141
    put (F2i) = putWord8 139
    put (F2l) = putWord8 140
    put (D2f) = putWord8 144
    put (D2i) = putWord8 142
    put (D2l) = putWord8 143
    put (Astore_0) = putWord8 75
    put (Astore_1) = putWord8 76
    put (Astore_2) = putWord8 77
    put (Astore_3) = putWord8 78
    put (Astore x) = do
        putWord8 58
        putWord8$toWord8 x
    put (Fstore_0) = putWord8 67
    put (Fstore_1) = putWord8 68
    put (Fstore_2) = putWord8 69
    put (Fstore_3) = putWord8 70
    put (Fstore x) = do
        putWord8 56
        putWord8$toWord8 x
    put (Dstore_0) = putWord8 71
    put (Dstore_1) = putWord8 72
    put (Dstore_2) = putWord8 73
    put (Dstore_3) = putWord8 74
    put (Dstore x) = do
        putWord8 57
        putWord8$toWord8 x
    put (Lstore_0) = putWord8 63
    put (Lstore_1) = putWord8 64
    put (Lstore_2) = putWord8 65
    put (Lstore_3) = putWord8 66
    put (Lstore x) = do
        putWord8 55
        putWord8$toWord8 x
    put (Fload_0) = putWord8 34
    put (Fload_1) = putWord8 35
    put (Fload_2) = putWord8 36
    put (Fload_3) = putWord8 37
    put (Fload x) = do
        putWord8 23
        putWord8$toWord8 x
    put (Dload_0) = putWord8 38
    put (Dload_1) = putWord8 39
    put (Dload_2) = putWord8 40
    put (Dload_3) = putWord8 41
    put (Dload x) = do
        putWord8 24
        putWord8$toWord8 x
    put (Lload_0) = putWord8 30
    put (Lload_1) = putWord8 31
    put (Lload_2) = putWord8 32
    put (Lload_3) = putWord8 33
    put (Lload x) = do
        putWord8 22
        putWord8$toWord8 x
    put (CodeNew i) = do
        putWord8 (187)
        putWord16 (i)
    put (Invokevirtual i) = do
        putWord8 (182)
        putWord16 (i)
    put (Dup) = putWord8 89
    put (Iinc i v) = do
        putWord8 132
        putWord8$toWord8 i
        putInt8 v
    get = undefined

calcAttrSize :: Attribute_Info -> Int
calcAttrSize (AttributeCode { array_code_attr, tam_ex_attr, array_attr_attr })
    = 2+2+4+(calcCodeSize array_code_attr)+2+(8*tam_ex_attr)+2+(foldl (+) 0 (map calcAttrSize array_attr_attr))
calcAttrSize _ = undefined

-- Helper functions for less code!

toWord8 :: Integral a => a -> Word8
toWord8 x = fromIntegral x :: Word8

putWord16 :: Integral a => a -> Put
putWord16 a = put (fromIntegral a :: Word16)

putWord32 :: Integral a => a -> Put
putWord32 a = put (fromIntegral a :: Word32)

putWord64 :: Integral a => a -> Put
putWord64 a = put (fromIntegral a :: Word64)

putInt8 :: Integral a => a -> Put
putInt8 a = put (fromIntegral a :: Int8)

putInt16 :: Integral a => a -> Put
putInt16 a = put (fromIntegral a :: Int16)

putInt32 :: Integral a => a -> Put
putInt32 a = put (fromIntegral a :: Int32)

putInt64 :: Integral a => a -> Put
putInt64 a = put (fromIntegral a :: Int64)

putUtf8 :: String -> Put
putUtf8 text =
    let encoded = UTF8.fromString text
        bytes = unpack encoded
        in mapM_ put bytes