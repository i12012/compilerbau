module CodeGen.Preparation where

import AbsSyn
import ClassFormat
import CodeGen.Data
import Mangle

import Data.List
import Control.Monad.State
import Data.Char (ord)

-- |Returns the index of a constant in the constant pool. If the constant isn't in the constant pool it is appended and
--  the new index is returned
findOrPut :: CP_Info -> State CompilerState Int
findOrPut x = do
    list <- getConstantPool
    case elemIndex x list of
        Nothing -> do
            putConstantPool (list ++ [x])
            return ((length list)+1)
        Just i ->
            return (i+1)

prepareClassDecl :: ClassDecl -> CompilerState
prepareClassDecl classDecl =
    execState
        (makeConstantPool classDecl)
        (CompilerState { targetClass = classDecl, constantPool = [], fields = [], currentVars = [], refdClasses = [] })

instance ConstantPoolGenerator ClassDecl where
    makeConstantPool cls@(ClassDecl (name, fields, methods)) = do
        nameIdx <- findOrPut (utf8Info name)
        findOrPut (classRefInfo nameIdx)
        findOrPut (utf8Info "Code")
        consIdx <- findOrPut (utf8Info "<init>")
        consDescIdx <- findOrPut (utf8Info "()V")
        objIdx <- findOrPut (utf8Info "java/lang/Object")
        objClassIdx <- findOrPut (classRefInfo objIdx)
        consNameTypeIdx <- findOrPut (nameAndTypeInfo consIdx consDescIdx)
        findOrPut (methodRefInfo objClassIdx consNameTypeIdx)
        generateConstantPool fields
        generateConstantPool methods

instance ConstantPoolGenerator FieldDecl where
    makeConstantPool (FieldDecl (typ, name)) = do
        findOrPut (utf8Info name)
        findOrPut (utf8Info (mangleType typ))
        return ()

instance ConstantPoolGenerator MethodDecl where
    makeConstantPool method@(MethodDecl (retType, name, params, stmt)) = do
        let vars = makeVars method
        putCurrentVars vars
        findOrPut (utf8Info (name))
        findOrPut (utf8Info (mangleMethodDecl method))
        makeConstantPool stmt

instance ConstantPoolGenerator Stmt where
    makeConstantPool (Block stmts) = generateConstantPool stmts
    makeConstantPool (Return (Just e)) = gcpExpr e ""
    makeConstantPool (While (e, s)) = do
        gcpExpr e ""
        makeConstantPool s
    makeConstantPool (If (e, thenStmt, Just elseStmt)) = do
        gcpExpr e ""
        makeConstantPool thenStmt
        makeConstantPool elseStmt
    makeConstantPool (If (e, thenStmt, Nothing)) = do
        gcpExpr e ""
        makeConstantPool thenStmt
    makeConstantPool (StmtExprStmt e) = gcpStmtExpr e ""
    makeConstantPool (TypedStmt (s, _)) = makeConstantPool s
    makeConstantPool _ = return ()

-- |Generates the constant pool by looking through the given expression for constants
gcpExpr :: Expr -> Type -> State CompilerState ()
gcpExpr (TypedExpr (expr, typ)) _ = gcpExpr expr typ
gcpExpr (LocalOrFieldVar name) typ = do
    vars <- getCurrentVars
    case lookup name vars of
        Nothing -> do
            nameIdx <- findOrPut (utf8Info name)
            typeIdx <- findOrPut (utf8Info (mangleType  typ))
            let classRefIdx = thisClassIdx -- This is at index 1 in cp
            natIdx <- findOrPut (nameAndTypeInfo nameIdx typeIdx)
            fieldRefIdx <- findOrPut (fieldRefInfo classRefIdx natIdx)
            appendField (name, fieldRefIdx)
            return ()
        Just _ -> return  ()
gcpExpr (InstVar (e, name)) varType = do
    let instType = getTypeFromExpr e
    nameIdx <- findOrPut (utf8Info (mangleType instType))
    classRef <- findOrPut (classRefInfo (nameIdx))
    varNameIdx <- findOrPut (utf8Info (name))
    varTypeIdx <- findOrPut (utf8Info (mangleType varType))
    natIdx <- findOrPut (nameAndTypeInfo varNameIdx varTypeIdx)
    findOrPut (fieldRefInfo classRef natIdx)
    return ()
gcpExpr (StmtExprExpr (stmtExpr)) t = gcpStmtExpr stmtExpr t
gcpExpr (Unary (_, expr)) t = gcpExpr expr t
gcpExpr (Binary (_, expr1, expr2)) t = do
    gcpExpr expr1 t
    gcpExpr expr2 t
gcpExpr (This) typ = do
    nameIdx <- findOrPut (utf8Info (mangleType typ))
    findOrPut (classRefInfo nameIdx)
    return ()
gcpExpr (Integer i) _
    | (i <= -32768) || (i >= 32767) = do
    findOrPut (integerInfo (fromInteger (i)))
    return ()
    | otherwise = return ()
gcpExpr (Char c) _ = gcpExpr (Integer (toInteger (ord c))) "int"
gcpExpr (String str) _ = do
    findOrPut (utf8Info (str))
    return ()
gcpExpr (Jnull) _ = return ()

-- |Generates the constant pool by looking through the given StmtExpr.
gcpStmtExpr :: StmtExpr -> Type -> State CompilerState ()
gcpStmtExpr (Assign (_, e)) _ = do
    gcpExpr e ""
gcpStmtExpr (New (typ, exprs)) _ = do
    -- Get class reference
    typeIdx <- findOrPut (utf8Info (mangleClassName typ))
    classIdx <- findOrPut (classRefInfo (typeIdx))
    -- Index of constructor name
    constrIdx <- findOrPut (utf8Info ("<init>"))
    -- Constructor signature
    sigIdx <- findOrPut (utf8Info (mangleMethodCall exprs "void"))
    -- Put name/type combination
    nameTypeIdx <- findOrPut (nameAndTypeInfo constrIdx sigIdx)
    -- Put method ref
    findOrPut (methodRefInfo classIdx nameTypeIdx)
    return ()
gcpStmtExpr (MethodCall (e, name, params)) retType = do
    let typ = mangleClassName (getTypeFromExpr e)
    typeIdx <- findOrPut (utf8Info typ)
    classIdx <- findOrPut (classRefInfo (typeIdx))
    -- Method name
    methodIdx <- findOrPut (utf8Info name)
    -- Method signature
    sigIdx <- findOrPut (utf8Info (mangleMethodCall params retType))
    nameTypeIdx <- findOrPut (nameAndTypeInfo methodIdx sigIdx)
    methodRefIdx <- findOrPut (methodRefInfo classIdx nameTypeIdx)

    return ()
gcpStmtExpr (TypedStmtExpr (e, typ)) _ = gcpStmtExpr e typ

makeVars :: MethodDecl -> Vars
makeVars (MethodDecl (_, _, params, stmt)) =
    let paramVars = (foldl (putVar) [] params)
        in putStmtVars paramVars stmt

putStmtVars :: Vars -> Stmt -> Vars
putStmtVars vars (Block stmts) = foldl putStmtVars vars stmts
putStmtVars vars (While (_, stmt)) = putStmtVars vars stmt
putStmtVars vars (If (_, stmt1, Just stmt2)) = foldl putStmtVars vars [stmt1, stmt2]
putStmtVars vars (If (_, stmt1, Nothing)) = putStmtVars vars stmt1
putStmtVars vars (LocalVarDecl decl) = putVar vars decl
putStmtVars vars (TypedStmt (s,_)) = putStmtVars vars s
putStmtVars vars (StmtExprStmt _) = vars
putStmtVars vars (Return _) = vars
--putStmtVars _ s = error ("Variable detection: Unknown statement " ++ (show s))

putVar :: Vars -> (Type, String) -> Vars
putVar vars (typ, name) =
    case lookup name vars of
        Nothing -> ((name, (typ, (length vars)+1)):vars)
        Just i -> vars
