module CodeGen.Data where

import AbsSyn
import ClassFormat

import Control.Monad.State
import Data.List

type VarInfo = (Type, Int)
type VarLookup = (String, VarInfo)
type Vars = [VarLookup]
type Field = (String, Int)
type Fields = [Field] -- List of field names and indices in the constant pool
type MethodReference = (String, (Int))
type MethodReferences = [MethodReference]
type ClassReferenceData = (Int, MethodReferences)
type ClassReference = (String, ClassReferenceData)
type ClassReferences = [ClassReference]

defaultConstructor :: MethodDecl
defaultConstructor = MethodDecl ("void", "<init>", [], Block [])

defaultConstructorStmt :: Stmt
defaultConstructorStmt =
    Block [StmtExprStmt (MethodCall (TypedExpr (LocalOrFieldVar "super", "java.lang.Object"), "<init>", []))]

defaultConstructorCode = [Aload_0, Invokespecial 9]

data CompilerState = CompilerState {
    targetClass :: ClassDecl,
    constantPool :: CP_Infos,
    fields :: Fields,
    currentVars :: Vars,
    refdClasses :: ClassReferences}
    deriving (Show)

getTargetClass :: State CompilerState ClassDecl
getTargetClass = do
    state <- get
    return (targetClass state)

getConstantPool :: State CompilerState CP_Infos
getConstantPool = do
    state <- get
    return (constantPool state)

getFields :: State CompilerState Fields
getFields = do
    state <- get
    return (fields state)

getCurrentVars :: State CompilerState Vars
getCurrentVars = do
    state <- get
    return (currentVars state)

getRefdClasses :: State CompilerState ClassReferences
getRefdClasses = do
    state <- get
    return (refdClasses state)

getThisClass :: State CompilerState ClassReferenceData
getThisClass = do
    state <- get
    let (ClassDecl (thisClassName, _, _)) = targetClass state
        classRef = lookup thisClassName (refdClasses state)
    case classRef of
        Nothing -> return (error ("ICE: Missing reference to this class") :: ClassReferenceData)
        Just x -> return x

putTargetClass :: ClassDecl -> State CompilerState ()
putTargetClass cls = do
    state <- get
    put (state { targetClass = cls })

putConstantPool :: CP_Infos -> State CompilerState ()
putConstantPool cp = do
    state <- get
    put (state { constantPool = cp })

putFields :: Fields -> State CompilerState ()
putFields flds = do
    state <- get
    put (state { fields = flds })

putCurrentVars :: Vars -> State CompilerState ()
putCurrentVars vars = do
    state <- get
    put (state { currentVars = vars })

putRefdClasses :: ClassReferences -> State CompilerState ()
putRefdClasses clsrefs = do
    state <- get
    put (state { refdClasses = clsrefs })

putRefdClass :: ClassReference -> State CompilerState ()
putRefdClass clsRef@(name,_) = do
    refdClasses <- getRefdClasses
    let newClasses = (clsRef:(filter (\(a,_) -> a == name) refdClasses))
    putRefdClasses newClasses

appendField :: Field -> State CompilerState ()
appendField fld = do
    fields <- getFields
    putFields (fields ++ [fld])

appendCurrentVar :: VarLookup -> State CompilerState ()
appendCurrentVar var = do
    vars <- getCurrentVars
    putCurrentVars (vars ++ [var])

appendRefdMethod :: Type -> MethodReference -> State CompilerState ()
appendRefdMethod cls methodRef@(name, _) = do
    classRefs <- getRefdClasses
    (idx, methodRefs) <- case lookup cls classRefs of
        Nothing -> error ("ICE: Missing referenced class " ++ cls)
        Just x -> return x
    let newMethodRefs = nubBy eqfst (methodRef:methodRefs)
    putRefdClass (cls, (idx, newMethodRefs))

findVar :: String -> Vars -> VarInfo
findVar name vars = case lookup name vars of
    Nothing -> error ("Variable " ++ name ++ " not found") :: VarInfo
    Just x -> x

class StackSize a where
    stackSize :: a -> Int
    stackSize = undefined

    maxStackSize :: [a] -> Int
    maxStackSize [] = 0
    maxStackSize items = maximum $ map stackSize items

class ConstantPoolGenerator a where
    -- | Gets the constant pool for the given
    makeConstantPool :: a -> State CompilerState ()
    makeConstantPool item = do
        pool <- get
        let newPool = simpleConstantPool item pool
        put newPool

    simpleConstantPool :: a -> CompilerState -> CompilerState
    simpleConstantPool item cs = execState (makeConstantPool item) cs

    generateConstantPool :: [a] -> State CompilerState ()
    generateConstantPool items = do
        mapM (makeConstantPool) items
        return ()

class CodeGenerator a where
    generateCode :: CompilerState -> a -> [Code]
    generateAllCodes :: CompilerState -> [a] -> [Code]
    generateAllCodes cs as = foldl (++) [] (map (generateCode cs) as)
eqfst a b = (fst a) == (fst b)