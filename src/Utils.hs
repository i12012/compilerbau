module Utils where
import AbsSyn

joinFieldMethodTuples :: ([FieldDecl], [MethodDecl]) -> ([FieldDecl], [MethodDecl]) -> ([FieldDecl], [MethodDecl])
joinFieldMethodTuples (a,b) (c,d) = (a++c, b++d)
