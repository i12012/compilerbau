module Mangle where

import AbsSyn;

-- |Takes a Method declaration and returns its descriptor.
--  The descriptors defines the parameter types and the return type.
mangleMethodDecl :: MethodDecl -> String
mangleMethodDecl (MethodDecl (retType, _, params, _)) =
    "(" ++ (foldl (++) "" (map mangleType (map fst params))) ++ ")"
        ++ (mangleType retType)

-- |Takes a list of typed expressions and a return type to generate the
--  method descriptor used for calling the method.
mangleMethodCall :: [Expr] -> Type -> String
mangleMethodCall [] t = "()" ++ (mangleType t)
mangleMethodCall (xs) retType =
    "(" ++ (foldl (++) "" (map _mangleExpr xs)) ++ ")"
        ++ (mangleType retType)

-- |Takes a list of types and generates the method descriptor.
--  The first element is the return type.
mangleMethodDesc :: [Type] -> String
mangleMethodDesc (t:ts) = "(" ++ (foldl (++) "" (map mangleType ts)) ++ ")" ++ mangleType t

-- |Utility function that mangles the type of a TypedExpr.
_mangleExpr :: Expr -> String
_mangleExpr (TypedExpr (_, typ)) = mangleType typ

-- |Mangles the names of types into the equivalent descriptor.
mangleType :: Type -> String
mangleType "byte" = "B"
mangleType "char" = "C"
mangleType "double" = "D"
mangleType "float" = "F"
mangleType "int" = "I"
mangleType "long" = "J"
mangleType "void" = "V"
mangleType "" = "V"
mangleType "short" = "S"
mangleType "boolean" = "Z"
-- Classes
mangleType o = "L" ++ (mangleClassName o) ++ ";"

-- |Checks if the given type is a class type or a basic type.
isClassType :: Type -> Bool
isClassType = not . isBasicType

-- |Checks if the given type is a basic type or a class type.
--  Currently doesn't work with arrays.
isBasicType :: Type -> Bool
isBasicType "byte" = True
isBasicType "char" = True
isBasicType "double" = True
isBasicType "float" = True
isBasicType "int" = True
isBasicType "long" = True
isBasicType "void" = True
isBasicType "short" = True
isBasicType "boolean" = True
isBasicType _  = False

mangleClassName :: Type -> String
mangleClassName = map (replaceChar '.' '/')

replaceChar :: Char -> Char -> Char -> Char
replaceChar a b c
    | a == c = b
    | otherwise = c
