module ClassFormat where
import qualified Data.ByteString.Lazy as BS

-- class file format
data ClassFile = ClassFile { magic            :: Magic
                           , minver           :: MinorVersion
                           , maxver           :: MajorVersion
                           , count_cp         :: ConstantPool_Count
                           , array_cp         :: CP_Infos
                           , acfg             :: AccessFlags
                           , this             :: ThisClass
                           , super            :: SuperClass
                           , count_interfaces :: Interfaces_Count
                           , array_interfaces :: Interfaces
                           , count_fields     :: Fields_Count
                           , array_fields     :: Field_Infos
                           , count_methods    :: Methods_Count
                           , array_methods    :: Method_Infos
                           , count_attributes :: Attributes_Count
                           , array_attributes :: Attribute_Infos
                           }
                    deriving Show

type CP_Infos        = [CP_Info]
type Interfaces      = [Interface]
type Field_Infos     = [Field_Info]
type Method_Infos    = [Method_Info]
type Attribute_Infos = [Attribute_Info]

data Magic = Magic
        deriving Show

data MinorVersion = MinorVersion {
                        numMinVer :: Int
                    }
        deriving Show

data MajorVersion = MajorVersion {
                        numMaxVer :: Int
                    }
        deriving Show

data CP_Info =
          Class_Info
                { tag_cp                :: Tag
                , index_cp              :: Index_Constant_Pool
                , desc                  :: String
                }
        | FieldRef_Info
                { tag_cp                :: Tag
                , index_name_cp         :: Index_Constant_Pool
                , index_nameandtype_cp  :: Index_Constant_Pool
                , desc                  :: String
                }
        | MethodRef_Info
                { tag_cp                :: Tag
                , index_name_cp         :: Index_Constant_Pool
                , index_nameandtype_cp  :: Index_Constant_Pool
                , desc                  :: String
                }
        | InterfaceMethodRef_Info
                { tag_cp                :: Tag
                , index_name_cp         :: Index_Constant_Pool
                , index_nameandtype_cp  :: Index_Constant_Pool
                , desc                  :: String
                }
        | String_Info
                { tag_cp                :: Tag
                , index_cp              :: Index_Constant_Pool
                , desc                  :: String
                }
        | Integer_Info
                { tag_cp                :: Tag
                , numi_cp               :: Int
                , desc                  :: String
                }
        | Float_Info
                { tag_cp                :: Tag
                , numf_cp               :: Float
                , desc                  :: String
                }
        | Long_Info
                { tag_cp                :: Tag
                , numi_l1_cp            :: Int
                , numi_l2_cp            :: Int
                , desc                  :: String
                }
        | Double_Info
                { tag_cp                :: Tag
                , numi_d1_cp            :: Int
                , numi_d2_cp            :: Int
                , desc                  :: String
                }
        | NameAndType_Info
                { tag_cp                :: Tag
                , index_name_cp         :: Index_Constant_Pool
                , index_descr_cp        :: Index_Constant_Pool
                , desc                  :: String
                }
        | Utf8_Info
                { tag_cp                :: Tag
                , tam_cp                :: Int
                , cad_cp                :: String
                , desc                  :: String
                }
            deriving (Show, Eq)

data Tag = TagClass
         | TagFieldRef
         | TagMethodRef
         | TagInterfaceMethodRef
         | TagString
         | TagInteger
         | TagFloat
         | TagLong
         | TagDouble
         | TagNameAndType
         | TagUtf8
        deriving (Show, Eq)

data AccessFlags = AccessFlags [Int]
            deriving Show

acc_Public     :: Int
acc_Public     = 1

acc_Private    :: Int
acc_Private    = 2

acc_Protected  :: Int
acc_Protected  = 4

acc_Static     :: Int
acc_Static     = 8

acc_Final      :: Int
acc_Final      = 16

acc_Super_Synchronized      :: Int
acc_Super_Synchronized      = 32

acc_Volatile_Bridge   :: Int
acc_Volatile_Bridge   = 64

acc_Transient_Varargs  :: Int
acc_Transient_Varargs  = 128

acc_Native :: Int
acc_Native = 256

acc_Interface  :: Int
acc_Interface  = 512

acc_Abstract   :: Int
acc_Abstract   = 1024

acc_Strict :: Int
acc_Strict = 2048

acc_Synthetic  :: Int
acc_Synthetic  = 4096

acc_Annotation :: Int
acc_Annotation = 8192

acc_Enum    :: Int
acc_Enum    = 16384

data ThisClass = ThisClass {
                    index_th :: Index_Constant_Pool
                 }
        deriving Show

data SuperClass = SuperClass {
                    index_sp :: Index_Constant_Pool
                  }
        deriving Show

data Interface = Interface {
                    index_if :: Index_Constant_Pool
                  }
        deriving Show

data Field_Info = Field_Info
                        { af_fi          :: AccessFlags
                        , index_name_fi  :: Index_Constant_Pool     -- name_index
                        , index_descr_fi :: Index_Constant_Pool     -- descriptor_index
                        , tam_fi         :: Int                     -- count_attributte
                        , array_attr_fi  :: Attribute_Infos
                        }
            deriving Show

data Method_Info = Method_Info
                        { af_mi          :: AccessFlags
                        , index_name_mi  :: Index_Constant_Pool       -- name_index
                        , index_descr_mi :: Index_Constant_Pool       -- descriptor_index
                        , tam_mi         :: Int                       -- attributes_count
                        , array_attr_mi  :: Attribute_Infos
                        }
                    deriving Show

data Attribute_Info =
        AttributeGeneric
            { index_name_attr           :: Index_Constant_Pool
            , tam_len_attr              :: Int
            , rest_attr                 :: BS.ByteString
            }

      | AttributeConstantValue
            { index_name_attr           :: Index_Constant_Pool              -- attribute_name_index
            , tam_attr                  :: Int                              -- attribute_length
            , index_value_attr          :: Index_Constant_Pool              -- constantvalue_index
            }
      | AttributeCode
            { index_name_attr           :: Index_Constant_Pool              -- attribute_name_index
            , tam_len_attr              :: Int                              -- attribute_length
            , len_stack_attr            :: Int                              -- max_stack
            , len_local_attr            :: Int                              -- max_local
            , tam_code_attr             :: Int                              -- code_length
--            , array_code_attr           :: ListaInt                         -- code como array de bytes
            , array_code_attr           :: [Code]                           -- code array (altern.)
            , tam_ex_attr               :: Int                              -- exceptions_length
            , array_ex_attr             :: Tupla4Int                        -- no usamos
            , tam_atrr_attr             :: Int                              -- attributes_count
            , array_attr_attr           :: Attribute_Infos
            }

      | AttributeExceptions
            { index_name_attr           :: Index_Constant_Pool              -- attribute_name_index
            , tam_len_attr              :: Int                              -- attribute_length
            , tam_num_ex_attr           :: Int                              -- number of exceptions
            , exception_index_table     :: [Int]                            -- exception_index_table
            }

      | AttributeInnerClasses
            { index_name_attr           :: Index_Constant_Pool              -- attribute_name_index
            , tam_len_attr              :: Int                              -- attribute_length
            , tam_classes               :: Int                              -- number_classes
            , array_classes             :: [(Int,Int,Int,AccessFlags)]       -- classes
            }

      | AttributeSynthetic
            { index_name_attr           :: Index_Constant_Pool              -- attribute_name_index
            , tam_len_attr              :: Int                              -- attribute_length
            }

      | AttributeSourceFile
            { index_name_attr           :: Index_Constant_Pool              -- attribute_name_index
            , tam_len_attr              :: Int                              -- attribute_length
            , index_src_attr            :: Index_Constant_Pool              -- sourcefile_index
            }

      | AttributeLineNumberTable
            { index_name_attr           :: Index_Constant_Pool              -- attribute_name_index
            , tam_len_attr              :: Int                              -- attribute_length
            , tam_table_attr            :: Int                              -- lineNumberTable_length
            , array_line_attr           :: Tupla2Int                        -- (start_pc, line_number)
            }
      | AttributeLocalVariableTable
            { index_name_attr           :: Index_Constant_Pool              -- attribute_name_index
            , tam_len_attr              :: Int                              -- attribute_length
            , tam__table_attr           :: Int                              -- local_varible_table_length
            , array_var_attr            :: Tupla5Int                        -- (start_pc, length, name_index, descriptor_index, inlinedex)
            }
      | AttributeDeprecated
            { index_name_attr           :: Index_Constant_Pool              -- attribute_name_index
            , tam_len_attr              :: Int                              -- attribute_length
            }
         deriving Show

type Tupla5Int = [(Int, Int, Int, Int, Int)]
type Tupla2Int = [(Int, Int)]
type Tupla4Int = [(Int, Int, Int, Int)]
type ListaInt  = [Int]
type ConstantPool_Count  = Int
type Interfaces_Count    = Int
type Fields_Count        = Int
type Methods_Count       = Int
type Attributes_Count    = Int
type Index_Constant_Pool = Int

-- |Creates a CP_Info for the given String
utf8Info :: String -> CP_Info
utf8Info str = Utf8_Info {
    tag_cp = TagUtf8,
    tam_cp = length str,
    cad_cp = str,
    desc = ""
}

-- |Creates a CP_Info for a class reference
classRefInfo :: Int -- ^Index of the class name in the constant pool
    -> CP_Info
classRefInfo idx = Class_Info {
    tag_cp = TagClass,
    index_cp = idx,
    desc = ""
}

-- |Creates a CP_Info for a field reference
fieldRefInfo :: Int -- ^ Index of the class reference which the field belongs to
    -> Int -- ^ Index of the name and type for the field
    -> CP_Info
fieldRefInfo classIndex nameAndTypeIndex = FieldRef_Info {
    tag_cp = TagFieldRef,
    index_name_cp = classIndex,
    index_nameandtype_cp = nameAndTypeIndex,
    desc = ""
}

-- |Creates a CP_Info for a method reference
methodRefInfo :: Int -- ^ Index of the class reference which the method belongs to
    -> Int -- ^ Index of the name and type of the referenced method
    -> CP_Info
methodRefInfo classIndex nameAndTypeIndex = MethodRef_Info {
    tag_cp = TagMethodRef,
    index_name_cp = classIndex,
    index_nameandtype_cp = nameAndTypeIndex,
    desc = ""
}

-- |Creates a name and type info
nameAndTypeInfo :: Int -- ^ Index of the name.
    -> Int -- ^ Index of the type. Either is a normal type or a method descriptor/signature
    -> CP_Info
nameAndTypeInfo nameIndex typeIndex = NameAndType_Info {
    tag_cp = TagNameAndType,
    index_name_cp = nameIndex,
    index_descr_cp = typeIndex,
    desc = ""
}

-- | Creates a CP_Info for a constant string that is used in the code
stringInfo :: Int -- ^ Index of the UTF8-Info that contains the string
    -> CP_Info
stringInfo text = String_Info {
    tag_cp = TagString,
    index_cp = text,
    desc = ""
}

-- | Creates a CP_Info for a constant integer that is used in the code
--   Characters are also encoded as integers
integerInfo :: Int -- ^ Value of the integer
    -> CP_Info
integerInfo value = Integer_Info {
    tag_cp = TagInteger,
    numi_cp = value,
    desc = ""
}

thisClassIdx = 1
thisClassRef = classRefInfo thisClassIdx

data Code
  = Nop -- Constants
  | Aconst_null
  | Iconst_m1
  | Iconst_0
  | Iconst_1
  | Iconst_2
  | Iconst_3
  | Iconst_4
  | Iconst_5
  | Lconst_0
  | Lconst_1
  | Fconst_0
  | Fconst_1
  | Dconst_0
  | Dconst_1
  | Bipush Int
  | Sipush Int
  | Ldc Int
  | Ldc_w Int
  | Ldc2_w Int
  -- Loads
  | Iload Int
  | Lload Int
  | Fload Int
  | Dload Int
  | Aload Int
  | Iload_0
  | Iload_1
  | Iload_2
  | Iload_3
  | Lload_0
  | Lload_1
  | Lload_2
  | Lload_3
  | Fload_0
  | Fload_1
  | Fload_2
  | Fload_3
  | Dload_0
  | Dload_1
  | Dload_2
  | Dload_3
  | Aload_0
  | Aload_1
  | Aload_2
  | Aload_3
  | Iaload
  | Laload
  | Faload
  | Daload
  | Aaload
  | Baload
  | Caload
  | Saload
  -- Stores
  | Istore Int
  | Lstore Int
  | Fstore Int
  | Dstore Int
  | Astore Int
  | Istore_0
  | Istore_1
  | Istore_2
  | Istore_3
  | Lstore_0
  | Lstore_1
  | Lstore_2
  | Lstore_3
  | Fstore_0
  | Fstore_1
  | Fstore_2
  | Fstore_3
  | Dstore_0
  | Dstore_1
  | Dstore_2
  | Dstore_3
  | Astore_0
  | Astore_1
  | Astore_2
  | Astore_3
  | Iastore
  | Lastore
  | Fastore
  | Dastore
  | Aastore
  | Bastore
  | Castore
  | Sastore
  -- Stack
  | Pop
  | Pop2
  | Dup
  | Dup_x1
  | Dup_x2
  | Dup2
  | Dup2_x1
  | Dup2_x2
  | Swap
  -- Math
  | Iadd
  | Ladd
  | Fadd
  | Dadd
  | Isub
  | Lsub
  | Fsub
  | Dsub
  | Imul
  | Lmul
  | Fmul
  | Dmul
  | Idiv
  | Ldiv
  | Fdiv
  | Ddiv
  | Irem
  | Lrem
  | Frem
  | Drem
  | Ineg
  | Lneg
  | Fneg
  | Dneg
  | Ishl
  | Lshl
  | Ishr
  | Lshr
  | Iushr
  | Lushr
  | Iand
  | Land
  | Ior
  | Lor
  | Ixor
  | Lxor
  | Iinc Int Int
  -- Conversions
  | I2l
  | I2f
  | I2d
  | L2i
  | L2f
  | L2d
  | F2i
  | F2l
  | F2d
  | D2i
  | D2l
  | D2f
  | I2b
  | I2c
  | I2s
  -- Comparisons
  | Lcmp
  | Fcmpl
  | Fcmpg
  | Dcmpl
  | Dcmpg
  | Ifeq Int
  | Ifne Int
  | Iflt Int
  | Ifge Int
  | Ifgt Int
  | Ifle Int
  | If_icmpeq Int
  | If_icmpne Int
  | If_icmplt Int
  | If_icmpge Int
  | If_icmpgt Int
  | If_icmple Int
  | If_acmpeq Int
  | If_acmpne Int
  -- Control
  | Goto Int
  | Jsr Int
  | Ret Int
  | Tableswitch (Maybe Int)
  | Lookupswitch (Maybe Int)
  | Ireturn
  | Lreturn
  | Freturn
  | Dreturn
  | Areturn
  | CodeReturn
  -- References
  | Getstatic Int
  | Putstatic Int
  | Getfield Int
  | Putfield Int
  | Invokevirtual Int
  | Invokespecial Int
  | Invokestatic Int
  | Invokeinterface Int Int
  | Invokedynamic Int
  | CodeNew Int
  | Newarray Int
  | Anewarray Int
  | Arraylength
  | Athrow
  | Checkcast Int
  | Instanceof Int
  | Monitorenter
  | Monitorexit
  -- Extended
  | Wide Code Int
  | Multinewarray Int Int
  | Ifnull Int
  | Ifnonnull Int
  | Goto_w Integer
  | Jsr_w Int
  -- Reserved
  | Breakpoint
  | Impdep1
  | Impdep2
 deriving (Show)

calcCodeSize :: [Code] -> Int
calcCodeSize x = (foldl (+) 0 (map getCodeSize x))

-- |Returns the size of a code instruction in number of bytes
getCodeSize :: Code -> Int
getCodeSize (Aload _) = 2
getCodeSize (Anewarray _) = 3
getCodeSize (Astore _) = 2
getCodeSize (Bipush _) = 2
getCodeSize (Checkcast _) = 3
getCodeSize (Dload _) = 2
getCodeSize (Dstore _) = 2
getCodeSize (Fload _) = 2
getCodeSize (Fstore _) = 2
getCodeSize (Getfield _) = 3
getCodeSize (Getstatic _) = 3
getCodeSize (Goto _) = 3
getCodeSize (Goto_w _) = 5
getCodeSize (If_acmpeq _) = 3
getCodeSize (If_acmpne _) = 3
getCodeSize (If_icmpeq _) = 3
getCodeSize (If_icmpne _) = 3
getCodeSize (If_icmplt _) = 3
getCodeSize (If_icmpge _) = 3
getCodeSize (If_icmpgt _) = 3
getCodeSize (If_icmple _) = 3
getCodeSize (Ifeq _) = 3
getCodeSize (Ifne _) = 3
getCodeSize (Iflt _) = 3
getCodeSize (Ifge _) = 3
getCodeSize (Ifgt _) = 3
getCodeSize (Ifle _) = 3
getCodeSize (Ifnonnull _) = 3
getCodeSize (Ifnull _) = 3
getCodeSize (Iinc _ _) = 3
getCodeSize (Iload _) = 2
getCodeSize (Instanceof _) = 3
getCodeSize (Invokedynamic _) = 5
getCodeSize (Invokeinterface _ _) = 5
getCodeSize (Invokespecial _) = 3
getCodeSize (Invokestatic _) = 3
getCodeSize (Invokevirtual _) = 3
getCodeSize (Istore _) = 2
getCodeSize (Jsr _) = 3
getCodeSize (Jsr_w _) = 5
getCodeSize (Ldc _) = 2
getCodeSize (Ldc_w _) = 3
getCodeSize (Ldc2_w _) = 3
getCodeSize (Lload _) = 2
getCodeSize (Lookupswitch _) = undefined -- complicated
getCodeSize (Lstore _) = 2
getCodeSize (Multinewarray _ _) = 4
getCodeSize (CodeNew _) = 3
getCodeSize (Newarray _) = 2
getCodeSize (Putfield _) = 3
getCodeSize (Putstatic _) = 3
getCodeSize (Ret _) = 2
getCodeSize (Sipush _) = 3
getCodeSize (Tableswitch _) = undefined -- also complicated
getCodeSize (Wide (Iinc _ _) _) = 6
getCodeSize (Wide c _) = 3 + getCodeSize c
getCodeSize _ = 1