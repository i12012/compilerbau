
import JavaParser
import TypeCheck
import CodeGen.ByteCodeGen
import CodeGen.ClassFileGen
import CodeGen.ClassFileIO
import AbsSyn

main = do
    inputText <- getContents
    let classDecl@(ClassDecl (name, _, _)) = (parse inputText)!!0
        byteCode = writeByteCode (generateClassFile (typecheckClassDecl classDecl)) (name ++ ".class")
    byteCode
