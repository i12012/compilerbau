{
module JavaParser (parse) where
import AbsSyn
import JavaLexer
import Utils
}

%name parseJava
%tokentype { Token }
%error { parseError }
  
%token 
	BOOLEAN { BOOLEAN }
	BREAK { BREAK }
	CASE { CASE }
	CHAR  { CHAR }
	CLASS { CLASS }
	IDENTIFIER { IDENTIFIER $$ }
	INTLITERAL { INTLITERAL $$ }
	CONTINUE { CONTINUE }
	DEFAULT { DEFAULT }
	DO { DO }
	ELSE { ELSE }
	FOR { FOR }
	IF { IF }
	INT{ INT}
	NEW { NEW }
	PRIVATE { PRIVATE }
	PROTECTED { PROTECTED }
	PUBLIC { PUBLIC }
	RETURN { RETURN }
	SWITCH { SWITCH }
	THIS { THIS }
	VOID { VOID }
	WHILE { WHILE }
	BOOLLITERAL { BOOLLITERAL $$ }
	JNULL { JNULL }
	CHARLITERAL { CHARLITERAL $$ }
	EQUAL { EQUAL }
	LESSEQUAL { LESSEQUAL }
	GREATEREQUAL { GREATEREQUAL }
	NOTEQUAL { NOTEQUAL }
	LOGICALOR { LOGICALOR }
	LOGICALAND { LOGICALAND }
	INCREMENT { INCREMENT }
	DECREMENT { DECREMENT }
	SHIFTLEFT { SHIFTLEFT }
	SHIFTRIGHT { SHIFTRIGHT }
	UNSIGNEDSHIFTRIGHT { UNSIGNEDSHIFTRIGHT }
	SIGNEDSHIFTRIGHT { SIGNEDSHIFTRIGHT }
	PLUSEQUAL { PLUSEQUAL }
	MINUSEQUAL { MINUSEQUAL }
	TIMESEQUAL { TIMESEQUAL }
	DIVIDEEQUAL { DIVIDEEQUAL }
	ANDEQUAL { ANDEQUAL }
	OREQUAL { OREQUAL }
	XOREQUAL { XOREQUAL }
	MODULOEQUAL { MODULOEQUAL }
	SHIFTLEFTEQUAL { SHIFTLEFTEQUAL }
	SIGNEDSHIFTRIGHTEQUAL { SIGNEDSHIFTRIGHTEQUAL }
	UNSIGNEDSHIFTRIGHTEQUAL { UNSIGNEDSHIFTRIGHTEQUAL }
	LPARENTHESIS { LPARENTHESIS }
	RPARENTHESIS { RPARENTHESIS }
	LCURLYBRACKET { LCURLYBRACKET }
	RCURLYBRACKET { RCURLYBRACKET }
	SEMICOLON { SEMICOLON }
	DOT { DOT }
	ASSIGN { ASSIGN }
	LESS { LESS }
	GREATER { GREATER }
	EXCLMARK { EXCLMARK }
	TILDE { TILDE }
	QUESMARK { QUESMARK }
	COMMA { COMMA }
	PLUS { PLUS }
	MINUS { MINUS }
	MUL { MUL }
	DIV { DIV }
	MOD { MOD }
	AND { AND }
	OR { OR }
	XOR { XOR }
	SHARP{ SHARP}
	ARROW{ ARROW}
	INSTANCEOF { INSTANCEOF }
	ABSTRACT{ ABSTRACT}
	STATIC{ STATIC}
	COLON { COLON }
%%

compilationunit  : classdeclaration { [$1] }
                 | compilationunit classdeclaration { $1 ++ [$2]}

classdeclaration : CLASS IDENTIFIER classbody { ClassDecl($2, (fst $3), (snd $3)) }
                 | modifiers CLASS IDENTIFIER classbody { ClassDecl($3, (fst $4), (snd $4)) }

classbody        : LCURLYBRACKET RCURLYBRACKET { ([],[]) }
		 | LCURLYBRACKET classbodydeclarations RCURLYBRACKET { $2 }

modifiers        : modifier { [$1] }
		 | modifiers modifier { $1 ++ [$2] }

-- ([FieldDecl], [MethodDecl])
classbodydeclarations : classbodydeclaration { $1 }
		 | classbodydeclarations classbodydeclaration { joinFieldMethodTuples $1 $2 }

modifier         : PUBLIC { $1 }
		 | PROTECTED { $1 }
                 | PRIVATE { $1 }
                 | STATIC { $1 }
                 | ABSTRACT { $1 }

-- ([FieldDecl],[MethodDecl])
classbodydeclaration : classmemberdeclaration { $1 }
		 | constructordeclaration { ([],[$1]) }

-- ([FieldDecl],[MethodDecl])
classmemberdeclaration : fielddeclaration { ([$1],[]) }
		 | methoddeclaration { ([],[$1]) }

-- MethodDecl
constructordeclaration : constructordeclarator constructorbody { MethodDecl("void","<init>",$1,$2) }
		 | modifiers constructordeclarator constructorbody { MethodDecl("void","<init>",$2,$3) }

-- FieldDecl
-- int a;int b; is allowed, int a,b; is not allowed, int a=4; is not allowed either
fielddeclaration : type IDENTIFIER SEMICOLON { FieldDecl($1,$2) }
 		 | modifiers type IDENTIFIER SEMICOLON { FieldDecl($2,$3) }

-- MethodDecl
methoddeclaration : methodheader block { MethodDecl(fst $1, fst (snd $1), snd (snd $1), $2) }

-- [ParamDecl]
-- IDENTIFIER (name of the constructor) is ignored
constructordeclarator : IDENTIFIER LPARENTHESIS RPARENTHESIS { [] }
		 | IDENTIFIER LPARENTHESIS formalparameterlist RPARENTHESIS { $3 }

-- Stmt
constructorbody	 : LCURLYBRACKET RCURLYBRACKET { Block([]) }
		 | LCURLYBRACKET blockstatements RCURLYBRACKET { Block($2) }

-- String
type             : primitivetype { $1 }
		 | referencetype { $1 }

--  Typ      Name    Parameter
-- (String, (String, [ParamDecl]))
methodheader	 : type methoddeclarator { ($1,$2) }
		 | modifiers type methoddeclarator { ($2,$3) }
		 | VOID methoddeclarator { ("void",$2) }
		 | modifiers VOID methoddeclarator { ("void",$3) }

-- [ParamDecl]
formalparameterlist : formalparameter { [$1] }
		 | formalparameterlist COMMA formalparameter { $1 ++ [$3] }

--[Stmt]
blockstatements  : blockstatement { $1 }
		 | blockstatements blockstatement { $1 ++ $2 }

--String
primitivetype    : BOOLEAN { "bool" }
                 | INT { "int" }
                 | CHAR { "char" }

--String
referencetype    : classorinterfacetype { $1 }


-- (String, [ParamDecl] )
methoddeclarator : IDENTIFIER LPARENTHESIS RPARENTHESIS { ($1,[]) }
		 | IDENTIFIER LPARENTHESIS formalparameterlist RPARENTHESIS { ($1,$3) }

-- String
name             : IDENTIFIER { $1 }
                 | name DOT IDENTIFIER { $1 ++ $3 }

-- String
classorinterfacetype : name { $1 }

-- Stmt
block            : LCURLYBRACKET RCURLYBRACKET { Block([]) }
		 | LCURLYBRACKET blockstatements RCURLYBRACKET { Block($2) }

-- Stmt
blockstatement	 : type IDENTIFIER SEMICOLON { [LocalVarDecl($1,$2)] }
  		 | type assignmentinternals SEMICOLON { [LocalVarDecl($1,fst($2)), StmtExprStmt(Assign(fst($2),snd($2)))] }
		 | statement { [$1] }

-- ParamDecl
-- (Type, String)
formalparameter  : type IDENTIFIER { ($1,$2) }

-- Stmt
statement        : statementwithouttrailingsubstatement{ $1 }
		 | ifthenstatement { $1 }
		 | ifthenelsestatement { $1 }
		 | whilestatement { $1 }

-- Expr
expression       : assignmentexpression { $1 }

-- Stmt
statementwithouttrailingsubstatement : block { $1 }
		 | SEMICOLON { EmptyStmt }
		 | statementexpression SEMICOLON { StmtExprStmt($1) }
		 | returnstatement { Return($1) }

ifthenstatement  : IF LPARENTHESIS expression RPARENTHESIS statement { If($3, $5, Nothing) }

ifthenelsestatement : IF LPARENTHESIS expression RPARENTHESIS statementnoshortif ELSE statement { If($3, $5, Just $7) }

whilestatement   : WHILE LPARENTHESIS expression RPARENTHESIS statement { While($3, $5) }

-- Expr
assignmentexpression : assignment { StmtExprExpr($1) }
		 | conditionalexpression { $1 }

-- Maybe Expr
returnstatement  : RETURN SEMICOLON { Nothing }
		 | RETURN expression SEMICOLON { Just $2 }

-- Stmt
statementnoshortif : statementwithouttrailingsubstatement { $1 }
		 | ifthenelsestatementnoshortif { $1 }
		 | whilestatementnoshortif { $1 }

-- Expr
conditionalexpression : conditionalorexpression { $1 }
--		 | conditionalorexpression QUESMARK expression COLON conditionalexpression { This } --If($1, $3, Just $5) }

-- StmtExpr
assignment       : assignmentinternals { Assign(fst($1),snd($1)) }

-- (String, Expr)
assignmentinternals : lefthandside assignmentoperator assignmentexpression { ($1, $3) }

-- StmtExpr
statementexpression : assignment { $1 }
--		 | preincrementexpression {}
--		 | predecrementexpression {}
--		 | postincrementexpression {}
--		 | postdecrementexpression {}
		 | methodinvocation { $1 }
		 | classinstancecreationexpression { $1 }

ifthenelsestatementnoshortif : IF LPARENTHESIS expression RPARENTHESIS statementnoshortif ELSE statementnoshortif { If($3, $5, Just $7) }

whilestatementnoshortif : WHILE LPARENTHESIS expression RPARENTHESIS statementnoshortif { While($3, $5) }

-- Expr
conditionalorexpression : conditionalandexpression { $1 }
--		 | conditionalorexpression LOGICALOR conditionalandexpression {}

-- String
lefthandside     : name { $1 }

assignmentoperator : ASSIGN {}
		 | TIMESEQUAL {}
		 | DIVIDEEQUAL {}
		 | MODULOEQUAL {}
		 | PLUSEQUAL {}
		 | MINUSEQUAL {}
		 | SHIFTLEFTEQUAL {}
		 | SIGNEDSHIFTRIGHTEQUAL {}
		 | UNSIGNEDSHIFTRIGHTEQUAL {}
		 | ANDEQUAL {}
		 | XOREQUAL {}
		 | OREQUAL{}

-- preincrementexpression : INCREMENT unaryexpression {}
-- predecrementexpression : DECREMENT unaryexpression {}
-- postincrementexpression : postfixexpression INCREMENT {}
-- postdecrementexpression : postfixexpression DECREMENT {}

-- StmtExpr
methodinvocation : name LPARENTHESIS RPARENTHESIS { MethodCall(This, $1, []) }
		 | name LPARENTHESIS argumentlist RPARENTHESIS { MethodCall(This, $1, $3) }
		 | primary DOT IDENTIFIER LPARENTHESIS RPARENTHESIS { MethodCall($1, $3, []) }
		 | primary DOT IDENTIFIER LPARENTHESIS argumentlist RPARENTHESIS { MethodCall($1, $3, $5) }
     
-- StmtExpr
classinstancecreationexpression : NEW classorinterfacetype LPARENTHESIS RPARENTHESIS { New($2, []) }
                 | NEW classorinterfacetype LPARENTHESIS argumentlist RPARENTHESIS { New($2, $4) }

-- Expr
conditionalandexpression : inclusiveorexpression { $1 }

-- Expr
primary		 : primarynonewarray { $1 }

-- Expr
inclusiveorexpression : exclusiveorexpression { $1 }
		 | inclusiveorexpression OR exclusiveorexpression { Binary("||", $1, $3) }

-- Expr
primarynonewarray : literal { $1 }
		 | THIS { This }
 		 | LPARENTHESIS expression RPARENTHESIS { $2 }
                 | classinstancecreationexpression { StmtExprExpr($1) }
		 | fieldaccess { $1 }
		 | methodinvocation { StmtExprExpr($1) }

fieldaccess      : primary DOT IDENTIFIER { InstVar($1,$3) } --TODO: Stimmt das? (Expr und String)

-- Expr
exclusiveorexpression : andexpression { $1 }
-- 		 | exclusiveorexpression XOR andexpression { Binary("^^",$1,$3) } -- Machen wir nicht

-- Expr
literal		 : INTLITERAL { Integer($1) }
		 | BOOLLITERAL { Bool($1) }
		 | CHARLITERAL { Char($1) }
-- 		 | STRINGLITERAL { String($1) }
		 | JNULL { Jnull }

-- Expr
andexpression    : equalityexpression { $1 }
		 | andexpression AND equalityexpression { Binary("&&",$1,$3) }

-- Expr
equalityexpression : relationalexpression { $1 }
		 | equalityexpression EQUAL relationalexpression { Binary("==",$1,$3) }
		 | equalityexpression NOTEQUAL relationalexpression { Binary("!=",$1,$3) }

-- Expr
relationalexpression : shiftexpression { $1 }
		 | relationalexpression LESS shiftexpression { Binary("<",$1,$3) }
		 | relationalexpression GREATER shiftexpression { Binary(">",$1,$3) }
-- 		 | relationalexpression LESSEQUAL shiftexpression { Binary("<=",$1,$3) }
-- 		 | relationalexpression GREATEREQUAL shiftexpression { Binary(">=",$1,$3) }
--		 | relationalexpression INSTANCEOF referencetype { Binary("instanceof",$1,$3) }

-- Expr
shiftexpression	 : additiveexpression { $1 }

-- Expr
additiveexpression : multiplicativeexpression { $1 }
		 | additiveexpression PLUS multiplicativeexpression { Binary("+",$1,$3) }
		 | additiveexpression MINUS multiplicativeexpression { Binary("-",$1,$3) }

-- Expr
multiplicativeexpression : unaryexpression { $1 }
		 | multiplicativeexpression MUL unaryexpression { Binary("*",$1,$3) }
		 | multiplicativeexpression DIV unaryexpression { Binary("/",$1,$3) }
		 | multiplicativeexpression MOD unaryexpression { Binary("%",$1,$3) }

-- [Expr]
argumentlist     : expression { [$1] }
		 | argumentlist COMMA expression { $1 ++ [$3] }

-- Expr
postfixexpression : primary { $1 }
		 | name { LocalOrFieldVar($1) }
-- 		 | postincrementexpression {}
-- 		 | postdecrementexpression{}

-- Expr
unaryexpressionnotplusminus : postfixexpression { $1 }
	         | TILDE unaryexpression { Unary("~",$2) }
		 | EXCLMARK unaryexpression { Unary("!",$2) }
		 | castexpression{ $1 }

-- Expr
castexpression	 : LPARENTHESIS primitivetype RPARENTHESIS unaryexpression { TypedExpr($4,$2) }
--  		 | LPARENTHESIS expression RPARENTHESIS unaryexpressionnotplusminus { This } --TODO

-- Expr
unaryexpression	 : unaryexpressionnotplusminus { $1 } --TODO
		 | PLUS unaryexpression { Unary("++",$2) }
		 | MINUS unaryexpression { Unary("--",$2) }
-- 		 | preincrementexpression {}
-- 		 | predecrementexpression {}


{
parse :: String -> [ClassDecl]
parse s = parseJava (alexScanTokens s)
parseError :: [Token] -> a
parseError _ = error "Parse error"
}
