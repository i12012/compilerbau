In Kommentare gesetzte Funktionalitäten, die reinkönnen, wenn sie vom Typcheck bzw. der Bytecodegenerierung akzeptiert werden: (Wenn doch verwendet werden, dann nicht nur in der Zeile auskommentieren, in denen die Regeln stehen sondern auch in solchen, in denen das Nonterminal verwendet wird!)
	conditionalexpression : conditionalorexpression QUESMARK expression COLON conditionalexpression { StmtExprExpr(If($1, $3, Just $5)) }

	assignmentoperator: TIMESEQUAL {}
		| DIVIDEEQUAL {}
		| MODULOEQUAL {}
		| PLUSEQUAL {}
		| MINUSEQUAL {}
		| SHIFTLEFTEQUAL {}
		| SIGNEDSHIFTRIGHTEQUAL {}
		| UNSIGNEDSHIFTRIGHTEQUAL {}
		| ANDEQUAL {}
		| XOREQUAL {}
		| OREQUAL{}
	preincrementexpression : INCREMENT unaryexpression {}
	predecrementexpression : DECREMENT unaryexpression {}
	predecrementexpression : DECREMENT unaryexpression {}
	predecrementexpression : DECREMENT unaryexpression {}
