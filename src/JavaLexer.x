{
module JavaLexer  (alexScanTokens, Token(..)) where
}

%wrapper "basic"

$digit = 0-9
$alpha = [a-zA-Z]

tokens :-

      $white+ ; 
      "//".*  ;
      "/*".*(\n.*)*"*/";

      \'($alpha|$digit)\'	{ \s -> CHARLITERAL ((\(_:snd:_) -> snd) s) }
      \+\+ 		{ \s -> INCREMENT }
	\+		{ \s -> PLUS }
	\-\-		{ \s -> DECREMENT }
	\-\>		{ \s -> ARROW }
	\-		{ \s -> MINUS }
	\*		{ \s -> MUL }
	\/		{ \s -> DIV }
	\%		{ \s -> MOD }
	\&\&		{ \s -> AND }
	\|\|		{ \s -> OR }
	\(		{ \s -> LPARENTHESIS }
	\)		{ \s -> RPARENTHESIS }
	\{		{ \s -> LCURLYBRACKET }
	\}		{ \s -> RCURLYBRACKET }
	\=\=		{ \s -> EQUAL }
	\!\=		{ \s -> NOTEQUAL }
	\<\=		{ \s -> LESSEQUAL }
	\>\=		{ \s -> GREATEREQUAL }
	\<		{ \s -> LESS }
	\>		{ \s -> GREATER }
	\;		{ \s -> SEMICOLON }
	\,		{ \s -> COMMA }
	\.		{ \s -> DOT }
	\=		{ \s -> ASSIGN }
	\#		{ \s -> SHARP }

      boolean          { \s -> BOOLEAN }
      break		{ \s  ->  BREAK }
      case		{ \s  ->  CASE }
      char 		{ \s  ->  CHAR  }
      class		{ \s  ->  CLASS}
      continue		{ \s  ->  CONTINUE }
      default		{ \s  ->  DEFAULT }
      do		{ \s  ->  DO }
      else		{ \s  ->  ELSE }
      for		{ \s  ->  FOR }
      or		{ \s  ->  OR }
      if		{ \s  ->  IF }
      int		{ \s  ->  INT }
      new		{ \s  ->  NEW }
      private		{ \s  ->  PRIVATE }
      protected		{ \s  ->  PROTECTED }
      public		{ \s  ->  PUBLIC }
      return		{ \s  ->  RETURN }
      switch		{ \s  ->  SWITCH }
      this		{ \s  ->  THIS }
      void		{ \s  ->  VOID }
      while		{ \s  ->  WHILE }
      true		{ \s  -> BOOLLITERAL(True) }
      false		{ \s  -> BOOLLITERAL(False) }
      null		{ \s  ->  JNULL }
      $digit+       { \s -> INTLITERAL (read s) }
      $alpha [$alpha $digit \_ \']*   { \s -> IDENTIFIER s }
      
{
data Token
     = BOOLEAN 
     | BREAK 
     | CASE 
     | CHAR  
     | CLASS
     | CONTINUE 
     | DEFAULT 
     | DO 
     | ELSE 
     | FOR 
     | IF 
     | INT
     | NEW 
     | PRIVATE 
     | PROTECTED 
     | PUBLIC 
     | RETURN 
     | SWITCH 
     | THIS 
     | VOID 
     | WHILE 
     | INTLITERAL Integer
     | BOOLLITERAL Bool
     | JNULL 
     | CHARLITERAL Char
     | IDENTIFIER String
     | EQUAL 
     | LESSEQUAL 
     | GREATEREQUAL 
     | NOTEQUAL 
     | LOGICALOR 
     | LOGICALAND 
     | INCREMENT 
     | DECREMENT 
     | SHIFTLEFT 
     | SHIFTRIGHT 
     | UNSIGNEDSHIFTRIGHT 
     | SIGNEDSHIFTRIGHT 
     | PLUSEQUAL 
     | MINUSEQUAL 
     | TIMESEQUAL 
     | DIVIDEEQUAL 
     | ANDEQUAL 
     | OREQUAL 
     | XOREQUAL 
     | MODULOEQUAL 
     | SHIFTLEFTEQUAL 
     | SIGNEDSHIFTRIGHTEQUAL 
     | UNSIGNEDSHIFTRIGHTEQUAL 
     | LPARENTHESIS
     | RPARENTHESIS
     | LCURLYBRACKET 
     | RCURLYBRACKET 
     | SEMICOLON 
     | DOT 
     | ASSIGN 
     | LESS 
     | GREATER 
     | EXCLMARK 
     | TILDE 
     | QUESMARK 
     | COMMA
     | PLUS 
     | MINUS 
     | MUL 
     | DIV 
     | MOD 
     | AND 
     | OR 
     | XOR 
     | SHARP
     | ARROW
     | INSTANCEOF
     | STATIC
     | ABSTRACT
     | COLON
     deriving(Eq,Show)


main = do
  --s <- readFile "xxx"
  s <- getContents
  print (alexScanTokens s)
}
