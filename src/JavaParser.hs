{-# OPTIONS_GHC -w #-}
module JavaParser (parse) where
import AbsSyn
import JavaLexer
import Utils
import Control.Applicative(Applicative(..))

-- parser produced by Happy Version 1.19.4

data HappyAbsSyn t4 t5 t6 t7 t8 t9 t10 t11 t12 t13 t14 t15 t16 t17 t18 t19 t20 t21 t22 t23 t24 t25 t26 t27 t28 t29 t30 t31 t32 t33 t34 t35 t36 t37 t38 t39 t40 t41 t42 t43 t44 t45 t46 t47 t48 t49 t50 t51 t52 t53 t54 t55 t56 t57 t58 t59 t60 t61 t62 t63 t64 t65 t66
	= HappyTerminal (Token)
	| HappyErrorToken Int
	| HappyAbsSyn4 t4
	| HappyAbsSyn5 t5
	| HappyAbsSyn6 t6
	| HappyAbsSyn7 t7
	| HappyAbsSyn8 t8
	| HappyAbsSyn9 t9
	| HappyAbsSyn10 t10
	| HappyAbsSyn11 t11
	| HappyAbsSyn12 t12
	| HappyAbsSyn13 t13
	| HappyAbsSyn14 t14
	| HappyAbsSyn15 t15
	| HappyAbsSyn16 t16
	| HappyAbsSyn17 t17
	| HappyAbsSyn18 t18
	| HappyAbsSyn19 t19
	| HappyAbsSyn20 t20
	| HappyAbsSyn21 t21
	| HappyAbsSyn22 t22
	| HappyAbsSyn23 t23
	| HappyAbsSyn24 t24
	| HappyAbsSyn25 t25
	| HappyAbsSyn26 t26
	| HappyAbsSyn27 t27
	| HappyAbsSyn28 t28
	| HappyAbsSyn29 t29
	| HappyAbsSyn30 t30
	| HappyAbsSyn31 t31
	| HappyAbsSyn32 t32
	| HappyAbsSyn33 t33
	| HappyAbsSyn34 t34
	| HappyAbsSyn35 t35
	| HappyAbsSyn36 t36
	| HappyAbsSyn37 t37
	| HappyAbsSyn38 t38
	| HappyAbsSyn39 t39
	| HappyAbsSyn40 t40
	| HappyAbsSyn41 t41
	| HappyAbsSyn42 t42
	| HappyAbsSyn43 t43
	| HappyAbsSyn44 t44
	| HappyAbsSyn45 t45
	| HappyAbsSyn46 t46
	| HappyAbsSyn47 t47
	| HappyAbsSyn48 t48
	| HappyAbsSyn49 t49
	| HappyAbsSyn50 t50
	| HappyAbsSyn51 t51
	| HappyAbsSyn52 t52
	| HappyAbsSyn53 t53
	| HappyAbsSyn54 t54
	| HappyAbsSyn55 t55
	| HappyAbsSyn56 t56
	| HappyAbsSyn57 t57
	| HappyAbsSyn58 t58
	| HappyAbsSyn59 t59
	| HappyAbsSyn60 t60
	| HappyAbsSyn61 t61
	| HappyAbsSyn62 t62
	| HappyAbsSyn63 t63
	| HappyAbsSyn64 t64
	| HappyAbsSyn65 t65
	| HappyAbsSyn66 t66

action_0 (71) = happyShift action_5
action_0 (82) = happyShift action_6
action_0 (83) = happyShift action_7
action_0 (84) = happyShift action_8
action_0 (140) = happyShift action_9
action_0 (141) = happyShift action_10
action_0 (4) = happyGoto action_11
action_0 (5) = happyGoto action_2
action_0 (7) = happyGoto action_3
action_0 (9) = happyGoto action_4
action_0 _ = happyFail

action_1 (71) = happyShift action_5
action_1 (82) = happyShift action_6
action_1 (83) = happyShift action_7
action_1 (84) = happyShift action_8
action_1 (140) = happyShift action_9
action_1 (141) = happyShift action_10
action_1 (5) = happyGoto action_2
action_1 (7) = happyGoto action_3
action_1 (9) = happyGoto action_4
action_1 _ = happyFail

action_2 _ = happyReduce_1

action_3 (71) = happyShift action_15
action_3 (82) = happyShift action_6
action_3 (83) = happyShift action_7
action_3 (84) = happyShift action_8
action_3 (140) = happyShift action_9
action_3 (141) = happyShift action_10
action_3 (9) = happyGoto action_14
action_3 _ = happyFail

action_4 _ = happyReduce_7

action_5 (72) = happyShift action_13
action_5 _ = happyFail

action_6 _ = happyReduce_13

action_7 _ = happyReduce_12

action_8 _ = happyReduce_11

action_9 _ = happyReduce_15

action_10 _ = happyReduce_14

action_11 (71) = happyShift action_5
action_11 (82) = happyShift action_6
action_11 (83) = happyShift action_7
action_11 (84) = happyShift action_8
action_11 (140) = happyShift action_9
action_11 (141) = happyShift action_10
action_11 (143) = happyAccept
action_11 (5) = happyGoto action_12
action_11 (7) = happyGoto action_3
action_11 (9) = happyGoto action_4
action_11 _ = happyFail

action_12 _ = happyReduce_2

action_13 (118) = happyShift action_18
action_13 (6) = happyGoto action_17
action_13 _ = happyFail

action_14 _ = happyReduce_8

action_15 (72) = happyShift action_16
action_15 _ = happyFail

action_16 (118) = happyShift action_18
action_16 (6) = happyGoto action_39
action_16 _ = happyFail

action_17 _ = happyReduce_3

action_18 (67) = happyShift action_33
action_18 (70) = happyShift action_34
action_18 (72) = happyShift action_35
action_18 (80) = happyShift action_36
action_18 (82) = happyShift action_6
action_18 (83) = happyShift action_7
action_18 (84) = happyShift action_8
action_18 (88) = happyShift action_37
action_18 (119) = happyShift action_38
action_18 (140) = happyShift action_9
action_18 (141) = happyShift action_10
action_18 (7) = happyGoto action_19
action_18 (8) = happyGoto action_20
action_18 (9) = happyGoto action_4
action_18 (10) = happyGoto action_21
action_18 (11) = happyGoto action_22
action_18 (12) = happyGoto action_23
action_18 (13) = happyGoto action_24
action_18 (14) = happyGoto action_25
action_18 (15) = happyGoto action_26
action_18 (17) = happyGoto action_27
action_18 (18) = happyGoto action_28
action_18 (21) = happyGoto action_29
action_18 (22) = happyGoto action_30
action_18 (24) = happyGoto action_31
action_18 (25) = happyGoto action_32
action_18 _ = happyFail

action_19 (67) = happyShift action_33
action_19 (70) = happyShift action_34
action_19 (72) = happyShift action_35
action_19 (80) = happyShift action_36
action_19 (82) = happyShift action_6
action_19 (83) = happyShift action_7
action_19 (84) = happyShift action_8
action_19 (88) = happyShift action_54
action_19 (140) = happyShift action_9
action_19 (141) = happyShift action_10
action_19 (9) = happyGoto action_14
action_19 (15) = happyGoto action_52
action_19 (17) = happyGoto action_53
action_19 (21) = happyGoto action_29
action_19 (22) = happyGoto action_30
action_19 (24) = happyGoto action_31
action_19 (25) = happyGoto action_32
action_19 _ = happyFail

action_20 (67) = happyShift action_33
action_20 (70) = happyShift action_34
action_20 (72) = happyShift action_35
action_20 (80) = happyShift action_36
action_20 (82) = happyShift action_6
action_20 (83) = happyShift action_7
action_20 (84) = happyShift action_8
action_20 (88) = happyShift action_37
action_20 (119) = happyShift action_51
action_20 (140) = happyShift action_9
action_20 (141) = happyShift action_10
action_20 (7) = happyGoto action_19
action_20 (9) = happyGoto action_4
action_20 (10) = happyGoto action_50
action_20 (11) = happyGoto action_22
action_20 (12) = happyGoto action_23
action_20 (13) = happyGoto action_24
action_20 (14) = happyGoto action_25
action_20 (15) = happyGoto action_26
action_20 (17) = happyGoto action_27
action_20 (18) = happyGoto action_28
action_20 (21) = happyGoto action_29
action_20 (22) = happyGoto action_30
action_20 (24) = happyGoto action_31
action_20 (25) = happyGoto action_32
action_20 _ = happyFail

action_21 _ = happyReduce_9

action_22 _ = happyReduce_16

action_23 _ = happyReduce_17

action_24 _ = happyReduce_18

action_25 _ = happyReduce_19

action_26 (118) = happyShift action_49
action_26 (16) = happyGoto action_48
action_26 _ = happyFail

action_27 (72) = happyShift action_47
action_27 (23) = happyGoto action_46
action_27 _ = happyFail

action_28 (118) = happyShift action_45
action_28 (26) = happyGoto action_44
action_28 _ = happyFail

action_29 _ = happyReduce_29

action_30 _ = happyReduce_30

action_31 (121) = happyShift action_43
action_31 _ = happyReduce_47

action_32 _ = happyReduce_42

action_33 _ = happyReduce_39

action_34 _ = happyReduce_41

action_35 (116) = happyShift action_42
action_35 _ = happyReduce_45

action_36 _ = happyReduce_40

action_37 (72) = happyShift action_41
action_37 (23) = happyGoto action_40
action_37 _ = happyFail

action_38 _ = happyReduce_5

action_39 _ = happyReduce_4

action_40 _ = happyReduce_33

action_41 (116) = happyShift action_93
action_41 _ = happyFail

action_42 (67) = happyShift action_33
action_42 (70) = happyShift action_34
action_42 (72) = happyShift action_80
action_42 (80) = happyShift action_36
action_42 (117) = happyShift action_101
action_42 (17) = happyGoto action_98
action_42 (19) = happyGoto action_99
action_42 (21) = happyGoto action_29
action_42 (22) = happyGoto action_30
action_42 (24) = happyGoto action_31
action_42 (25) = happyGoto action_32
action_42 (28) = happyGoto action_100
action_42 _ = happyFail

action_43 (72) = happyShift action_97
action_43 _ = happyFail

action_44 _ = happyReduce_24

action_45 (67) = happyShift action_33
action_45 (70) = happyShift action_34
action_45 (72) = happyShift action_80
action_45 (73) = happyShift action_81
action_45 (79) = happyShift action_82
action_45 (80) = happyShift action_36
action_45 (81) = happyShift action_83
action_45 (85) = happyShift action_84
action_45 (87) = happyShift action_85
action_45 (89) = happyShift action_86
action_45 (90) = happyShift action_87
action_45 (91) = happyShift action_88
action_45 (92) = happyShift action_89
action_45 (116) = happyShift action_90
action_45 (118) = happyShift action_45
action_45 (119) = happyShift action_96
action_45 (120) = happyShift action_92
action_45 (17) = happyGoto action_59
action_45 (20) = happyGoto action_95
action_45 (21) = happyGoto action_29
action_45 (22) = happyGoto action_30
action_45 (24) = happyGoto action_61
action_45 (25) = happyGoto action_32
action_45 (26) = happyGoto action_62
action_45 (27) = happyGoto action_63
action_45 (29) = happyGoto action_64
action_45 (31) = happyGoto action_65
action_45 (32) = happyGoto action_66
action_45 (33) = happyGoto action_67
action_45 (34) = happyGoto action_68
action_45 (36) = happyGoto action_69
action_45 (39) = happyGoto action_70
action_45 (40) = happyGoto action_71
action_45 (41) = happyGoto action_72
action_45 (45) = happyGoto action_73
action_45 (47) = happyGoto action_74
action_45 (48) = happyGoto action_75
action_45 (50) = happyGoto action_76
action_45 (52) = happyGoto action_77
action_45 (53) = happyGoto action_78
action_45 (55) = happyGoto action_79
action_45 _ = happyFail

action_46 _ = happyReduce_31

action_47 (116) = happyShift action_93
action_47 (120) = happyShift action_94
action_47 _ = happyFail

action_48 _ = happyReduce_20

action_49 (67) = happyShift action_33
action_49 (70) = happyShift action_34
action_49 (72) = happyShift action_80
action_49 (73) = happyShift action_81
action_49 (79) = happyShift action_82
action_49 (80) = happyShift action_36
action_49 (81) = happyShift action_83
action_49 (85) = happyShift action_84
action_49 (87) = happyShift action_85
action_49 (89) = happyShift action_86
action_49 (90) = happyShift action_87
action_49 (91) = happyShift action_88
action_49 (92) = happyShift action_89
action_49 (116) = happyShift action_90
action_49 (118) = happyShift action_45
action_49 (119) = happyShift action_91
action_49 (120) = happyShift action_92
action_49 (17) = happyGoto action_59
action_49 (20) = happyGoto action_60
action_49 (21) = happyGoto action_29
action_49 (22) = happyGoto action_30
action_49 (24) = happyGoto action_61
action_49 (25) = happyGoto action_32
action_49 (26) = happyGoto action_62
action_49 (27) = happyGoto action_63
action_49 (29) = happyGoto action_64
action_49 (31) = happyGoto action_65
action_49 (32) = happyGoto action_66
action_49 (33) = happyGoto action_67
action_49 (34) = happyGoto action_68
action_49 (36) = happyGoto action_69
action_49 (39) = happyGoto action_70
action_49 (40) = happyGoto action_71
action_49 (41) = happyGoto action_72
action_49 (45) = happyGoto action_73
action_49 (47) = happyGoto action_74
action_49 (48) = happyGoto action_75
action_49 (50) = happyGoto action_76
action_49 (52) = happyGoto action_77
action_49 (53) = happyGoto action_78
action_49 (55) = happyGoto action_79
action_49 _ = happyFail

action_50 _ = happyReduce_10

action_51 _ = happyReduce_6

action_52 (118) = happyShift action_49
action_52 (16) = happyGoto action_58
action_52 _ = happyFail

action_53 (72) = happyShift action_57
action_53 (23) = happyGoto action_56
action_53 _ = happyFail

action_54 (72) = happyShift action_41
action_54 (23) = happyGoto action_55
action_54 _ = happyFail

action_55 _ = happyReduce_34

action_56 _ = happyReduce_32

action_57 (116) = happyShift action_93
action_57 (120) = happyShift action_161
action_57 _ = happyFail

action_58 _ = happyReduce_21

action_59 (72) = happyShift action_160
action_59 (24) = happyGoto action_158
action_59 (40) = happyGoto action_159
action_59 (45) = happyGoto action_73
action_59 _ = happyFail

action_60 (67) = happyShift action_33
action_60 (70) = happyShift action_34
action_60 (72) = happyShift action_80
action_60 (73) = happyShift action_81
action_60 (79) = happyShift action_82
action_60 (80) = happyShift action_36
action_60 (81) = happyShift action_83
action_60 (85) = happyShift action_84
action_60 (87) = happyShift action_85
action_60 (89) = happyShift action_86
action_60 (90) = happyShift action_87
action_60 (91) = happyShift action_88
action_60 (92) = happyShift action_89
action_60 (116) = happyShift action_90
action_60 (118) = happyShift action_45
action_60 (119) = happyShift action_157
action_60 (120) = happyShift action_92
action_60 (17) = happyGoto action_59
action_60 (21) = happyGoto action_29
action_60 (22) = happyGoto action_30
action_60 (24) = happyGoto action_61
action_60 (25) = happyGoto action_32
action_60 (26) = happyGoto action_62
action_60 (27) = happyGoto action_105
action_60 (29) = happyGoto action_64
action_60 (31) = happyGoto action_65
action_60 (32) = happyGoto action_66
action_60 (33) = happyGoto action_67
action_60 (34) = happyGoto action_68
action_60 (36) = happyGoto action_69
action_60 (39) = happyGoto action_70
action_60 (40) = happyGoto action_71
action_60 (41) = happyGoto action_72
action_60 (45) = happyGoto action_73
action_60 (47) = happyGoto action_74
action_60 (48) = happyGoto action_75
action_60 (50) = happyGoto action_76
action_60 (52) = happyGoto action_77
action_60 (53) = happyGoto action_78
action_60 (55) = happyGoto action_79
action_60 _ = happyFail

action_61 (105) = happyReduce_82
action_61 (106) = happyReduce_82
action_61 (107) = happyReduce_82
action_61 (108) = happyReduce_82
action_61 (109) = happyReduce_82
action_61 (110) = happyReduce_82
action_61 (111) = happyReduce_82
action_61 (112) = happyReduce_82
action_61 (113) = happyReduce_82
action_61 (114) = happyReduce_82
action_61 (115) = happyReduce_82
action_61 (116) = happyShift action_156
action_61 (121) = happyShift action_43
action_61 (122) = happyReduce_82
action_61 _ = happyReduce_47

action_62 _ = happyReduce_59

action_63 _ = happyReduce_37

action_64 _ = happyReduce_52

action_65 _ = happyReduce_54

action_66 _ = happyReduce_55

action_67 _ = happyReduce_56

action_68 _ = happyReduce_57

action_69 _ = happyReduce_62

action_70 _ = happyReduce_76

action_71 _ = happyReduce_74

action_72 (120) = happyShift action_155
action_72 _ = happyFail

action_73 (105) = happyShift action_143
action_73 (106) = happyShift action_144
action_73 (107) = happyShift action_145
action_73 (108) = happyShift action_146
action_73 (109) = happyShift action_147
action_73 (110) = happyShift action_148
action_73 (111) = happyShift action_149
action_73 (112) = happyShift action_150
action_73 (113) = happyShift action_151
action_73 (114) = happyShift action_152
action_73 (115) = happyShift action_153
action_73 (122) = happyShift action_154
action_73 (46) = happyGoto action_142
action_73 _ = happyFail

action_74 (121) = happyReduce_110
action_74 _ = happyReduce_77

action_75 (121) = happyReduce_108
action_75 _ = happyReduce_78

action_76 (121) = happyShift action_141
action_76 _ = happyFail

action_77 _ = happyReduce_102

action_78 _ = happyReduce_109

action_79 _ = happyReduce_105

action_80 _ = happyReduce_45

action_81 _ = happyReduce_113

action_82 (116) = happyShift action_140
action_82 _ = happyFail

action_83 (72) = happyShift action_80
action_83 (24) = happyGoto action_31
action_83 (25) = happyGoto action_139
action_83 _ = happyFail

action_84 (72) = happyShift action_80
action_84 (73) = happyShift action_81
action_84 (81) = happyShift action_83
action_84 (87) = happyShift action_85
action_84 (90) = happyShift action_87
action_84 (91) = happyShift action_88
action_84 (92) = happyShift action_89
action_84 (116) = happyShift action_131
action_84 (120) = happyShift action_138
action_84 (125) = happyShift action_132
action_84 (126) = happyShift action_133
action_84 (129) = happyShift action_134
action_84 (130) = happyShift action_135
action_84 (24) = happyGoto action_109
action_84 (30) = happyGoto action_137
action_84 (35) = happyGoto action_111
action_84 (38) = happyGoto action_112
action_84 (39) = happyGoto action_113
action_84 (40) = happyGoto action_71
action_84 (44) = happyGoto action_114
action_84 (45) = happyGoto action_73
action_84 (47) = happyGoto action_115
action_84 (48) = happyGoto action_116
action_84 (49) = happyGoto action_117
action_84 (50) = happyGoto action_118
action_84 (51) = happyGoto action_119
action_84 (52) = happyGoto action_77
action_84 (53) = happyGoto action_78
action_84 (54) = happyGoto action_120
action_84 (55) = happyGoto action_79
action_84 (56) = happyGoto action_121
action_84 (57) = happyGoto action_122
action_84 (58) = happyGoto action_123
action_84 (59) = happyGoto action_124
action_84 (60) = happyGoto action_125
action_84 (61) = happyGoto action_126
action_84 (63) = happyGoto action_127
action_84 (64) = happyGoto action_128
action_84 (65) = happyGoto action_129
action_84 (66) = happyGoto action_130
action_84 _ = happyFail

action_85 _ = happyReduce_106

action_86 (116) = happyShift action_136
action_86 _ = happyFail

action_87 _ = happyReduce_114

action_88 _ = happyReduce_116

action_89 _ = happyReduce_115

action_90 (72) = happyShift action_80
action_90 (73) = happyShift action_81
action_90 (81) = happyShift action_83
action_90 (87) = happyShift action_85
action_90 (90) = happyShift action_87
action_90 (91) = happyShift action_88
action_90 (92) = happyShift action_89
action_90 (116) = happyShift action_131
action_90 (125) = happyShift action_132
action_90 (126) = happyShift action_133
action_90 (129) = happyShift action_134
action_90 (130) = happyShift action_135
action_90 (24) = happyGoto action_109
action_90 (30) = happyGoto action_110
action_90 (35) = happyGoto action_111
action_90 (38) = happyGoto action_112
action_90 (39) = happyGoto action_113
action_90 (40) = happyGoto action_71
action_90 (44) = happyGoto action_114
action_90 (45) = happyGoto action_73
action_90 (47) = happyGoto action_115
action_90 (48) = happyGoto action_116
action_90 (49) = happyGoto action_117
action_90 (50) = happyGoto action_118
action_90 (51) = happyGoto action_119
action_90 (52) = happyGoto action_77
action_90 (53) = happyGoto action_78
action_90 (54) = happyGoto action_120
action_90 (55) = happyGoto action_79
action_90 (56) = happyGoto action_121
action_90 (57) = happyGoto action_122
action_90 (58) = happyGoto action_123
action_90 (59) = happyGoto action_124
action_90 (60) = happyGoto action_125
action_90 (61) = happyGoto action_126
action_90 (63) = happyGoto action_127
action_90 (64) = happyGoto action_128
action_90 (65) = happyGoto action_129
action_90 (66) = happyGoto action_130
action_90 _ = happyFail

action_91 _ = happyReduce_27

action_92 _ = happyReduce_60

action_93 (67) = happyShift action_33
action_93 (70) = happyShift action_34
action_93 (72) = happyShift action_80
action_93 (80) = happyShift action_36
action_93 (117) = happyShift action_108
action_93 (17) = happyGoto action_98
action_93 (19) = happyGoto action_107
action_93 (21) = happyGoto action_29
action_93 (22) = happyGoto action_30
action_93 (24) = happyGoto action_31
action_93 (25) = happyGoto action_32
action_93 (28) = happyGoto action_100
action_93 _ = happyFail

action_94 _ = happyReduce_22

action_95 (67) = happyShift action_33
action_95 (70) = happyShift action_34
action_95 (72) = happyShift action_80
action_95 (73) = happyShift action_81
action_95 (79) = happyShift action_82
action_95 (80) = happyShift action_36
action_95 (81) = happyShift action_83
action_95 (85) = happyShift action_84
action_95 (87) = happyShift action_85
action_95 (89) = happyShift action_86
action_95 (90) = happyShift action_87
action_95 (91) = happyShift action_88
action_95 (92) = happyShift action_89
action_95 (116) = happyShift action_90
action_95 (118) = happyShift action_45
action_95 (119) = happyShift action_106
action_95 (120) = happyShift action_92
action_95 (17) = happyGoto action_59
action_95 (21) = happyGoto action_29
action_95 (22) = happyGoto action_30
action_95 (24) = happyGoto action_61
action_95 (25) = happyGoto action_32
action_95 (26) = happyGoto action_62
action_95 (27) = happyGoto action_105
action_95 (29) = happyGoto action_64
action_95 (31) = happyGoto action_65
action_95 (32) = happyGoto action_66
action_95 (33) = happyGoto action_67
action_95 (34) = happyGoto action_68
action_95 (36) = happyGoto action_69
action_95 (39) = happyGoto action_70
action_95 (40) = happyGoto action_71
action_95 (41) = happyGoto action_72
action_95 (45) = happyGoto action_73
action_95 (47) = happyGoto action_74
action_95 (48) = happyGoto action_75
action_95 (50) = happyGoto action_76
action_95 (52) = happyGoto action_77
action_95 (53) = happyGoto action_78
action_95 (55) = happyGoto action_79
action_95 _ = happyFail

action_96 _ = happyReduce_48

action_97 _ = happyReduce_46

action_98 (72) = happyShift action_104
action_98 _ = happyFail

action_99 (117) = happyShift action_102
action_99 (128) = happyShift action_103
action_99 _ = happyFail

action_100 _ = happyReduce_35

action_101 _ = happyReduce_25

action_102 _ = happyReduce_26

action_103 (67) = happyShift action_33
action_103 (70) = happyShift action_34
action_103 (72) = happyShift action_80
action_103 (80) = happyShift action_36
action_103 (17) = happyGoto action_98
action_103 (21) = happyGoto action_29
action_103 (22) = happyGoto action_30
action_103 (24) = happyGoto action_31
action_103 (25) = happyGoto action_32
action_103 (28) = happyGoto action_192
action_103 _ = happyFail

action_104 _ = happyReduce_53

action_105 _ = happyReduce_38

action_106 _ = happyReduce_49

action_107 (117) = happyShift action_191
action_107 (128) = happyShift action_103
action_107 _ = happyFail

action_108 _ = happyReduce_43

action_109 (105) = happyReduce_82
action_109 (106) = happyReduce_82
action_109 (107) = happyReduce_82
action_109 (108) = happyReduce_82
action_109 (109) = happyReduce_82
action_109 (110) = happyReduce_82
action_109 (111) = happyReduce_82
action_109 (112) = happyReduce_82
action_109 (113) = happyReduce_82
action_109 (114) = happyReduce_82
action_109 (115) = happyReduce_82
action_109 (116) = happyShift action_156
action_109 (121) = happyShift action_43
action_109 (122) = happyReduce_82
action_109 _ = happyReduce_136

action_110 (117) = happyShift action_190
action_110 _ = happyFail

action_111 _ = happyReduce_58

action_112 _ = happyReduce_67

action_113 _ = happyReduce_66

action_114 _ = happyReduce_73

action_115 _ = happyReduce_110

action_116 _ = happyReduce_108

action_117 _ = happyReduce_81

action_118 (121) = happyShift action_141
action_118 _ = happyReduce_135

action_119 (135) = happyShift action_189
action_119 _ = happyReduce_101

action_120 _ = happyReduce_103

action_121 (134) = happyShift action_188
action_121 _ = happyReduce_112

action_122 (93) = happyShift action_186
action_122 (96) = happyShift action_187
action_122 _ = happyReduce_117

action_123 (123) = happyShift action_184
action_123 (124) = happyShift action_185
action_123 _ = happyReduce_119

action_124 _ = happyReduce_122

action_125 (129) = happyShift action_182
action_125 (130) = happyShift action_183
action_125 _ = happyReduce_125

action_126 (131) = happyShift action_179
action_126 (132) = happyShift action_180
action_126 (133) = happyShift action_181
action_126 _ = happyReduce_126

action_127 _ = happyReduce_137

action_128 _ = happyReduce_142

action_129 _ = happyReduce_140

action_130 _ = happyReduce_129

action_131 (67) = happyShift action_33
action_131 (70) = happyShift action_34
action_131 (72) = happyShift action_80
action_131 (73) = happyShift action_81
action_131 (80) = happyShift action_36
action_131 (81) = happyShift action_83
action_131 (87) = happyShift action_85
action_131 (90) = happyShift action_87
action_131 (91) = happyShift action_88
action_131 (92) = happyShift action_89
action_131 (116) = happyShift action_131
action_131 (125) = happyShift action_132
action_131 (126) = happyShift action_133
action_131 (129) = happyShift action_134
action_131 (130) = happyShift action_135
action_131 (21) = happyGoto action_178
action_131 (24) = happyGoto action_109
action_131 (30) = happyGoto action_110
action_131 (35) = happyGoto action_111
action_131 (38) = happyGoto action_112
action_131 (39) = happyGoto action_113
action_131 (40) = happyGoto action_71
action_131 (44) = happyGoto action_114
action_131 (45) = happyGoto action_73
action_131 (47) = happyGoto action_115
action_131 (48) = happyGoto action_116
action_131 (49) = happyGoto action_117
action_131 (50) = happyGoto action_118
action_131 (51) = happyGoto action_119
action_131 (52) = happyGoto action_77
action_131 (53) = happyGoto action_78
action_131 (54) = happyGoto action_120
action_131 (55) = happyGoto action_79
action_131 (56) = happyGoto action_121
action_131 (57) = happyGoto action_122
action_131 (58) = happyGoto action_123
action_131 (59) = happyGoto action_124
action_131 (60) = happyGoto action_125
action_131 (61) = happyGoto action_126
action_131 (63) = happyGoto action_127
action_131 (64) = happyGoto action_128
action_131 (65) = happyGoto action_129
action_131 (66) = happyGoto action_130
action_131 _ = happyFail

action_132 (72) = happyShift action_80
action_132 (73) = happyShift action_81
action_132 (81) = happyShift action_83
action_132 (87) = happyShift action_85
action_132 (90) = happyShift action_87
action_132 (91) = happyShift action_88
action_132 (92) = happyShift action_89
action_132 (116) = happyShift action_131
action_132 (125) = happyShift action_132
action_132 (126) = happyShift action_133
action_132 (129) = happyShift action_134
action_132 (130) = happyShift action_135
action_132 (24) = happyGoto action_173
action_132 (47) = happyGoto action_115
action_132 (48) = happyGoto action_116
action_132 (50) = happyGoto action_118
action_132 (52) = happyGoto action_77
action_132 (53) = happyGoto action_78
action_132 (55) = happyGoto action_79
action_132 (63) = happyGoto action_127
action_132 (64) = happyGoto action_128
action_132 (65) = happyGoto action_129
action_132 (66) = happyGoto action_177
action_132 _ = happyFail

action_133 (72) = happyShift action_80
action_133 (73) = happyShift action_81
action_133 (81) = happyShift action_83
action_133 (87) = happyShift action_85
action_133 (90) = happyShift action_87
action_133 (91) = happyShift action_88
action_133 (92) = happyShift action_89
action_133 (116) = happyShift action_131
action_133 (125) = happyShift action_132
action_133 (126) = happyShift action_133
action_133 (129) = happyShift action_134
action_133 (130) = happyShift action_135
action_133 (24) = happyGoto action_173
action_133 (47) = happyGoto action_115
action_133 (48) = happyGoto action_116
action_133 (50) = happyGoto action_118
action_133 (52) = happyGoto action_77
action_133 (53) = happyGoto action_78
action_133 (55) = happyGoto action_79
action_133 (63) = happyGoto action_127
action_133 (64) = happyGoto action_128
action_133 (65) = happyGoto action_129
action_133 (66) = happyGoto action_176
action_133 _ = happyFail

action_134 (72) = happyShift action_80
action_134 (73) = happyShift action_81
action_134 (81) = happyShift action_83
action_134 (87) = happyShift action_85
action_134 (90) = happyShift action_87
action_134 (91) = happyShift action_88
action_134 (92) = happyShift action_89
action_134 (116) = happyShift action_131
action_134 (125) = happyShift action_132
action_134 (126) = happyShift action_133
action_134 (129) = happyShift action_134
action_134 (130) = happyShift action_135
action_134 (24) = happyGoto action_173
action_134 (47) = happyGoto action_115
action_134 (48) = happyGoto action_116
action_134 (50) = happyGoto action_118
action_134 (52) = happyGoto action_77
action_134 (53) = happyGoto action_78
action_134 (55) = happyGoto action_79
action_134 (63) = happyGoto action_127
action_134 (64) = happyGoto action_128
action_134 (65) = happyGoto action_129
action_134 (66) = happyGoto action_175
action_134 _ = happyFail

action_135 (72) = happyShift action_80
action_135 (73) = happyShift action_81
action_135 (81) = happyShift action_83
action_135 (87) = happyShift action_85
action_135 (90) = happyShift action_87
action_135 (91) = happyShift action_88
action_135 (92) = happyShift action_89
action_135 (116) = happyShift action_131
action_135 (125) = happyShift action_132
action_135 (126) = happyShift action_133
action_135 (129) = happyShift action_134
action_135 (130) = happyShift action_135
action_135 (24) = happyGoto action_173
action_135 (47) = happyGoto action_115
action_135 (48) = happyGoto action_116
action_135 (50) = happyGoto action_118
action_135 (52) = happyGoto action_77
action_135 (53) = happyGoto action_78
action_135 (55) = happyGoto action_79
action_135 (63) = happyGoto action_127
action_135 (64) = happyGoto action_128
action_135 (65) = happyGoto action_129
action_135 (66) = happyGoto action_174
action_135 _ = happyFail

action_136 (72) = happyShift action_80
action_136 (73) = happyShift action_81
action_136 (81) = happyShift action_83
action_136 (87) = happyShift action_85
action_136 (90) = happyShift action_87
action_136 (91) = happyShift action_88
action_136 (92) = happyShift action_89
action_136 (116) = happyShift action_131
action_136 (125) = happyShift action_132
action_136 (126) = happyShift action_133
action_136 (129) = happyShift action_134
action_136 (130) = happyShift action_135
action_136 (24) = happyGoto action_109
action_136 (30) = happyGoto action_172
action_136 (35) = happyGoto action_111
action_136 (38) = happyGoto action_112
action_136 (39) = happyGoto action_113
action_136 (40) = happyGoto action_71
action_136 (44) = happyGoto action_114
action_136 (45) = happyGoto action_73
action_136 (47) = happyGoto action_115
action_136 (48) = happyGoto action_116
action_136 (49) = happyGoto action_117
action_136 (50) = happyGoto action_118
action_136 (51) = happyGoto action_119
action_136 (52) = happyGoto action_77
action_136 (53) = happyGoto action_78
action_136 (54) = happyGoto action_120
action_136 (55) = happyGoto action_79
action_136 (56) = happyGoto action_121
action_136 (57) = happyGoto action_122
action_136 (58) = happyGoto action_123
action_136 (59) = happyGoto action_124
action_136 (60) = happyGoto action_125
action_136 (61) = happyGoto action_126
action_136 (63) = happyGoto action_127
action_136 (64) = happyGoto action_128
action_136 (65) = happyGoto action_129
action_136 (66) = happyGoto action_130
action_136 _ = happyFail

action_137 (120) = happyShift action_171
action_137 _ = happyFail

action_138 _ = happyReduce_68

action_139 (116) = happyShift action_170
action_139 _ = happyFail

action_140 (72) = happyShift action_80
action_140 (73) = happyShift action_81
action_140 (81) = happyShift action_83
action_140 (87) = happyShift action_85
action_140 (90) = happyShift action_87
action_140 (91) = happyShift action_88
action_140 (92) = happyShift action_89
action_140 (116) = happyShift action_131
action_140 (125) = happyShift action_132
action_140 (126) = happyShift action_133
action_140 (129) = happyShift action_134
action_140 (130) = happyShift action_135
action_140 (24) = happyGoto action_109
action_140 (30) = happyGoto action_169
action_140 (35) = happyGoto action_111
action_140 (38) = happyGoto action_112
action_140 (39) = happyGoto action_113
action_140 (40) = happyGoto action_71
action_140 (44) = happyGoto action_114
action_140 (45) = happyGoto action_73
action_140 (47) = happyGoto action_115
action_140 (48) = happyGoto action_116
action_140 (49) = happyGoto action_117
action_140 (50) = happyGoto action_118
action_140 (51) = happyGoto action_119
action_140 (52) = happyGoto action_77
action_140 (53) = happyGoto action_78
action_140 (54) = happyGoto action_120
action_140 (55) = happyGoto action_79
action_140 (56) = happyGoto action_121
action_140 (57) = happyGoto action_122
action_140 (58) = happyGoto action_123
action_140 (59) = happyGoto action_124
action_140 (60) = happyGoto action_125
action_140 (61) = happyGoto action_126
action_140 (63) = happyGoto action_127
action_140 (64) = happyGoto action_128
action_140 (65) = happyGoto action_129
action_140 (66) = happyGoto action_130
action_140 _ = happyFail

action_141 (72) = happyShift action_168
action_141 _ = happyFail

action_142 (72) = happyShift action_80
action_142 (73) = happyShift action_81
action_142 (81) = happyShift action_83
action_142 (87) = happyShift action_85
action_142 (90) = happyShift action_87
action_142 (91) = happyShift action_88
action_142 (92) = happyShift action_89
action_142 (116) = happyShift action_131
action_142 (125) = happyShift action_132
action_142 (126) = happyShift action_133
action_142 (129) = happyShift action_134
action_142 (130) = happyShift action_135
action_142 (24) = happyGoto action_109
action_142 (35) = happyGoto action_167
action_142 (38) = happyGoto action_112
action_142 (39) = happyGoto action_113
action_142 (40) = happyGoto action_71
action_142 (44) = happyGoto action_114
action_142 (45) = happyGoto action_73
action_142 (47) = happyGoto action_115
action_142 (48) = happyGoto action_116
action_142 (49) = happyGoto action_117
action_142 (50) = happyGoto action_118
action_142 (51) = happyGoto action_119
action_142 (52) = happyGoto action_77
action_142 (53) = happyGoto action_78
action_142 (54) = happyGoto action_120
action_142 (55) = happyGoto action_79
action_142 (56) = happyGoto action_121
action_142 (57) = happyGoto action_122
action_142 (58) = happyGoto action_123
action_142 (59) = happyGoto action_124
action_142 (60) = happyGoto action_125
action_142 (61) = happyGoto action_126
action_142 (63) = happyGoto action_127
action_142 (64) = happyGoto action_128
action_142 (65) = happyGoto action_129
action_142 (66) = happyGoto action_130
action_142 _ = happyFail

action_143 _ = happyReduce_87

action_144 _ = happyReduce_88

action_145 _ = happyReduce_84

action_146 _ = happyReduce_85

action_147 _ = happyReduce_92

action_148 _ = happyReduce_94

action_149 _ = happyReduce_93

action_150 _ = happyReduce_86

action_151 _ = happyReduce_89

action_152 _ = happyReduce_90

action_153 _ = happyReduce_91

action_154 _ = happyReduce_83

action_155 _ = happyReduce_61

action_156 (72) = happyShift action_80
action_156 (73) = happyShift action_81
action_156 (81) = happyShift action_83
action_156 (87) = happyShift action_85
action_156 (90) = happyShift action_87
action_156 (91) = happyShift action_88
action_156 (92) = happyShift action_89
action_156 (116) = happyShift action_131
action_156 (117) = happyShift action_166
action_156 (125) = happyShift action_132
action_156 (126) = happyShift action_133
action_156 (129) = happyShift action_134
action_156 (130) = happyShift action_135
action_156 (24) = happyGoto action_109
action_156 (30) = happyGoto action_164
action_156 (35) = happyGoto action_111
action_156 (38) = happyGoto action_112
action_156 (39) = happyGoto action_113
action_156 (40) = happyGoto action_71
action_156 (44) = happyGoto action_114
action_156 (45) = happyGoto action_73
action_156 (47) = happyGoto action_115
action_156 (48) = happyGoto action_116
action_156 (49) = happyGoto action_117
action_156 (50) = happyGoto action_118
action_156 (51) = happyGoto action_119
action_156 (52) = happyGoto action_77
action_156 (53) = happyGoto action_78
action_156 (54) = happyGoto action_120
action_156 (55) = happyGoto action_79
action_156 (56) = happyGoto action_121
action_156 (57) = happyGoto action_122
action_156 (58) = happyGoto action_123
action_156 (59) = happyGoto action_124
action_156 (60) = happyGoto action_125
action_156 (61) = happyGoto action_126
action_156 (62) = happyGoto action_165
action_156 (63) = happyGoto action_127
action_156 (64) = happyGoto action_128
action_156 (65) = happyGoto action_129
action_156 (66) = happyGoto action_130
action_156 _ = happyFail

action_157 _ = happyReduce_28

action_158 (121) = happyShift action_43
action_158 _ = happyReduce_82

action_159 (120) = happyShift action_163
action_159 _ = happyFail

action_160 (120) = happyShift action_162
action_160 _ = happyReduce_45

action_161 _ = happyReduce_23

action_162 _ = happyReduce_50

action_163 _ = happyReduce_51

action_164 _ = happyReduce_133

action_165 (117) = happyShift action_210
action_165 (128) = happyShift action_211
action_165 _ = happyFail

action_166 _ = happyReduce_95

action_167 _ = happyReduce_75

action_168 (116) = happyShift action_209
action_168 _ = happyReduce_111

action_169 (117) = happyShift action_208
action_169 _ = happyFail

action_170 (72) = happyShift action_80
action_170 (73) = happyShift action_81
action_170 (81) = happyShift action_83
action_170 (87) = happyShift action_85
action_170 (90) = happyShift action_87
action_170 (91) = happyShift action_88
action_170 (92) = happyShift action_89
action_170 (116) = happyShift action_131
action_170 (117) = happyShift action_207
action_170 (125) = happyShift action_132
action_170 (126) = happyShift action_133
action_170 (129) = happyShift action_134
action_170 (130) = happyShift action_135
action_170 (24) = happyGoto action_109
action_170 (30) = happyGoto action_164
action_170 (35) = happyGoto action_111
action_170 (38) = happyGoto action_112
action_170 (39) = happyGoto action_113
action_170 (40) = happyGoto action_71
action_170 (44) = happyGoto action_114
action_170 (45) = happyGoto action_73
action_170 (47) = happyGoto action_115
action_170 (48) = happyGoto action_116
action_170 (49) = happyGoto action_117
action_170 (50) = happyGoto action_118
action_170 (51) = happyGoto action_119
action_170 (52) = happyGoto action_77
action_170 (53) = happyGoto action_78
action_170 (54) = happyGoto action_120
action_170 (55) = happyGoto action_79
action_170 (56) = happyGoto action_121
action_170 (57) = happyGoto action_122
action_170 (58) = happyGoto action_123
action_170 (59) = happyGoto action_124
action_170 (60) = happyGoto action_125
action_170 (61) = happyGoto action_126
action_170 (62) = happyGoto action_206
action_170 (63) = happyGoto action_127
action_170 (64) = happyGoto action_128
action_170 (65) = happyGoto action_129
action_170 (66) = happyGoto action_130
action_170 _ = happyFail

action_171 _ = happyReduce_69

action_172 (117) = happyShift action_205
action_172 _ = happyFail

action_173 (116) = happyShift action_156
action_173 (121) = happyShift action_43
action_173 _ = happyReduce_136

action_174 _ = happyReduce_144

action_175 _ = happyReduce_143

action_176 _ = happyReduce_138

action_177 _ = happyReduce_139

action_178 (117) = happyShift action_204
action_178 _ = happyFail

action_179 (72) = happyShift action_80
action_179 (73) = happyShift action_81
action_179 (81) = happyShift action_83
action_179 (87) = happyShift action_85
action_179 (90) = happyShift action_87
action_179 (91) = happyShift action_88
action_179 (92) = happyShift action_89
action_179 (116) = happyShift action_131
action_179 (125) = happyShift action_132
action_179 (126) = happyShift action_133
action_179 (129) = happyShift action_134
action_179 (130) = happyShift action_135
action_179 (24) = happyGoto action_173
action_179 (47) = happyGoto action_115
action_179 (48) = happyGoto action_116
action_179 (50) = happyGoto action_118
action_179 (52) = happyGoto action_77
action_179 (53) = happyGoto action_78
action_179 (55) = happyGoto action_79
action_179 (63) = happyGoto action_127
action_179 (64) = happyGoto action_128
action_179 (65) = happyGoto action_129
action_179 (66) = happyGoto action_203
action_179 _ = happyFail

action_180 (72) = happyShift action_80
action_180 (73) = happyShift action_81
action_180 (81) = happyShift action_83
action_180 (87) = happyShift action_85
action_180 (90) = happyShift action_87
action_180 (91) = happyShift action_88
action_180 (92) = happyShift action_89
action_180 (116) = happyShift action_131
action_180 (125) = happyShift action_132
action_180 (126) = happyShift action_133
action_180 (129) = happyShift action_134
action_180 (130) = happyShift action_135
action_180 (24) = happyGoto action_173
action_180 (47) = happyGoto action_115
action_180 (48) = happyGoto action_116
action_180 (50) = happyGoto action_118
action_180 (52) = happyGoto action_77
action_180 (53) = happyGoto action_78
action_180 (55) = happyGoto action_79
action_180 (63) = happyGoto action_127
action_180 (64) = happyGoto action_128
action_180 (65) = happyGoto action_129
action_180 (66) = happyGoto action_202
action_180 _ = happyFail

action_181 (72) = happyShift action_80
action_181 (73) = happyShift action_81
action_181 (81) = happyShift action_83
action_181 (87) = happyShift action_85
action_181 (90) = happyShift action_87
action_181 (91) = happyShift action_88
action_181 (92) = happyShift action_89
action_181 (116) = happyShift action_131
action_181 (125) = happyShift action_132
action_181 (126) = happyShift action_133
action_181 (129) = happyShift action_134
action_181 (130) = happyShift action_135
action_181 (24) = happyGoto action_173
action_181 (47) = happyGoto action_115
action_181 (48) = happyGoto action_116
action_181 (50) = happyGoto action_118
action_181 (52) = happyGoto action_77
action_181 (53) = happyGoto action_78
action_181 (55) = happyGoto action_79
action_181 (63) = happyGoto action_127
action_181 (64) = happyGoto action_128
action_181 (65) = happyGoto action_129
action_181 (66) = happyGoto action_201
action_181 _ = happyFail

action_182 (72) = happyShift action_80
action_182 (73) = happyShift action_81
action_182 (81) = happyShift action_83
action_182 (87) = happyShift action_85
action_182 (90) = happyShift action_87
action_182 (91) = happyShift action_88
action_182 (92) = happyShift action_89
action_182 (116) = happyShift action_131
action_182 (125) = happyShift action_132
action_182 (126) = happyShift action_133
action_182 (129) = happyShift action_134
action_182 (130) = happyShift action_135
action_182 (24) = happyGoto action_173
action_182 (47) = happyGoto action_115
action_182 (48) = happyGoto action_116
action_182 (50) = happyGoto action_118
action_182 (52) = happyGoto action_77
action_182 (53) = happyGoto action_78
action_182 (55) = happyGoto action_79
action_182 (61) = happyGoto action_200
action_182 (63) = happyGoto action_127
action_182 (64) = happyGoto action_128
action_182 (65) = happyGoto action_129
action_182 (66) = happyGoto action_130
action_182 _ = happyFail

action_183 (72) = happyShift action_80
action_183 (73) = happyShift action_81
action_183 (81) = happyShift action_83
action_183 (87) = happyShift action_85
action_183 (90) = happyShift action_87
action_183 (91) = happyShift action_88
action_183 (92) = happyShift action_89
action_183 (116) = happyShift action_131
action_183 (125) = happyShift action_132
action_183 (126) = happyShift action_133
action_183 (129) = happyShift action_134
action_183 (130) = happyShift action_135
action_183 (24) = happyGoto action_173
action_183 (47) = happyGoto action_115
action_183 (48) = happyGoto action_116
action_183 (50) = happyGoto action_118
action_183 (52) = happyGoto action_77
action_183 (53) = happyGoto action_78
action_183 (55) = happyGoto action_79
action_183 (61) = happyGoto action_199
action_183 (63) = happyGoto action_127
action_183 (64) = happyGoto action_128
action_183 (65) = happyGoto action_129
action_183 (66) = happyGoto action_130
action_183 _ = happyFail

action_184 (72) = happyShift action_80
action_184 (73) = happyShift action_81
action_184 (81) = happyShift action_83
action_184 (87) = happyShift action_85
action_184 (90) = happyShift action_87
action_184 (91) = happyShift action_88
action_184 (92) = happyShift action_89
action_184 (116) = happyShift action_131
action_184 (125) = happyShift action_132
action_184 (126) = happyShift action_133
action_184 (129) = happyShift action_134
action_184 (130) = happyShift action_135
action_184 (24) = happyGoto action_173
action_184 (47) = happyGoto action_115
action_184 (48) = happyGoto action_116
action_184 (50) = happyGoto action_118
action_184 (52) = happyGoto action_77
action_184 (53) = happyGoto action_78
action_184 (55) = happyGoto action_79
action_184 (59) = happyGoto action_198
action_184 (60) = happyGoto action_125
action_184 (61) = happyGoto action_126
action_184 (63) = happyGoto action_127
action_184 (64) = happyGoto action_128
action_184 (65) = happyGoto action_129
action_184 (66) = happyGoto action_130
action_184 _ = happyFail

action_185 (72) = happyShift action_80
action_185 (73) = happyShift action_81
action_185 (81) = happyShift action_83
action_185 (87) = happyShift action_85
action_185 (90) = happyShift action_87
action_185 (91) = happyShift action_88
action_185 (92) = happyShift action_89
action_185 (116) = happyShift action_131
action_185 (125) = happyShift action_132
action_185 (126) = happyShift action_133
action_185 (129) = happyShift action_134
action_185 (130) = happyShift action_135
action_185 (24) = happyGoto action_173
action_185 (47) = happyGoto action_115
action_185 (48) = happyGoto action_116
action_185 (50) = happyGoto action_118
action_185 (52) = happyGoto action_77
action_185 (53) = happyGoto action_78
action_185 (55) = happyGoto action_79
action_185 (59) = happyGoto action_197
action_185 (60) = happyGoto action_125
action_185 (61) = happyGoto action_126
action_185 (63) = happyGoto action_127
action_185 (64) = happyGoto action_128
action_185 (65) = happyGoto action_129
action_185 (66) = happyGoto action_130
action_185 _ = happyFail

action_186 (72) = happyShift action_80
action_186 (73) = happyShift action_81
action_186 (81) = happyShift action_83
action_186 (87) = happyShift action_85
action_186 (90) = happyShift action_87
action_186 (91) = happyShift action_88
action_186 (92) = happyShift action_89
action_186 (116) = happyShift action_131
action_186 (125) = happyShift action_132
action_186 (126) = happyShift action_133
action_186 (129) = happyShift action_134
action_186 (130) = happyShift action_135
action_186 (24) = happyGoto action_173
action_186 (47) = happyGoto action_115
action_186 (48) = happyGoto action_116
action_186 (50) = happyGoto action_118
action_186 (52) = happyGoto action_77
action_186 (53) = happyGoto action_78
action_186 (55) = happyGoto action_79
action_186 (58) = happyGoto action_196
action_186 (59) = happyGoto action_124
action_186 (60) = happyGoto action_125
action_186 (61) = happyGoto action_126
action_186 (63) = happyGoto action_127
action_186 (64) = happyGoto action_128
action_186 (65) = happyGoto action_129
action_186 (66) = happyGoto action_130
action_186 _ = happyFail

action_187 (72) = happyShift action_80
action_187 (73) = happyShift action_81
action_187 (81) = happyShift action_83
action_187 (87) = happyShift action_85
action_187 (90) = happyShift action_87
action_187 (91) = happyShift action_88
action_187 (92) = happyShift action_89
action_187 (116) = happyShift action_131
action_187 (125) = happyShift action_132
action_187 (126) = happyShift action_133
action_187 (129) = happyShift action_134
action_187 (130) = happyShift action_135
action_187 (24) = happyGoto action_173
action_187 (47) = happyGoto action_115
action_187 (48) = happyGoto action_116
action_187 (50) = happyGoto action_118
action_187 (52) = happyGoto action_77
action_187 (53) = happyGoto action_78
action_187 (55) = happyGoto action_79
action_187 (58) = happyGoto action_195
action_187 (59) = happyGoto action_124
action_187 (60) = happyGoto action_125
action_187 (61) = happyGoto action_126
action_187 (63) = happyGoto action_127
action_187 (64) = happyGoto action_128
action_187 (65) = happyGoto action_129
action_187 (66) = happyGoto action_130
action_187 _ = happyFail

action_188 (72) = happyShift action_80
action_188 (73) = happyShift action_81
action_188 (81) = happyShift action_83
action_188 (87) = happyShift action_85
action_188 (90) = happyShift action_87
action_188 (91) = happyShift action_88
action_188 (92) = happyShift action_89
action_188 (116) = happyShift action_131
action_188 (125) = happyShift action_132
action_188 (126) = happyShift action_133
action_188 (129) = happyShift action_134
action_188 (130) = happyShift action_135
action_188 (24) = happyGoto action_173
action_188 (47) = happyGoto action_115
action_188 (48) = happyGoto action_116
action_188 (50) = happyGoto action_118
action_188 (52) = happyGoto action_77
action_188 (53) = happyGoto action_78
action_188 (55) = happyGoto action_79
action_188 (57) = happyGoto action_194
action_188 (58) = happyGoto action_123
action_188 (59) = happyGoto action_124
action_188 (60) = happyGoto action_125
action_188 (61) = happyGoto action_126
action_188 (63) = happyGoto action_127
action_188 (64) = happyGoto action_128
action_188 (65) = happyGoto action_129
action_188 (66) = happyGoto action_130
action_188 _ = happyFail

action_189 (72) = happyShift action_80
action_189 (73) = happyShift action_81
action_189 (81) = happyShift action_83
action_189 (87) = happyShift action_85
action_189 (90) = happyShift action_87
action_189 (91) = happyShift action_88
action_189 (92) = happyShift action_89
action_189 (116) = happyShift action_131
action_189 (125) = happyShift action_132
action_189 (126) = happyShift action_133
action_189 (129) = happyShift action_134
action_189 (130) = happyShift action_135
action_189 (24) = happyGoto action_173
action_189 (47) = happyGoto action_115
action_189 (48) = happyGoto action_116
action_189 (50) = happyGoto action_118
action_189 (52) = happyGoto action_77
action_189 (53) = happyGoto action_78
action_189 (54) = happyGoto action_193
action_189 (55) = happyGoto action_79
action_189 (56) = happyGoto action_121
action_189 (57) = happyGoto action_122
action_189 (58) = happyGoto action_123
action_189 (59) = happyGoto action_124
action_189 (60) = happyGoto action_125
action_189 (61) = happyGoto action_126
action_189 (63) = happyGoto action_127
action_189 (64) = happyGoto action_128
action_189 (65) = happyGoto action_129
action_189 (66) = happyGoto action_130
action_189 _ = happyFail

action_190 _ = happyReduce_107

action_191 _ = happyReduce_44

action_192 _ = happyReduce_36

action_193 _ = happyReduce_104

action_194 (93) = happyShift action_186
action_194 (96) = happyShift action_187
action_194 _ = happyReduce_118

action_195 (123) = happyShift action_184
action_195 (124) = happyShift action_185
action_195 _ = happyReduce_121

action_196 (123) = happyShift action_184
action_196 (124) = happyShift action_185
action_196 _ = happyReduce_120

action_197 _ = happyReduce_124

action_198 _ = happyReduce_123

action_199 (131) = happyShift action_179
action_199 (132) = happyShift action_180
action_199 (133) = happyShift action_181
action_199 _ = happyReduce_128

action_200 (131) = happyShift action_179
action_200 (132) = happyShift action_180
action_200 (133) = happyShift action_181
action_200 _ = happyReduce_127

action_201 _ = happyReduce_132

action_202 _ = happyReduce_131

action_203 _ = happyReduce_130

action_204 (72) = happyShift action_80
action_204 (73) = happyShift action_81
action_204 (81) = happyShift action_83
action_204 (87) = happyShift action_85
action_204 (90) = happyShift action_87
action_204 (91) = happyShift action_88
action_204 (92) = happyShift action_89
action_204 (116) = happyShift action_131
action_204 (125) = happyShift action_132
action_204 (126) = happyShift action_133
action_204 (129) = happyShift action_134
action_204 (130) = happyShift action_135
action_204 (24) = happyGoto action_173
action_204 (47) = happyGoto action_115
action_204 (48) = happyGoto action_116
action_204 (50) = happyGoto action_118
action_204 (52) = happyGoto action_77
action_204 (53) = happyGoto action_78
action_204 (55) = happyGoto action_79
action_204 (63) = happyGoto action_127
action_204 (64) = happyGoto action_128
action_204 (65) = happyGoto action_129
action_204 (66) = happyGoto action_225
action_204 _ = happyFail

action_205 (72) = happyShift action_80
action_205 (73) = happyShift action_81
action_205 (79) = happyShift action_82
action_205 (81) = happyShift action_83
action_205 (85) = happyShift action_84
action_205 (87) = happyShift action_85
action_205 (89) = happyShift action_86
action_205 (90) = happyShift action_87
action_205 (91) = happyShift action_88
action_205 (92) = happyShift action_89
action_205 (116) = happyShift action_90
action_205 (118) = happyShift action_45
action_205 (120) = happyShift action_92
action_205 (24) = happyGoto action_215
action_205 (26) = happyGoto action_62
action_205 (29) = happyGoto action_224
action_205 (31) = happyGoto action_65
action_205 (32) = happyGoto action_66
action_205 (33) = happyGoto action_67
action_205 (34) = happyGoto action_68
action_205 (36) = happyGoto action_69
action_205 (39) = happyGoto action_70
action_205 (40) = happyGoto action_71
action_205 (41) = happyGoto action_72
action_205 (45) = happyGoto action_73
action_205 (47) = happyGoto action_74
action_205 (48) = happyGoto action_75
action_205 (50) = happyGoto action_76
action_205 (52) = happyGoto action_77
action_205 (53) = happyGoto action_78
action_205 (55) = happyGoto action_79
action_205 _ = happyFail

action_206 (117) = happyShift action_223
action_206 (128) = happyShift action_211
action_206 _ = happyFail

action_207 _ = happyReduce_99

action_208 (72) = happyShift action_80
action_208 (73) = happyShift action_81
action_208 (79) = happyShift action_221
action_208 (81) = happyShift action_83
action_208 (85) = happyShift action_84
action_208 (87) = happyShift action_85
action_208 (89) = happyShift action_222
action_208 (90) = happyShift action_87
action_208 (91) = happyShift action_88
action_208 (92) = happyShift action_89
action_208 (116) = happyShift action_90
action_208 (118) = happyShift action_45
action_208 (120) = happyShift action_92
action_208 (24) = happyGoto action_215
action_208 (26) = happyGoto action_62
action_208 (29) = happyGoto action_216
action_208 (31) = happyGoto action_217
action_208 (32) = happyGoto action_66
action_208 (33) = happyGoto action_67
action_208 (34) = happyGoto action_68
action_208 (36) = happyGoto action_69
action_208 (37) = happyGoto action_218
action_208 (39) = happyGoto action_70
action_208 (40) = happyGoto action_71
action_208 (41) = happyGoto action_72
action_208 (42) = happyGoto action_219
action_208 (43) = happyGoto action_220
action_208 (45) = happyGoto action_73
action_208 (47) = happyGoto action_74
action_208 (48) = happyGoto action_75
action_208 (50) = happyGoto action_76
action_208 (52) = happyGoto action_77
action_208 (53) = happyGoto action_78
action_208 (55) = happyGoto action_79
action_208 _ = happyFail

action_209 (72) = happyShift action_80
action_209 (73) = happyShift action_81
action_209 (81) = happyShift action_83
action_209 (87) = happyShift action_85
action_209 (90) = happyShift action_87
action_209 (91) = happyShift action_88
action_209 (92) = happyShift action_89
action_209 (116) = happyShift action_131
action_209 (117) = happyShift action_214
action_209 (125) = happyShift action_132
action_209 (126) = happyShift action_133
action_209 (129) = happyShift action_134
action_209 (130) = happyShift action_135
action_209 (24) = happyGoto action_109
action_209 (30) = happyGoto action_164
action_209 (35) = happyGoto action_111
action_209 (38) = happyGoto action_112
action_209 (39) = happyGoto action_113
action_209 (40) = happyGoto action_71
action_209 (44) = happyGoto action_114
action_209 (45) = happyGoto action_73
action_209 (47) = happyGoto action_115
action_209 (48) = happyGoto action_116
action_209 (49) = happyGoto action_117
action_209 (50) = happyGoto action_118
action_209 (51) = happyGoto action_119
action_209 (52) = happyGoto action_77
action_209 (53) = happyGoto action_78
action_209 (54) = happyGoto action_120
action_209 (55) = happyGoto action_79
action_209 (56) = happyGoto action_121
action_209 (57) = happyGoto action_122
action_209 (58) = happyGoto action_123
action_209 (59) = happyGoto action_124
action_209 (60) = happyGoto action_125
action_209 (61) = happyGoto action_126
action_209 (62) = happyGoto action_213
action_209 (63) = happyGoto action_127
action_209 (64) = happyGoto action_128
action_209 (65) = happyGoto action_129
action_209 (66) = happyGoto action_130
action_209 _ = happyFail

action_210 _ = happyReduce_96

action_211 (72) = happyShift action_80
action_211 (73) = happyShift action_81
action_211 (81) = happyShift action_83
action_211 (87) = happyShift action_85
action_211 (90) = happyShift action_87
action_211 (91) = happyShift action_88
action_211 (92) = happyShift action_89
action_211 (116) = happyShift action_131
action_211 (125) = happyShift action_132
action_211 (126) = happyShift action_133
action_211 (129) = happyShift action_134
action_211 (130) = happyShift action_135
action_211 (24) = happyGoto action_109
action_211 (30) = happyGoto action_212
action_211 (35) = happyGoto action_111
action_211 (38) = happyGoto action_112
action_211 (39) = happyGoto action_113
action_211 (40) = happyGoto action_71
action_211 (44) = happyGoto action_114
action_211 (45) = happyGoto action_73
action_211 (47) = happyGoto action_115
action_211 (48) = happyGoto action_116
action_211 (49) = happyGoto action_117
action_211 (50) = happyGoto action_118
action_211 (51) = happyGoto action_119
action_211 (52) = happyGoto action_77
action_211 (53) = happyGoto action_78
action_211 (54) = happyGoto action_120
action_211 (55) = happyGoto action_79
action_211 (56) = happyGoto action_121
action_211 (57) = happyGoto action_122
action_211 (58) = happyGoto action_123
action_211 (59) = happyGoto action_124
action_211 (60) = happyGoto action_125
action_211 (61) = happyGoto action_126
action_211 (63) = happyGoto action_127
action_211 (64) = happyGoto action_128
action_211 (65) = happyGoto action_129
action_211 (66) = happyGoto action_130
action_211 _ = happyFail

action_212 _ = happyReduce_134

action_213 (117) = happyShift action_229
action_213 (128) = happyShift action_211
action_213 _ = happyFail

action_214 _ = happyReduce_97

action_215 (116) = happyShift action_156
action_215 (121) = happyShift action_43
action_215 _ = happyReduce_82

action_216 _ = happyReduce_63

action_217 (77) = happyReduce_70
action_217 _ = happyReduce_54

action_218 (77) = happyShift action_228
action_218 _ = happyFail

action_219 _ = happyReduce_71

action_220 _ = happyReduce_72

action_221 (116) = happyShift action_227
action_221 _ = happyFail

action_222 (116) = happyShift action_226
action_222 _ = happyFail

action_223 _ = happyReduce_100

action_224 _ = happyReduce_65

action_225 _ = happyReduce_141

action_226 (72) = happyShift action_80
action_226 (73) = happyShift action_81
action_226 (81) = happyShift action_83
action_226 (87) = happyShift action_85
action_226 (90) = happyShift action_87
action_226 (91) = happyShift action_88
action_226 (92) = happyShift action_89
action_226 (116) = happyShift action_131
action_226 (125) = happyShift action_132
action_226 (126) = happyShift action_133
action_226 (129) = happyShift action_134
action_226 (130) = happyShift action_135
action_226 (24) = happyGoto action_109
action_226 (30) = happyGoto action_232
action_226 (35) = happyGoto action_111
action_226 (38) = happyGoto action_112
action_226 (39) = happyGoto action_113
action_226 (40) = happyGoto action_71
action_226 (44) = happyGoto action_114
action_226 (45) = happyGoto action_73
action_226 (47) = happyGoto action_115
action_226 (48) = happyGoto action_116
action_226 (49) = happyGoto action_117
action_226 (50) = happyGoto action_118
action_226 (51) = happyGoto action_119
action_226 (52) = happyGoto action_77
action_226 (53) = happyGoto action_78
action_226 (54) = happyGoto action_120
action_226 (55) = happyGoto action_79
action_226 (56) = happyGoto action_121
action_226 (57) = happyGoto action_122
action_226 (58) = happyGoto action_123
action_226 (59) = happyGoto action_124
action_226 (60) = happyGoto action_125
action_226 (61) = happyGoto action_126
action_226 (63) = happyGoto action_127
action_226 (64) = happyGoto action_128
action_226 (65) = happyGoto action_129
action_226 (66) = happyGoto action_130
action_226 _ = happyFail

action_227 (72) = happyShift action_80
action_227 (73) = happyShift action_81
action_227 (81) = happyShift action_83
action_227 (87) = happyShift action_85
action_227 (90) = happyShift action_87
action_227 (91) = happyShift action_88
action_227 (92) = happyShift action_89
action_227 (116) = happyShift action_131
action_227 (125) = happyShift action_132
action_227 (126) = happyShift action_133
action_227 (129) = happyShift action_134
action_227 (130) = happyShift action_135
action_227 (24) = happyGoto action_109
action_227 (30) = happyGoto action_231
action_227 (35) = happyGoto action_111
action_227 (38) = happyGoto action_112
action_227 (39) = happyGoto action_113
action_227 (40) = happyGoto action_71
action_227 (44) = happyGoto action_114
action_227 (45) = happyGoto action_73
action_227 (47) = happyGoto action_115
action_227 (48) = happyGoto action_116
action_227 (49) = happyGoto action_117
action_227 (50) = happyGoto action_118
action_227 (51) = happyGoto action_119
action_227 (52) = happyGoto action_77
action_227 (53) = happyGoto action_78
action_227 (54) = happyGoto action_120
action_227 (55) = happyGoto action_79
action_227 (56) = happyGoto action_121
action_227 (57) = happyGoto action_122
action_227 (58) = happyGoto action_123
action_227 (59) = happyGoto action_124
action_227 (60) = happyGoto action_125
action_227 (61) = happyGoto action_126
action_227 (63) = happyGoto action_127
action_227 (64) = happyGoto action_128
action_227 (65) = happyGoto action_129
action_227 (66) = happyGoto action_130
action_227 _ = happyFail

action_228 (72) = happyShift action_80
action_228 (73) = happyShift action_81
action_228 (79) = happyShift action_82
action_228 (81) = happyShift action_83
action_228 (85) = happyShift action_84
action_228 (87) = happyShift action_85
action_228 (89) = happyShift action_86
action_228 (90) = happyShift action_87
action_228 (91) = happyShift action_88
action_228 (92) = happyShift action_89
action_228 (116) = happyShift action_90
action_228 (118) = happyShift action_45
action_228 (120) = happyShift action_92
action_228 (24) = happyGoto action_215
action_228 (26) = happyGoto action_62
action_228 (29) = happyGoto action_230
action_228 (31) = happyGoto action_65
action_228 (32) = happyGoto action_66
action_228 (33) = happyGoto action_67
action_228 (34) = happyGoto action_68
action_228 (36) = happyGoto action_69
action_228 (39) = happyGoto action_70
action_228 (40) = happyGoto action_71
action_228 (41) = happyGoto action_72
action_228 (45) = happyGoto action_73
action_228 (47) = happyGoto action_74
action_228 (48) = happyGoto action_75
action_228 (50) = happyGoto action_76
action_228 (52) = happyGoto action_77
action_228 (53) = happyGoto action_78
action_228 (55) = happyGoto action_79
action_228 _ = happyFail

action_229 _ = happyReduce_98

action_230 _ = happyReduce_64

action_231 (117) = happyShift action_234
action_231 _ = happyFail

action_232 (117) = happyShift action_233
action_232 _ = happyFail

action_233 (72) = happyShift action_80
action_233 (73) = happyShift action_81
action_233 (79) = happyShift action_221
action_233 (81) = happyShift action_83
action_233 (85) = happyShift action_84
action_233 (87) = happyShift action_85
action_233 (89) = happyShift action_222
action_233 (90) = happyShift action_87
action_233 (91) = happyShift action_88
action_233 (92) = happyShift action_89
action_233 (116) = happyShift action_90
action_233 (118) = happyShift action_45
action_233 (120) = happyShift action_92
action_233 (24) = happyGoto action_215
action_233 (26) = happyGoto action_62
action_233 (29) = happyGoto action_224
action_233 (31) = happyGoto action_217
action_233 (32) = happyGoto action_66
action_233 (33) = happyGoto action_67
action_233 (34) = happyGoto action_68
action_233 (36) = happyGoto action_69
action_233 (37) = happyGoto action_236
action_233 (39) = happyGoto action_70
action_233 (40) = happyGoto action_71
action_233 (41) = happyGoto action_72
action_233 (42) = happyGoto action_219
action_233 (43) = happyGoto action_220
action_233 (45) = happyGoto action_73
action_233 (47) = happyGoto action_74
action_233 (48) = happyGoto action_75
action_233 (50) = happyGoto action_76
action_233 (52) = happyGoto action_77
action_233 (53) = happyGoto action_78
action_233 (55) = happyGoto action_79
action_233 _ = happyFail

action_234 (72) = happyShift action_80
action_234 (73) = happyShift action_81
action_234 (79) = happyShift action_221
action_234 (81) = happyShift action_83
action_234 (85) = happyShift action_84
action_234 (87) = happyShift action_85
action_234 (89) = happyShift action_222
action_234 (90) = happyShift action_87
action_234 (91) = happyShift action_88
action_234 (92) = happyShift action_89
action_234 (116) = happyShift action_90
action_234 (118) = happyShift action_45
action_234 (120) = happyShift action_92
action_234 (24) = happyGoto action_215
action_234 (26) = happyGoto action_62
action_234 (29) = happyGoto action_216
action_234 (31) = happyGoto action_217
action_234 (32) = happyGoto action_66
action_234 (33) = happyGoto action_67
action_234 (34) = happyGoto action_68
action_234 (36) = happyGoto action_69
action_234 (37) = happyGoto action_235
action_234 (39) = happyGoto action_70
action_234 (40) = happyGoto action_71
action_234 (41) = happyGoto action_72
action_234 (42) = happyGoto action_219
action_234 (43) = happyGoto action_220
action_234 (45) = happyGoto action_73
action_234 (47) = happyGoto action_74
action_234 (48) = happyGoto action_75
action_234 (50) = happyGoto action_76
action_234 (52) = happyGoto action_77
action_234 (53) = happyGoto action_78
action_234 (55) = happyGoto action_79
action_234 _ = happyFail

action_235 (77) = happyShift action_237
action_235 _ = happyFail

action_236 _ = happyReduce_80

action_237 (72) = happyShift action_80
action_237 (73) = happyShift action_81
action_237 (79) = happyShift action_221
action_237 (81) = happyShift action_83
action_237 (85) = happyShift action_84
action_237 (87) = happyShift action_85
action_237 (89) = happyShift action_222
action_237 (90) = happyShift action_87
action_237 (91) = happyShift action_88
action_237 (92) = happyShift action_89
action_237 (116) = happyShift action_90
action_237 (118) = happyShift action_45
action_237 (120) = happyShift action_92
action_237 (24) = happyGoto action_215
action_237 (26) = happyGoto action_62
action_237 (29) = happyGoto action_230
action_237 (31) = happyGoto action_217
action_237 (32) = happyGoto action_66
action_237 (33) = happyGoto action_67
action_237 (34) = happyGoto action_68
action_237 (36) = happyGoto action_69
action_237 (37) = happyGoto action_238
action_237 (39) = happyGoto action_70
action_237 (40) = happyGoto action_71
action_237 (41) = happyGoto action_72
action_237 (42) = happyGoto action_219
action_237 (43) = happyGoto action_220
action_237 (45) = happyGoto action_73
action_237 (47) = happyGoto action_74
action_237 (48) = happyGoto action_75
action_237 (50) = happyGoto action_76
action_237 (52) = happyGoto action_77
action_237 (53) = happyGoto action_78
action_237 (55) = happyGoto action_79
action_237 _ = happyFail

action_238 _ = happyReduce_79

happyReduce_1 = happySpecReduce_1  4 happyReduction_1
happyReduction_1 (HappyAbsSyn5  happy_var_1)
	 =  HappyAbsSyn4
		 ([happy_var_1]
	)
happyReduction_1 _  = notHappyAtAll 

happyReduce_2 = happySpecReduce_2  4 happyReduction_2
happyReduction_2 (HappyAbsSyn5  happy_var_2)
	(HappyAbsSyn4  happy_var_1)
	 =  HappyAbsSyn4
		 (happy_var_1 ++ [happy_var_2]
	)
happyReduction_2 _ _  = notHappyAtAll 

happyReduce_3 = happySpecReduce_3  5 happyReduction_3
happyReduction_3 (HappyAbsSyn6  happy_var_3)
	(HappyTerminal (IDENTIFIER happy_var_2))
	_
	 =  HappyAbsSyn5
		 (ClassDecl(happy_var_2, (fst happy_var_3), (snd happy_var_3))
	)
happyReduction_3 _ _ _  = notHappyAtAll 

happyReduce_4 = happyReduce 4 5 happyReduction_4
happyReduction_4 ((HappyAbsSyn6  happy_var_4) `HappyStk`
	(HappyTerminal (IDENTIFIER happy_var_3)) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn5
		 (ClassDecl(happy_var_3, (fst happy_var_4), (snd happy_var_4))
	) `HappyStk` happyRest

happyReduce_5 = happySpecReduce_2  6 happyReduction_5
happyReduction_5 _
	_
	 =  HappyAbsSyn6
		 (([],[])
	)

happyReduce_6 = happySpecReduce_3  6 happyReduction_6
happyReduction_6 _
	(HappyAbsSyn8  happy_var_2)
	_
	 =  HappyAbsSyn6
		 (happy_var_2
	)
happyReduction_6 _ _ _  = notHappyAtAll 

happyReduce_7 = happySpecReduce_1  7 happyReduction_7
happyReduction_7 (HappyAbsSyn9  happy_var_1)
	 =  HappyAbsSyn7
		 ([happy_var_1]
	)
happyReduction_7 _  = notHappyAtAll 

happyReduce_8 = happySpecReduce_2  7 happyReduction_8
happyReduction_8 (HappyAbsSyn9  happy_var_2)
	(HappyAbsSyn7  happy_var_1)
	 =  HappyAbsSyn7
		 (happy_var_1 ++ [happy_var_2]
	)
happyReduction_8 _ _  = notHappyAtAll 

happyReduce_9 = happySpecReduce_1  8 happyReduction_9
happyReduction_9 (HappyAbsSyn10  happy_var_1)
	 =  HappyAbsSyn8
		 (happy_var_1
	)
happyReduction_9 _  = notHappyAtAll 

happyReduce_10 = happySpecReduce_2  8 happyReduction_10
happyReduction_10 (HappyAbsSyn10  happy_var_2)
	(HappyAbsSyn8  happy_var_1)
	 =  HappyAbsSyn8
		 (joinFieldMethodTuples happy_var_1 happy_var_2
	)
happyReduction_10 _ _  = notHappyAtAll 

happyReduce_11 = happySpecReduce_1  9 happyReduction_11
happyReduction_11 (HappyTerminal happy_var_1)
	 =  HappyAbsSyn9
		 (happy_var_1
	)
happyReduction_11 _  = notHappyAtAll 

happyReduce_12 = happySpecReduce_1  9 happyReduction_12
happyReduction_12 (HappyTerminal happy_var_1)
	 =  HappyAbsSyn9
		 (happy_var_1
	)
happyReduction_12 _  = notHappyAtAll 

happyReduce_13 = happySpecReduce_1  9 happyReduction_13
happyReduction_13 (HappyTerminal happy_var_1)
	 =  HappyAbsSyn9
		 (happy_var_1
	)
happyReduction_13 _  = notHappyAtAll 

happyReduce_14 = happySpecReduce_1  9 happyReduction_14
happyReduction_14 (HappyTerminal happy_var_1)
	 =  HappyAbsSyn9
		 (happy_var_1
	)
happyReduction_14 _  = notHappyAtAll 

happyReduce_15 = happySpecReduce_1  9 happyReduction_15
happyReduction_15 (HappyTerminal happy_var_1)
	 =  HappyAbsSyn9
		 (happy_var_1
	)
happyReduction_15 _  = notHappyAtAll 

happyReduce_16 = happySpecReduce_1  10 happyReduction_16
happyReduction_16 (HappyAbsSyn11  happy_var_1)
	 =  HappyAbsSyn10
		 (happy_var_1
	)
happyReduction_16 _  = notHappyAtAll 

happyReduce_17 = happySpecReduce_1  10 happyReduction_17
happyReduction_17 (HappyAbsSyn12  happy_var_1)
	 =  HappyAbsSyn10
		 (([],[happy_var_1])
	)
happyReduction_17 _  = notHappyAtAll 

happyReduce_18 = happySpecReduce_1  11 happyReduction_18
happyReduction_18 (HappyAbsSyn13  happy_var_1)
	 =  HappyAbsSyn11
		 (([happy_var_1],[])
	)
happyReduction_18 _  = notHappyAtAll 

happyReduce_19 = happySpecReduce_1  11 happyReduction_19
happyReduction_19 (HappyAbsSyn14  happy_var_1)
	 =  HappyAbsSyn11
		 (([],[happy_var_1])
	)
happyReduction_19 _  = notHappyAtAll 

happyReduce_20 = happySpecReduce_2  12 happyReduction_20
happyReduction_20 (HappyAbsSyn16  happy_var_2)
	(HappyAbsSyn15  happy_var_1)
	 =  HappyAbsSyn12
		 (MethodDecl("void","<init>",happy_var_1,happy_var_2)
	)
happyReduction_20 _ _  = notHappyAtAll 

happyReduce_21 = happySpecReduce_3  12 happyReduction_21
happyReduction_21 (HappyAbsSyn16  happy_var_3)
	(HappyAbsSyn15  happy_var_2)
	_
	 =  HappyAbsSyn12
		 (MethodDecl("void","<init>",happy_var_2,happy_var_3)
	)
happyReduction_21 _ _ _  = notHappyAtAll 

happyReduce_22 = happySpecReduce_3  13 happyReduction_22
happyReduction_22 _
	(HappyTerminal (IDENTIFIER happy_var_2))
	(HappyAbsSyn17  happy_var_1)
	 =  HappyAbsSyn13
		 (FieldDecl(happy_var_1,happy_var_2)
	)
happyReduction_22 _ _ _  = notHappyAtAll 

happyReduce_23 = happyReduce 4 13 happyReduction_23
happyReduction_23 (_ `HappyStk`
	(HappyTerminal (IDENTIFIER happy_var_3)) `HappyStk`
	(HappyAbsSyn17  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn13
		 (FieldDecl(happy_var_2,happy_var_3)
	) `HappyStk` happyRest

happyReduce_24 = happySpecReduce_2  14 happyReduction_24
happyReduction_24 (HappyAbsSyn26  happy_var_2)
	(HappyAbsSyn18  happy_var_1)
	 =  HappyAbsSyn14
		 (MethodDecl(fst happy_var_1, fst (snd happy_var_1), snd (snd happy_var_1), happy_var_2)
	)
happyReduction_24 _ _  = notHappyAtAll 

happyReduce_25 = happySpecReduce_3  15 happyReduction_25
happyReduction_25 _
	_
	_
	 =  HappyAbsSyn15
		 ([]
	)

happyReduce_26 = happyReduce 4 15 happyReduction_26
happyReduction_26 (_ `HappyStk`
	(HappyAbsSyn19  happy_var_3) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn15
		 (happy_var_3
	) `HappyStk` happyRest

happyReduce_27 = happySpecReduce_2  16 happyReduction_27
happyReduction_27 _
	_
	 =  HappyAbsSyn16
		 (Block([])
	)

happyReduce_28 = happySpecReduce_3  16 happyReduction_28
happyReduction_28 _
	(HappyAbsSyn20  happy_var_2)
	_
	 =  HappyAbsSyn16
		 (Block(happy_var_2)
	)
happyReduction_28 _ _ _  = notHappyAtAll 

happyReduce_29 = happySpecReduce_1  17 happyReduction_29
happyReduction_29 (HappyAbsSyn21  happy_var_1)
	 =  HappyAbsSyn17
		 (happy_var_1
	)
happyReduction_29 _  = notHappyAtAll 

happyReduce_30 = happySpecReduce_1  17 happyReduction_30
happyReduction_30 (HappyAbsSyn22  happy_var_1)
	 =  HappyAbsSyn17
		 (happy_var_1
	)
happyReduction_30 _  = notHappyAtAll 

happyReduce_31 = happySpecReduce_2  18 happyReduction_31
happyReduction_31 (HappyAbsSyn23  happy_var_2)
	(HappyAbsSyn17  happy_var_1)
	 =  HappyAbsSyn18
		 ((happy_var_1,happy_var_2)
	)
happyReduction_31 _ _  = notHappyAtAll 

happyReduce_32 = happySpecReduce_3  18 happyReduction_32
happyReduction_32 (HappyAbsSyn23  happy_var_3)
	(HappyAbsSyn17  happy_var_2)
	_
	 =  HappyAbsSyn18
		 ((happy_var_2,happy_var_3)
	)
happyReduction_32 _ _ _  = notHappyAtAll 

happyReduce_33 = happySpecReduce_2  18 happyReduction_33
happyReduction_33 (HappyAbsSyn23  happy_var_2)
	_
	 =  HappyAbsSyn18
		 (("void",happy_var_2)
	)
happyReduction_33 _ _  = notHappyAtAll 

happyReduce_34 = happySpecReduce_3  18 happyReduction_34
happyReduction_34 (HappyAbsSyn23  happy_var_3)
	_
	_
	 =  HappyAbsSyn18
		 (("void",happy_var_3)
	)
happyReduction_34 _ _ _  = notHappyAtAll 

happyReduce_35 = happySpecReduce_1  19 happyReduction_35
happyReduction_35 (HappyAbsSyn28  happy_var_1)
	 =  HappyAbsSyn19
		 ([happy_var_1]
	)
happyReduction_35 _  = notHappyAtAll 

happyReduce_36 = happySpecReduce_3  19 happyReduction_36
happyReduction_36 (HappyAbsSyn28  happy_var_3)
	_
	(HappyAbsSyn19  happy_var_1)
	 =  HappyAbsSyn19
		 (happy_var_1 ++ [happy_var_3]
	)
happyReduction_36 _ _ _  = notHappyAtAll 

happyReduce_37 = happySpecReduce_1  20 happyReduction_37
happyReduction_37 (HappyAbsSyn27  happy_var_1)
	 =  HappyAbsSyn20
		 (happy_var_1
	)
happyReduction_37 _  = notHappyAtAll 

happyReduce_38 = happySpecReduce_2  20 happyReduction_38
happyReduction_38 (HappyAbsSyn27  happy_var_2)
	(HappyAbsSyn20  happy_var_1)
	 =  HappyAbsSyn20
		 (happy_var_1 ++ happy_var_2
	)
happyReduction_38 _ _  = notHappyAtAll 

happyReduce_39 = happySpecReduce_1  21 happyReduction_39
happyReduction_39 _
	 =  HappyAbsSyn21
		 ("bool"
	)

happyReduce_40 = happySpecReduce_1  21 happyReduction_40
happyReduction_40 _
	 =  HappyAbsSyn21
		 ("int"
	)

happyReduce_41 = happySpecReduce_1  21 happyReduction_41
happyReduction_41 _
	 =  HappyAbsSyn21
		 ("char"
	)

happyReduce_42 = happySpecReduce_1  22 happyReduction_42
happyReduction_42 (HappyAbsSyn25  happy_var_1)
	 =  HappyAbsSyn22
		 (happy_var_1
	)
happyReduction_42 _  = notHappyAtAll 

happyReduce_43 = happySpecReduce_3  23 happyReduction_43
happyReduction_43 _
	_
	(HappyTerminal (IDENTIFIER happy_var_1))
	 =  HappyAbsSyn23
		 ((happy_var_1,[])
	)
happyReduction_43 _ _ _  = notHappyAtAll 

happyReduce_44 = happyReduce 4 23 happyReduction_44
happyReduction_44 (_ `HappyStk`
	(HappyAbsSyn19  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (IDENTIFIER happy_var_1)) `HappyStk`
	happyRest)
	 = HappyAbsSyn23
		 ((happy_var_1,happy_var_3)
	) `HappyStk` happyRest

happyReduce_45 = happySpecReduce_1  24 happyReduction_45
happyReduction_45 (HappyTerminal (IDENTIFIER happy_var_1))
	 =  HappyAbsSyn24
		 (happy_var_1
	)
happyReduction_45 _  = notHappyAtAll 

happyReduce_46 = happySpecReduce_3  24 happyReduction_46
happyReduction_46 (HappyTerminal (IDENTIFIER happy_var_3))
	_
	(HappyAbsSyn24  happy_var_1)
	 =  HappyAbsSyn24
		 (happy_var_1 ++ happy_var_3
	)
happyReduction_46 _ _ _  = notHappyAtAll 

happyReduce_47 = happySpecReduce_1  25 happyReduction_47
happyReduction_47 (HappyAbsSyn24  happy_var_1)
	 =  HappyAbsSyn25
		 (happy_var_1
	)
happyReduction_47 _  = notHappyAtAll 

happyReduce_48 = happySpecReduce_2  26 happyReduction_48
happyReduction_48 _
	_
	 =  HappyAbsSyn26
		 (Block([])
	)

happyReduce_49 = happySpecReduce_3  26 happyReduction_49
happyReduction_49 _
	(HappyAbsSyn20  happy_var_2)
	_
	 =  HappyAbsSyn26
		 (Block(happy_var_2)
	)
happyReduction_49 _ _ _  = notHappyAtAll 

happyReduce_50 = happySpecReduce_3  27 happyReduction_50
happyReduction_50 _
	(HappyTerminal (IDENTIFIER happy_var_2))
	(HappyAbsSyn17  happy_var_1)
	 =  HappyAbsSyn27
		 ([LocalVarDecl(happy_var_1,happy_var_2)]
	)
happyReduction_50 _ _ _  = notHappyAtAll 

happyReduce_51 = happySpecReduce_3  27 happyReduction_51
happyReduction_51 _
	(HappyAbsSyn40  happy_var_2)
	(HappyAbsSyn17  happy_var_1)
	 =  HappyAbsSyn27
		 ([LocalVarDecl(happy_var_1,fst(happy_var_2)), StmtExprStmt(Assign(fst(happy_var_2),snd(happy_var_2)))]
	)
happyReduction_51 _ _ _  = notHappyAtAll 

happyReduce_52 = happySpecReduce_1  27 happyReduction_52
happyReduction_52 (HappyAbsSyn29  happy_var_1)
	 =  HappyAbsSyn27
		 ([happy_var_1]
	)
happyReduction_52 _  = notHappyAtAll 

happyReduce_53 = happySpecReduce_2  28 happyReduction_53
happyReduction_53 (HappyTerminal (IDENTIFIER happy_var_2))
	(HappyAbsSyn17  happy_var_1)
	 =  HappyAbsSyn28
		 ((happy_var_1,happy_var_2)
	)
happyReduction_53 _ _  = notHappyAtAll 

happyReduce_54 = happySpecReduce_1  29 happyReduction_54
happyReduction_54 (HappyAbsSyn31  happy_var_1)
	 =  HappyAbsSyn29
		 (happy_var_1
	)
happyReduction_54 _  = notHappyAtAll 

happyReduce_55 = happySpecReduce_1  29 happyReduction_55
happyReduction_55 (HappyAbsSyn32  happy_var_1)
	 =  HappyAbsSyn29
		 (happy_var_1
	)
happyReduction_55 _  = notHappyAtAll 

happyReduce_56 = happySpecReduce_1  29 happyReduction_56
happyReduction_56 (HappyAbsSyn33  happy_var_1)
	 =  HappyAbsSyn29
		 (happy_var_1
	)
happyReduction_56 _  = notHappyAtAll 

happyReduce_57 = happySpecReduce_1  29 happyReduction_57
happyReduction_57 (HappyAbsSyn34  happy_var_1)
	 =  HappyAbsSyn29
		 (happy_var_1
	)
happyReduction_57 _  = notHappyAtAll 

happyReduce_58 = happySpecReduce_1  30 happyReduction_58
happyReduction_58 (HappyAbsSyn35  happy_var_1)
	 =  HappyAbsSyn30
		 (happy_var_1
	)
happyReduction_58 _  = notHappyAtAll 

happyReduce_59 = happySpecReduce_1  31 happyReduction_59
happyReduction_59 (HappyAbsSyn26  happy_var_1)
	 =  HappyAbsSyn31
		 (happy_var_1
	)
happyReduction_59 _  = notHappyAtAll 

happyReduce_60 = happySpecReduce_1  31 happyReduction_60
happyReduction_60 _
	 =  HappyAbsSyn31
		 (EmptyStmt
	)

happyReduce_61 = happySpecReduce_2  31 happyReduction_61
happyReduction_61 _
	(HappyAbsSyn41  happy_var_1)
	 =  HappyAbsSyn31
		 (StmtExprStmt(happy_var_1)
	)
happyReduction_61 _ _  = notHappyAtAll 

happyReduce_62 = happySpecReduce_1  31 happyReduction_62
happyReduction_62 (HappyAbsSyn36  happy_var_1)
	 =  HappyAbsSyn31
		 (Return(happy_var_1)
	)
happyReduction_62 _  = notHappyAtAll 

happyReduce_63 = happyReduce 5 32 happyReduction_63
happyReduction_63 ((HappyAbsSyn29  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn30  happy_var_3) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn32
		 (If(happy_var_3, happy_var_5, Nothing)
	) `HappyStk` happyRest

happyReduce_64 = happyReduce 7 33 happyReduction_64
happyReduction_64 ((HappyAbsSyn29  happy_var_7) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn37  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn30  happy_var_3) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn33
		 (If(happy_var_3, happy_var_5, Just happy_var_7)
	) `HappyStk` happyRest

happyReduce_65 = happyReduce 5 34 happyReduction_65
happyReduction_65 ((HappyAbsSyn29  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn30  happy_var_3) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn34
		 (While(happy_var_3, happy_var_5)
	) `HappyStk` happyRest

happyReduce_66 = happySpecReduce_1  35 happyReduction_66
happyReduction_66 (HappyAbsSyn39  happy_var_1)
	 =  HappyAbsSyn35
		 (StmtExprExpr(happy_var_1)
	)
happyReduction_66 _  = notHappyAtAll 

happyReduce_67 = happySpecReduce_1  35 happyReduction_67
happyReduction_67 (HappyAbsSyn38  happy_var_1)
	 =  HappyAbsSyn35
		 (happy_var_1
	)
happyReduction_67 _  = notHappyAtAll 

happyReduce_68 = happySpecReduce_2  36 happyReduction_68
happyReduction_68 _
	_
	 =  HappyAbsSyn36
		 (Nothing
	)

happyReduce_69 = happySpecReduce_3  36 happyReduction_69
happyReduction_69 _
	(HappyAbsSyn30  happy_var_2)
	_
	 =  HappyAbsSyn36
		 (Just happy_var_2
	)
happyReduction_69 _ _ _  = notHappyAtAll 

happyReduce_70 = happySpecReduce_1  37 happyReduction_70
happyReduction_70 (HappyAbsSyn31  happy_var_1)
	 =  HappyAbsSyn37
		 (happy_var_1
	)
happyReduction_70 _  = notHappyAtAll 

happyReduce_71 = happySpecReduce_1  37 happyReduction_71
happyReduction_71 (HappyAbsSyn42  happy_var_1)
	 =  HappyAbsSyn37
		 (happy_var_1
	)
happyReduction_71 _  = notHappyAtAll 

happyReduce_72 = happySpecReduce_1  37 happyReduction_72
happyReduction_72 (HappyAbsSyn43  happy_var_1)
	 =  HappyAbsSyn37
		 (happy_var_1
	)
happyReduction_72 _  = notHappyAtAll 

happyReduce_73 = happySpecReduce_1  38 happyReduction_73
happyReduction_73 (HappyAbsSyn44  happy_var_1)
	 =  HappyAbsSyn38
		 (happy_var_1
	)
happyReduction_73 _  = notHappyAtAll 

happyReduce_74 = happySpecReduce_1  39 happyReduction_74
happyReduction_74 (HappyAbsSyn40  happy_var_1)
	 =  HappyAbsSyn39
		 (Assign(fst(happy_var_1),snd(happy_var_1))
	)
happyReduction_74 _  = notHappyAtAll 

happyReduce_75 = happySpecReduce_3  40 happyReduction_75
happyReduction_75 (HappyAbsSyn35  happy_var_3)
	_
	(HappyAbsSyn45  happy_var_1)
	 =  HappyAbsSyn40
		 ((happy_var_1, happy_var_3)
	)
happyReduction_75 _ _ _  = notHappyAtAll 

happyReduce_76 = happySpecReduce_1  41 happyReduction_76
happyReduction_76 (HappyAbsSyn39  happy_var_1)
	 =  HappyAbsSyn41
		 (happy_var_1
	)
happyReduction_76 _  = notHappyAtAll 

happyReduce_77 = happySpecReduce_1  41 happyReduction_77
happyReduction_77 (HappyAbsSyn47  happy_var_1)
	 =  HappyAbsSyn41
		 (happy_var_1
	)
happyReduction_77 _  = notHappyAtAll 

happyReduce_78 = happySpecReduce_1  41 happyReduction_78
happyReduction_78 (HappyAbsSyn48  happy_var_1)
	 =  HappyAbsSyn41
		 (happy_var_1
	)
happyReduction_78 _  = notHappyAtAll 

happyReduce_79 = happyReduce 7 42 happyReduction_79
happyReduction_79 ((HappyAbsSyn37  happy_var_7) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn37  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn30  happy_var_3) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn42
		 (If(happy_var_3, happy_var_5, Just happy_var_7)
	) `HappyStk` happyRest

happyReduce_80 = happyReduce 5 43 happyReduction_80
happyReduction_80 ((HappyAbsSyn37  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn30  happy_var_3) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn43
		 (While(happy_var_3, happy_var_5)
	) `HappyStk` happyRest

happyReduce_81 = happySpecReduce_1  44 happyReduction_81
happyReduction_81 (HappyAbsSyn49  happy_var_1)
	 =  HappyAbsSyn44
		 (happy_var_1
	)
happyReduction_81 _  = notHappyAtAll 

happyReduce_82 = happySpecReduce_1  45 happyReduction_82
happyReduction_82 (HappyAbsSyn24  happy_var_1)
	 =  HappyAbsSyn45
		 (happy_var_1
	)
happyReduction_82 _  = notHappyAtAll 

happyReduce_83 = happySpecReduce_1  46 happyReduction_83
happyReduction_83 _
	 =  HappyAbsSyn46
		 (
	)

happyReduce_84 = happySpecReduce_1  46 happyReduction_84
happyReduction_84 _
	 =  HappyAbsSyn46
		 (
	)

happyReduce_85 = happySpecReduce_1  46 happyReduction_85
happyReduction_85 _
	 =  HappyAbsSyn46
		 (
	)

happyReduce_86 = happySpecReduce_1  46 happyReduction_86
happyReduction_86 _
	 =  HappyAbsSyn46
		 (
	)

happyReduce_87 = happySpecReduce_1  46 happyReduction_87
happyReduction_87 _
	 =  HappyAbsSyn46
		 (
	)

happyReduce_88 = happySpecReduce_1  46 happyReduction_88
happyReduction_88 _
	 =  HappyAbsSyn46
		 (
	)

happyReduce_89 = happySpecReduce_1  46 happyReduction_89
happyReduction_89 _
	 =  HappyAbsSyn46
		 (
	)

happyReduce_90 = happySpecReduce_1  46 happyReduction_90
happyReduction_90 _
	 =  HappyAbsSyn46
		 (
	)

happyReduce_91 = happySpecReduce_1  46 happyReduction_91
happyReduction_91 _
	 =  HappyAbsSyn46
		 (
	)

happyReduce_92 = happySpecReduce_1  46 happyReduction_92
happyReduction_92 _
	 =  HappyAbsSyn46
		 (
	)

happyReduce_93 = happySpecReduce_1  46 happyReduction_93
happyReduction_93 _
	 =  HappyAbsSyn46
		 (
	)

happyReduce_94 = happySpecReduce_1  46 happyReduction_94
happyReduction_94 _
	 =  HappyAbsSyn46
		 (
	)

happyReduce_95 = happySpecReduce_3  47 happyReduction_95
happyReduction_95 _
	_
	(HappyAbsSyn24  happy_var_1)
	 =  HappyAbsSyn47
		 (MethodCall(This, happy_var_1, [])
	)
happyReduction_95 _ _ _  = notHappyAtAll 

happyReduce_96 = happyReduce 4 47 happyReduction_96
happyReduction_96 (_ `HappyStk`
	(HappyAbsSyn62  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest)
	 = HappyAbsSyn47
		 (MethodCall(This, happy_var_1, happy_var_3)
	) `HappyStk` happyRest

happyReduce_97 = happyReduce 5 47 happyReduction_97
happyReduction_97 (_ `HappyStk`
	_ `HappyStk`
	(HappyTerminal (IDENTIFIER happy_var_3)) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn50  happy_var_1) `HappyStk`
	happyRest)
	 = HappyAbsSyn47
		 (MethodCall(happy_var_1, happy_var_3, [])
	) `HappyStk` happyRest

happyReduce_98 = happyReduce 6 47 happyReduction_98
happyReduction_98 (_ `HappyStk`
	(HappyAbsSyn62  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (IDENTIFIER happy_var_3)) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn50  happy_var_1) `HappyStk`
	happyRest)
	 = HappyAbsSyn47
		 (MethodCall(happy_var_1, happy_var_3, happy_var_5)
	) `HappyStk` happyRest

happyReduce_99 = happyReduce 4 48 happyReduction_99
happyReduction_99 (_ `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn25  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn48
		 (New(happy_var_2, [])
	) `HappyStk` happyRest

happyReduce_100 = happyReduce 5 48 happyReduction_100
happyReduction_100 (_ `HappyStk`
	(HappyAbsSyn62  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn25  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn48
		 (New(happy_var_2, happy_var_4)
	) `HappyStk` happyRest

happyReduce_101 = happySpecReduce_1  49 happyReduction_101
happyReduction_101 (HappyAbsSyn51  happy_var_1)
	 =  HappyAbsSyn49
		 (happy_var_1
	)
happyReduction_101 _  = notHappyAtAll 

happyReduce_102 = happySpecReduce_1  50 happyReduction_102
happyReduction_102 (HappyAbsSyn52  happy_var_1)
	 =  HappyAbsSyn50
		 (happy_var_1
	)
happyReduction_102 _  = notHappyAtAll 

happyReduce_103 = happySpecReduce_1  51 happyReduction_103
happyReduction_103 (HappyAbsSyn54  happy_var_1)
	 =  HappyAbsSyn51
		 (happy_var_1
	)
happyReduction_103 _  = notHappyAtAll 

happyReduce_104 = happySpecReduce_3  51 happyReduction_104
happyReduction_104 (HappyAbsSyn54  happy_var_3)
	_
	(HappyAbsSyn51  happy_var_1)
	 =  HappyAbsSyn51
		 (Binary("||", happy_var_1, happy_var_3)
	)
happyReduction_104 _ _ _  = notHappyAtAll 

happyReduce_105 = happySpecReduce_1  52 happyReduction_105
happyReduction_105 (HappyAbsSyn55  happy_var_1)
	 =  HappyAbsSyn52
		 (happy_var_1
	)
happyReduction_105 _  = notHappyAtAll 

happyReduce_106 = happySpecReduce_1  52 happyReduction_106
happyReduction_106 _
	 =  HappyAbsSyn52
		 (This
	)

happyReduce_107 = happySpecReduce_3  52 happyReduction_107
happyReduction_107 _
	(HappyAbsSyn30  happy_var_2)
	_
	 =  HappyAbsSyn52
		 (happy_var_2
	)
happyReduction_107 _ _ _  = notHappyAtAll 

happyReduce_108 = happySpecReduce_1  52 happyReduction_108
happyReduction_108 (HappyAbsSyn48  happy_var_1)
	 =  HappyAbsSyn52
		 (StmtExprExpr(happy_var_1)
	)
happyReduction_108 _  = notHappyAtAll 

happyReduce_109 = happySpecReduce_1  52 happyReduction_109
happyReduction_109 (HappyAbsSyn53  happy_var_1)
	 =  HappyAbsSyn52
		 (happy_var_1
	)
happyReduction_109 _  = notHappyAtAll 

happyReduce_110 = happySpecReduce_1  52 happyReduction_110
happyReduction_110 (HappyAbsSyn47  happy_var_1)
	 =  HappyAbsSyn52
		 (StmtExprExpr(happy_var_1)
	)
happyReduction_110 _  = notHappyAtAll 

happyReduce_111 = happySpecReduce_3  53 happyReduction_111
happyReduction_111 (HappyTerminal (IDENTIFIER happy_var_3))
	_
	(HappyAbsSyn50  happy_var_1)
	 =  HappyAbsSyn53
		 (InstVar(happy_var_1,happy_var_3)
	)
happyReduction_111 _ _ _  = notHappyAtAll 

happyReduce_112 = happySpecReduce_1  54 happyReduction_112
happyReduction_112 (HappyAbsSyn56  happy_var_1)
	 =  HappyAbsSyn54
		 (happy_var_1
	)
happyReduction_112 _  = notHappyAtAll 

happyReduce_113 = happySpecReduce_1  55 happyReduction_113
happyReduction_113 (HappyTerminal (INTLITERAL happy_var_1))
	 =  HappyAbsSyn55
		 (Integer(happy_var_1)
	)
happyReduction_113 _  = notHappyAtAll 

happyReduce_114 = happySpecReduce_1  55 happyReduction_114
happyReduction_114 (HappyTerminal (BOOLLITERAL happy_var_1))
	 =  HappyAbsSyn55
		 (Bool(happy_var_1)
	)
happyReduction_114 _  = notHappyAtAll 

happyReduce_115 = happySpecReduce_1  55 happyReduction_115
happyReduction_115 (HappyTerminal (CHARLITERAL happy_var_1))
	 =  HappyAbsSyn55
		 (Char(happy_var_1)
	)
happyReduction_115 _  = notHappyAtAll 

happyReduce_116 = happySpecReduce_1  55 happyReduction_116
happyReduction_116 _
	 =  HappyAbsSyn55
		 (Jnull
	)

happyReduce_117 = happySpecReduce_1  56 happyReduction_117
happyReduction_117 (HappyAbsSyn57  happy_var_1)
	 =  HappyAbsSyn56
		 (happy_var_1
	)
happyReduction_117 _  = notHappyAtAll 

happyReduce_118 = happySpecReduce_3  56 happyReduction_118
happyReduction_118 (HappyAbsSyn57  happy_var_3)
	_
	(HappyAbsSyn56  happy_var_1)
	 =  HappyAbsSyn56
		 (Binary("&&",happy_var_1,happy_var_3)
	)
happyReduction_118 _ _ _  = notHappyAtAll 

happyReduce_119 = happySpecReduce_1  57 happyReduction_119
happyReduction_119 (HappyAbsSyn58  happy_var_1)
	 =  HappyAbsSyn57
		 (happy_var_1
	)
happyReduction_119 _  = notHappyAtAll 

happyReduce_120 = happySpecReduce_3  57 happyReduction_120
happyReduction_120 (HappyAbsSyn58  happy_var_3)
	_
	(HappyAbsSyn57  happy_var_1)
	 =  HappyAbsSyn57
		 (Binary("==",happy_var_1,happy_var_3)
	)
happyReduction_120 _ _ _  = notHappyAtAll 

happyReduce_121 = happySpecReduce_3  57 happyReduction_121
happyReduction_121 (HappyAbsSyn58  happy_var_3)
	_
	(HappyAbsSyn57  happy_var_1)
	 =  HappyAbsSyn57
		 (Binary("!=",happy_var_1,happy_var_3)
	)
happyReduction_121 _ _ _  = notHappyAtAll 

happyReduce_122 = happySpecReduce_1  58 happyReduction_122
happyReduction_122 (HappyAbsSyn59  happy_var_1)
	 =  HappyAbsSyn58
		 (happy_var_1
	)
happyReduction_122 _  = notHappyAtAll 

happyReduce_123 = happySpecReduce_3  58 happyReduction_123
happyReduction_123 (HappyAbsSyn59  happy_var_3)
	_
	(HappyAbsSyn58  happy_var_1)
	 =  HappyAbsSyn58
		 (Binary("<",happy_var_1,happy_var_3)
	)
happyReduction_123 _ _ _  = notHappyAtAll 

happyReduce_124 = happySpecReduce_3  58 happyReduction_124
happyReduction_124 (HappyAbsSyn59  happy_var_3)
	_
	(HappyAbsSyn58  happy_var_1)
	 =  HappyAbsSyn58
		 (Binary(">",happy_var_1,happy_var_3)
	)
happyReduction_124 _ _ _  = notHappyAtAll 

happyReduce_125 = happySpecReduce_1  59 happyReduction_125
happyReduction_125 (HappyAbsSyn60  happy_var_1)
	 =  HappyAbsSyn59
		 (happy_var_1
	)
happyReduction_125 _  = notHappyAtAll 

happyReduce_126 = happySpecReduce_1  60 happyReduction_126
happyReduction_126 (HappyAbsSyn61  happy_var_1)
	 =  HappyAbsSyn60
		 (happy_var_1
	)
happyReduction_126 _  = notHappyAtAll 

happyReduce_127 = happySpecReduce_3  60 happyReduction_127
happyReduction_127 (HappyAbsSyn61  happy_var_3)
	_
	(HappyAbsSyn60  happy_var_1)
	 =  HappyAbsSyn60
		 (Binary("+",happy_var_1,happy_var_3)
	)
happyReduction_127 _ _ _  = notHappyAtAll 

happyReduce_128 = happySpecReduce_3  60 happyReduction_128
happyReduction_128 (HappyAbsSyn61  happy_var_3)
	_
	(HappyAbsSyn60  happy_var_1)
	 =  HappyAbsSyn60
		 (Binary("-",happy_var_1,happy_var_3)
	)
happyReduction_128 _ _ _  = notHappyAtAll 

happyReduce_129 = happySpecReduce_1  61 happyReduction_129
happyReduction_129 (HappyAbsSyn66  happy_var_1)
	 =  HappyAbsSyn61
		 (happy_var_1
	)
happyReduction_129 _  = notHappyAtAll 

happyReduce_130 = happySpecReduce_3  61 happyReduction_130
happyReduction_130 (HappyAbsSyn66  happy_var_3)
	_
	(HappyAbsSyn61  happy_var_1)
	 =  HappyAbsSyn61
		 (Binary("*",happy_var_1,happy_var_3)
	)
happyReduction_130 _ _ _  = notHappyAtAll 

happyReduce_131 = happySpecReduce_3  61 happyReduction_131
happyReduction_131 (HappyAbsSyn66  happy_var_3)
	_
	(HappyAbsSyn61  happy_var_1)
	 =  HappyAbsSyn61
		 (Binary("/",happy_var_1,happy_var_3)
	)
happyReduction_131 _ _ _  = notHappyAtAll 

happyReduce_132 = happySpecReduce_3  61 happyReduction_132
happyReduction_132 (HappyAbsSyn66  happy_var_3)
	_
	(HappyAbsSyn61  happy_var_1)
	 =  HappyAbsSyn61
		 (Binary("%",happy_var_1,happy_var_3)
	)
happyReduction_132 _ _ _  = notHappyAtAll 

happyReduce_133 = happySpecReduce_1  62 happyReduction_133
happyReduction_133 (HappyAbsSyn30  happy_var_1)
	 =  HappyAbsSyn62
		 ([happy_var_1]
	)
happyReduction_133 _  = notHappyAtAll 

happyReduce_134 = happySpecReduce_3  62 happyReduction_134
happyReduction_134 (HappyAbsSyn30  happy_var_3)
	_
	(HappyAbsSyn62  happy_var_1)
	 =  HappyAbsSyn62
		 (happy_var_1 ++ [happy_var_3]
	)
happyReduction_134 _ _ _  = notHappyAtAll 

happyReduce_135 = happySpecReduce_1  63 happyReduction_135
happyReduction_135 (HappyAbsSyn50  happy_var_1)
	 =  HappyAbsSyn63
		 (happy_var_1
	)
happyReduction_135 _  = notHappyAtAll 

happyReduce_136 = happySpecReduce_1  63 happyReduction_136
happyReduction_136 (HappyAbsSyn24  happy_var_1)
	 =  HappyAbsSyn63
		 (LocalOrFieldVar(happy_var_1)
	)
happyReduction_136 _  = notHappyAtAll 

happyReduce_137 = happySpecReduce_1  64 happyReduction_137
happyReduction_137 (HappyAbsSyn63  happy_var_1)
	 =  HappyAbsSyn64
		 (happy_var_1
	)
happyReduction_137 _  = notHappyAtAll 

happyReduce_138 = happySpecReduce_2  64 happyReduction_138
happyReduction_138 (HappyAbsSyn66  happy_var_2)
	_
	 =  HappyAbsSyn64
		 (Unary("~",happy_var_2)
	)
happyReduction_138 _ _  = notHappyAtAll 

happyReduce_139 = happySpecReduce_2  64 happyReduction_139
happyReduction_139 (HappyAbsSyn66  happy_var_2)
	_
	 =  HappyAbsSyn64
		 (Unary("!",happy_var_2)
	)
happyReduction_139 _ _  = notHappyAtAll 

happyReduce_140 = happySpecReduce_1  64 happyReduction_140
happyReduction_140 (HappyAbsSyn65  happy_var_1)
	 =  HappyAbsSyn64
		 (happy_var_1
	)
happyReduction_140 _  = notHappyAtAll 

happyReduce_141 = happyReduce 4 65 happyReduction_141
happyReduction_141 ((HappyAbsSyn66  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn21  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn65
		 (TypedExpr(happy_var_4,happy_var_2)
	) `HappyStk` happyRest

happyReduce_142 = happySpecReduce_1  66 happyReduction_142
happyReduction_142 (HappyAbsSyn64  happy_var_1)
	 =  HappyAbsSyn66
		 (happy_var_1
	)
happyReduction_142 _  = notHappyAtAll 

happyReduce_143 = happySpecReduce_2  66 happyReduction_143
happyReduction_143 (HappyAbsSyn66  happy_var_2)
	_
	 =  HappyAbsSyn66
		 (Unary("++",happy_var_2)
	)
happyReduction_143 _ _  = notHappyAtAll 

happyReduce_144 = happySpecReduce_2  66 happyReduction_144
happyReduction_144 (HappyAbsSyn66  happy_var_2)
	_
	 =  HappyAbsSyn66
		 (Unary("--",happy_var_2)
	)
happyReduction_144 _ _  = notHappyAtAll 

happyNewToken action sts stk [] =
	action 143 143 notHappyAtAll (HappyState action) sts stk []

happyNewToken action sts stk (tk:tks) =
	let cont i = action i i tk (HappyState action) sts stk tks in
	case tk of {
	BOOLEAN -> cont 67;
	BREAK -> cont 68;
	CASE -> cont 69;
	CHAR -> cont 70;
	CLASS -> cont 71;
	IDENTIFIER happy_dollar_dollar -> cont 72;
	INTLITERAL happy_dollar_dollar -> cont 73;
	CONTINUE -> cont 74;
	DEFAULT -> cont 75;
	DO -> cont 76;
	ELSE -> cont 77;
	FOR -> cont 78;
	IF -> cont 79;
	INT -> cont 80;
	NEW -> cont 81;
	PRIVATE -> cont 82;
	PROTECTED -> cont 83;
	PUBLIC -> cont 84;
	RETURN -> cont 85;
	SWITCH -> cont 86;
	THIS -> cont 87;
	VOID -> cont 88;
	WHILE -> cont 89;
	BOOLLITERAL happy_dollar_dollar -> cont 90;
	JNULL -> cont 91;
	CHARLITERAL happy_dollar_dollar -> cont 92;
	EQUAL -> cont 93;
	LESSEQUAL -> cont 94;
	GREATEREQUAL -> cont 95;
	NOTEQUAL -> cont 96;
	LOGICALOR -> cont 97;
	LOGICALAND -> cont 98;
	INCREMENT -> cont 99;
	DECREMENT -> cont 100;
	SHIFTLEFT -> cont 101;
	SHIFTRIGHT -> cont 102;
	UNSIGNEDSHIFTRIGHT -> cont 103;
	SIGNEDSHIFTRIGHT -> cont 104;
	PLUSEQUAL -> cont 105;
	MINUSEQUAL -> cont 106;
	TIMESEQUAL -> cont 107;
	DIVIDEEQUAL -> cont 108;
	ANDEQUAL -> cont 109;
	OREQUAL -> cont 110;
	XOREQUAL -> cont 111;
	MODULOEQUAL -> cont 112;
	SHIFTLEFTEQUAL -> cont 113;
	SIGNEDSHIFTRIGHTEQUAL -> cont 114;
	UNSIGNEDSHIFTRIGHTEQUAL -> cont 115;
	LPARENTHESIS -> cont 116;
	RPARENTHESIS -> cont 117;
	LCURLYBRACKET -> cont 118;
	RCURLYBRACKET -> cont 119;
	SEMICOLON -> cont 120;
	DOT -> cont 121;
	ASSIGN -> cont 122;
	LESS -> cont 123;
	GREATER -> cont 124;
	EXCLMARK -> cont 125;
	TILDE -> cont 126;
	QUESMARK -> cont 127;
	COMMA -> cont 128;
	PLUS -> cont 129;
	MINUS -> cont 130;
	MUL -> cont 131;
	DIV -> cont 132;
	MOD -> cont 133;
	AND -> cont 134;
	OR -> cont 135;
	XOR -> cont 136;
	SHARP -> cont 137;
	ARROW -> cont 138;
	INSTANCEOF -> cont 139;
	ABSTRACT -> cont 140;
	STATIC -> cont 141;
	COLON -> cont 142;
	_ -> happyError' (tk:tks)
	}

happyError_ 143 tk tks = happyError' tks
happyError_ _ tk tks = happyError' (tk:tks)

newtype HappyIdentity a = HappyIdentity a
happyIdentity = HappyIdentity
happyRunIdentity (HappyIdentity a) = a

instance Functor HappyIdentity where
    fmap f (HappyIdentity a) = HappyIdentity (f a)

instance Applicative HappyIdentity where
    pure    = return
    a <*> b = (fmap id a) <*> b
instance Monad HappyIdentity where
    return = HappyIdentity
    (HappyIdentity p) >>= q = q p

happyThen :: () => HappyIdentity a -> (a -> HappyIdentity b) -> HappyIdentity b
happyThen = (>>=)
happyReturn :: () => a -> HappyIdentity a
happyReturn = (return)
happyThen1 m k tks = (>>=) m (\a -> k a tks)
happyReturn1 :: () => a -> b -> HappyIdentity a
happyReturn1 = \a tks -> (return) a
happyError' :: () => [(Token)] -> HappyIdentity a
happyError' = HappyIdentity . parseError

parseJava tks = happyRunIdentity happySomeParser where
  happySomeParser = happyThen (happyParse action_0 tks) (\x -> case x of {HappyAbsSyn4 z -> happyReturn z; _other -> notHappyAtAll })

happySeq = happyDontSeq


parse :: String -> [ClassDecl]
parse s = parseJava (alexScanTokens s)
parseError :: [Token] -> a
parseError _ = error "Parse error"
{-# LINE 1 "templates/GenericTemplate.hs" #-}
{-# LINE 1 "templates/GenericTemplate.hs" #-}
{-# LINE 1 "<built-in>" #-}
{-# LINE 1 "<command-line>" #-}





# 1 "/usr/include/stdc-predef.h" 1 3 4

# 17 "/usr/include/stdc-predef.h" 3 4










































{-# LINE 5 "<command-line>" #-}
{-# LINE 1 "templates/GenericTemplate.hs" #-}
-- Id: GenericTemplate.hs,v 1.26 2005/01/14 14:47:22 simonmar Exp 

{-# LINE 13 "templates/GenericTemplate.hs" #-}

{-# LINE 46 "templates/GenericTemplate.hs" #-}








{-# LINE 67 "templates/GenericTemplate.hs" #-}

{-# LINE 77 "templates/GenericTemplate.hs" #-}

{-# LINE 86 "templates/GenericTemplate.hs" #-}

infixr 9 `HappyStk`
data HappyStk a = HappyStk a (HappyStk a)

-----------------------------------------------------------------------------
-- starting the parse

happyParse start_state = happyNewToken start_state notHappyAtAll notHappyAtAll

-----------------------------------------------------------------------------
-- Accepting the parse

-- If the current token is (1), it means we've just accepted a partial
-- parse (a %partial parser).  We must ignore the saved token on the top of
-- the stack in this case.
happyAccept (1) tk st sts (_ `HappyStk` ans `HappyStk` _) =
        happyReturn1 ans
happyAccept j tk st sts (HappyStk ans _) = 
         (happyReturn1 ans)

-----------------------------------------------------------------------------
-- Arrays only: do the next action

{-# LINE 155 "templates/GenericTemplate.hs" #-}

-----------------------------------------------------------------------------
-- HappyState data type (not arrays)



newtype HappyState b c = HappyState
        (Int ->                    -- token number
         Int ->                    -- token number (yes, again)
         b ->                           -- token semantic value
         HappyState b c ->              -- current state
         [HappyState b c] ->            -- state stack
         c)



-----------------------------------------------------------------------------
-- Shifting a token

happyShift new_state (1) tk st sts stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--     trace "shifting the error token" $
     new_state i i tk (HappyState (new_state)) ((st):(sts)) (stk)

happyShift new_state i tk st sts stk =
     happyNewToken new_state ((st):(sts)) ((HappyTerminal (tk))`HappyStk`stk)

-- happyReduce is specialised for the common cases.

happySpecReduce_0 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_0 nt fn j tk st@((HappyState (action))) sts stk
     = action nt j tk st ((st):(sts)) (fn `HappyStk` stk)

happySpecReduce_1 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_1 nt fn j tk _ sts@(((st@(HappyState (action))):(_))) (v1`HappyStk`stk')
     = let r = fn v1 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_2 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_2 nt fn j tk _ ((_):(sts@(((st@(HappyState (action))):(_))))) (v1`HappyStk`v2`HappyStk`stk')
     = let r = fn v1 v2 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_3 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_3 nt fn j tk _ ((_):(((_):(sts@(((st@(HappyState (action))):(_))))))) (v1`HappyStk`v2`HappyStk`v3`HappyStk`stk')
     = let r = fn v1 v2 v3 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happyReduce k i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyReduce k nt fn j tk st sts stk
     = case happyDrop (k - ((1) :: Int)) sts of
         sts1@(((st1@(HappyState (action))):(_))) ->
                let r = fn stk in  -- it doesn't hurt to always seq here...
                happyDoSeq r (action nt j tk st1 sts1 r)

happyMonadReduce k nt fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyMonadReduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
          let drop_stk = happyDropStk k stk in
          happyThen1 (fn stk tk) (\r -> action nt j tk st1 sts1 (r `HappyStk` drop_stk))

happyMonad2Reduce k nt fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyMonad2Reduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
         let drop_stk = happyDropStk k stk





             new_state = action

          in
          happyThen1 (fn stk tk) (\r -> happyNewToken new_state sts1 (r `HappyStk` drop_stk))

happyDrop (0) l = l
happyDrop n ((_):(t)) = happyDrop (n - ((1) :: Int)) t

happyDropStk (0) l = l
happyDropStk n (x `HappyStk` xs) = happyDropStk (n - ((1)::Int)) xs

-----------------------------------------------------------------------------
-- Moving to a new state after a reduction

{-# LINE 256 "templates/GenericTemplate.hs" #-}
happyGoto action j tk st = action j j tk (HappyState action)


-----------------------------------------------------------------------------
-- Error recovery ((1) is the error token)

-- parse error if we are in recovery and we fail again
happyFail (1) tk old_st _ stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--      trace "failing" $ 
        happyError_ i tk

{-  We don't need state discarding for our restricted implementation of
    "error".  In fact, it can cause some bogus parses, so I've disabled it
    for now --SDM

-- discard a state
happyFail  (1) tk old_st (((HappyState (action))):(sts)) 
                                                (saved_tok `HappyStk` _ `HappyStk` stk) =
--      trace ("discarding state, depth " ++ show (length stk))  $
        action (1) (1) tk (HappyState (action)) sts ((saved_tok`HappyStk`stk))
-}

-- Enter error recovery: generate an error token,
--                       save the old token and carry on.
happyFail  i tk (HappyState (action)) sts stk =
--      trace "entering error recovery" $
        action (1) (1) tk (HappyState (action)) sts ( (HappyErrorToken (i)) `HappyStk` stk)

-- Internal happy errors:

notHappyAtAll :: a
notHappyAtAll = error "Internal Happy error\n"

-----------------------------------------------------------------------------
-- Hack to get the typechecker to accept our action functions







-----------------------------------------------------------------------------
-- Seq-ing.  If the --strict flag is given, then Happy emits 
--      happySeq = happyDoSeq
-- otherwise it emits
--      happySeq = happyDontSeq

happyDoSeq, happyDontSeq :: a -> b -> b
happyDoSeq   a b = a `seq` b
happyDontSeq a b = b

-----------------------------------------------------------------------------
-- Don't inline any functions from the template.  GHC has a nasty habit
-- of deciding to inline happyGoto everywhere, which increases the size of
-- the generated parser quite a bit.

{-# LINE 322 "templates/GenericTemplate.hs" #-}
{-# NOINLINE happyShift #-}
{-# NOINLINE happySpecReduce_0 #-}
{-# NOINLINE happySpecReduce_1 #-}
{-# NOINLINE happySpecReduce_2 #-}
{-# NOINLINE happySpecReduce_3 #-}
{-# NOINLINE happyReduce #-}
{-# NOINLINE happyMonadReduce #-}
{-# NOINLINE happyGoto #-}
{-# NOINLINE happyFail #-}

-- end of Happy Template.
