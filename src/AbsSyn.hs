module AbsSyn where

type Type = String
type VarDecl = (Type, String)
type ParamDecl = VarDecl

data ClassDecl = ClassDecl (Type, [FieldDecl], [MethodDecl])
    deriving (Show)

data FieldDecl = FieldDecl (Type, String)
    deriving (Show)
                            -- typ, name,    parameter,       code
data MethodDecl = MethodDecl (Type, String, [ParamDecl], Stmt)
    deriving (Show)

data Stmt = Block ([Stmt])
          | Return (Maybe Expr)
          | While (Expr, Stmt)
          | LocalVarDecl (Type, String)
          | If (Expr, Stmt, Maybe Stmt) --Maybe Stmt for else statement
          | StmtExprStmt (StmtExpr)
          | TypedStmt (Stmt, Type)
	  | EmptyStmt
          deriving (Show)
          -- | For ((Maybe StmtExpr), (Maybe Expr), (Maybe Expr), Stmt)
          -- | ForEach (Type, String, Exp, Stmt)
          -- | Do (Stmt, Expr)
          -- | Switch

getTypeFromStmt :: Stmt -> Type
getTypeFromStmt (TypedStmt(_, typ)) = typ          

data StmtExpr = Assign (String, Expr)
              | New (Type, [Expr])
              | MethodCall (Expr, String, [Expr])
              | TypedStmtExpr (StmtExpr, Type)
        deriving (Show)

getTypeFromStmtExpr :: StmtExpr -> Type
getTypeFromStmtExpr (TypedStmtExpr(_, typ)) = typ

data Expr = This
          | Super
          | LocalOrFieldVar (String)
          | InstVar (Expr, String)
          | Unary (String, Expr)
          | Binary (String, Expr, Expr)
          | Integer (Integer)
          | Bool (Bool)
          | Char (Char)
          | String (String)
          | Jnull
          | StmtExprExpr (StmtExpr)
          | TypedExpr (Expr, Type)
          deriving (Show)

getTypeFromExpr :: Expr -> Type
getTypeFromExpr (TypedExpr(_, typ)) = typ

type Program = [ClassDecl]

-- String-Werte für Typen:
-- _______________________
-- Integer  : "int"
-- Bool     : "bool"
-- Char     : "char"
-- Void     : "void"
-- String   : "string"
-- Null     : "null" (ist aber mit allen anderen als gleich anzusehen!!!)
-- None (void)   : "<none>"
